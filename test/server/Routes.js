'use strict';

const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chai = require('chai');
chai.use(sinonChai);
const expect = chai.expect;
const _ = require('lodash');
const uuid = require('uuid');

const GameDB = require('../../lib/services/GameDBService');
const Auth = require('../../lib/services/AuthService');
const config = require('../../lib/services/ConfigService');
const Player = require('../../lib/objects/things/Player');
const Account = require('../../lib/objects/things/Account');
const Realms = require('../../lib/objects/Realms');
const ServerService = require('../../lib/services/ServerService');
const routes = require('../../lib/server/routes/');
const Place = require('../../lib/objects/things/Place');

function asRequestObj(obj) {
    return JSON.parse(JSON.stringify(obj));
}

describe('In Routes', () => {

    let sandbox;
    let env;

    // Mock accounts used for testing API access tickets
    // ...outside of the /login and /register endpoints
    let admintestaccount,    
        normaltestaccount;

    before(() => {
        // quash intentionally fired errors
        config.init('CRITICAL');
    });

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        env = {
            responseType: '',
            responseCode: -1,
            model: '',
            request: {}
        };
      
        admintestaccount = new Account('i.am.an.admin@goit.com', 'admin');
        normaltestaccount = new Account('normal@goit.com');
    });

    afterEach(() => {
        sandbox.restore();
        GameDB.reset();
    });

    it('.all() should return an array of objects', (done) => {
        let routes = ServerService.routes;
        expect(routes).to.be.an('array');

        _.each(routes, function (route) {
            expect(route).to.be.an('object');
        });

        done();
    });

    /**
     * Build a handler for a particular endpoint, such as POST to /players
     *
     * Example: let handler = makeHandler('/players', 'GET');
     *          handler(() => { console.log(env.result); });
     *          // outputs the result of GET to /players to the console
     *
     * @param path - path to the endpoint, such as /players or /places
     * @param method - GET, POST, PUT, DELETE, etc.
     * @returns {Function} - function to run your endpoint with env
     */
    function makeHandler(path, method) {
        let handler = _.find(ServerService.routes, (route) => {
            return route.path === path && route.method === method;
        }).handler;

        return function (done) {

            let reply;

            let type = function (type) {
                env.responseType = type;

                if (type === 'application/json') {
                    env.result = JSON.parse(env.result);
                }

                return mockReply;
            };

            let code = function (code) {
                env.responseCode = code;

                return mockReply;
            };

            let redirect = function (path) {
                env.redirectPath = path;

                return mockReply;
            };

            reply = function (result) {
                env.result = result;
                _.defer(() => {
                    done();
                });

                return mockReply;
            };

            let mockReply = {
                code,
                type,
                redirect
            };

            handler(env.request, reply);

        };
    }

    describe('the /players endpoint', () => {

        describe('via GET', () => {

            let players,
                places;

            let handler = makeHandler('/players', 'GET');

            beforeEach(() => {
                players = [
                    new Player('Jack', 'abcd'),
                    new Player('Jill', 'efgh'),
                    new Player('Bob', 'ijkl'),
                    new Player('Alice', 'abc'),
                    new Player('Bobby', 'efg'),
                    new Player('Jillian', 'ijk')
                ];

                places = [
                    new Place('test place 1', 'a test place', 1),
                    new Place('test place 2', 'another test place', 2)
                ];

                places[1].uid = places[0].uid.substring(0, 5);

                _.each(places, (place) => {
                    GameDB.Places.add(place);
                });

                players[2].moveTo(places[0].uid);
                players[5].moveTo(places[0].uid);

                players[3].moveTo(places[1].uid);
                players[4].moveTo(places[1].uid);

                _.each(players, (player) => {
                    GameDB.Players.add(player);
                });

                env.request = {
                    query: {}
                };

                sandbox.spy(GameDB.Players, 'all');
                sandbox.spy(GameDB.Players, 'search');
            });

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Players.all', () => {
                    expect(GameDB.Players.all).to.not.have.been.called;
                });

                it('should not have called GameDB.Players.search', () => {
                    expect(GameDB.Players.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Players.all', () => {
                    expect(GameDB.Players.all).to.not.have.been.called;
                });

                it('should not have called GameDB.Players.search', () => {
                    expect(GameDB.Players.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should have not called GameDB.Players.all', () => {
                    expect(GameDB.Players.all).to.not.have.been.called;
                });

                it('should not have called GameDB.Players.search', () => {
                    expect(GameDB.Players.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when there are no query params', () => {

                let adminticketuid;

                beforeEach((done) => {
                    sandbox.spy(Auth,'retrieveTicket');

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    handler(done);
                });

                it('should have called GameDB.Players.all', () => {
                    expect(GameDB.Players.all).to.have.been.called;
                });

                it('should not have called GameDB.Players.search', () => {
                    expect(GameDB.Players.search).not.to.have.been.called;
                });

                it('should return all the players', () => {
                    expect(env.result).to.deep.equal(asRequestObj({
                        totalPlayers: 6,
                        players: players
                    }));
                });

                it('should have return type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

                it('should return with code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

            });

            describe('when there are query params', () => {

                let adminticketuid;

                beforeEach((done) => {

                    sandbox.spy(Auth,'retrieveTicket');

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    env.request.query.name = 'j';
                    env.request.query.description = 'b';
                    env.request.query.search = true;

                    handler(done);
                });

                it('should have called GameDB.Players.search', () => {
                    expect(GameDB.Players.search).to.have.been.calledWith({
                        name: 'j',
                        description: 'b'
                    });
                });

                it('should not have called GameDB.Players.all', () => {
                    expect(GameDB.Players.all).not.to.have.been.called;
                });

            });

        });

        describe('via POST', () => {

            let handler = makeHandler('/players', 'POST');

            beforeEach(() => {
                sandbox.stub(GameDB.Players, 'add');

                env.request = {
                    headers: {}
                };
            });

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Players.all', () => {
                    expect(GameDB.Players.add).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.payload = {
                        name: 'test'
                    };

                    env.request.headers['content-type'] = 'application/json';

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Players.add', () => {
                    expect(GameDB.Players.add).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    env.request.payload = {
                        name: 'test'
                    };

                    env.request.headers['content-type'] = 'application/json';

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should not have called GameDB.Players.add', () => {
                    expect(GameDB.Players.add).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });


            describe('when your content-type is not application/json', () => {

                beforeEach((done) => {
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Can only accept JSON');
                });

                it('should have response type 415', () => {
                    expect(env.responseCode).to.equal(415);
                });

                it('should not have called GameDB.Players.add', () => {
                    expect(GameDB.Players.add).not.to.have.been.called;
                });

                it('should have response type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when the payload is null', () => {

                beforeEach((done) => {

                    env.request.payload = null;
                    env.request.headers['content-type'] = 'application/json';
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Cannot create a player from input null');
                });

                it('should have response type 415', () => {
                    expect(env.responseCode).to.equal(415);
                });

                it('should not have called GameDB.Players.add', () => {
                    expect(GameDB.Players.add).not.to.have.been.called;
                });

                it('should have response type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when the payload has no name', () => {

                let adminticketuid;

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    env.request.payload = {
                        description: 'test'
                    };

                    env.request.headers['content-type'] = 'application/json';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Cannot instantiate new Thing(Player) with no name');
                });

                it('should have response type 415', () => {
                    expect(env.responseCode).to.equal(400);
                });

                it('should not have called GameDB.Players.add', () => {
                    expect(GameDB.Players.add).not.to.have.been.called;
                });

                it('should have response type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when your request is valid', () => {

                let adminticketuid;  

                beforeEach((done) => {
                    
                    sandbox.spy(Auth, 'retrieveTicket');
                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);
                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    env.request.payload = {
                        name: 'test'
                    };

                    env.request.headers['content-type'] = 'application/json';

                    handler(done);
                });

                it('should reply with the test player', () => {
                    let result = env.result;
                    let player = GameDB.Players.add.getCall(0).args[0];

                    expect(result).to.deep.equal(asRequestObj(player));
                });

                it('should have response type 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should have called GameDB.Players.add with a player object', () => {
                    expect(GameDB.Players.add.getCall(0).args[0].type).to.equal('Player');
                });

                it('should have response type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

            });

        });

    });

    describe('the /players/{id} endpoint', () => {

        describe('via GET', () => {

            let handler = makeHandler('/players/{id}', 'GET');

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Players, 'byId');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request = {
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Players.byId', () => {
                    expect(GameDB.Players.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });


            describe('when invoked by an account that is not logged in', () => {

                let player;

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Players, 'byId');

                    GameDB.Accounts.add(normaltestaccount);

                    player = new Player('test');
                    GameDB.Players.add(player);

                    env.request = {
                        params: {
                            id: player.uid
                        },
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Players.byId', () => {
                    expect(GameDB.Players.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid,
                    player;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Players, 'byId');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    player = new Player('test');
                    GameDB.Players.add(player);

                    env.request = {
                        params: {
                            id: player.uid
                        },
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should have not called GameDB.Players.byId', () => {
                    expect(GameDB.Players.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('with a valid player uid', () => {

                let player,
                    adminticketuid;

                beforeEach((done) => {
                    player = new Player('test');
                    GameDB.Players.add(player);

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request = {
                        params: {
                            id: player.uid
                        },
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    sandbox.spy(GameDB.Players, 'byId');

                    handler(done);
                });

                it('should have called GameDB.Players.byId', () => {
                    expect(GameDB.Players.byId).to.have.been.calledWith(player.uid);
                });

                it('should return the player in JSON format', () => {
                    expect(env.result).to.deep.equal(asRequestObj(player));
                });

                it('should have return code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should response with type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

            });

            describe('with an invalid player uid', () => {

                let player,
                    adminticketuid;

                beforeEach((done) => {

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);
                    
                    env.request = {
                        params: {
                            id: 'test'
                        },
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    handler(done);
                });

                it('should return an error', () => {
                    expect(env.result).to.equal('No Player with uid test');
                });

                it('should return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

                it('should return code 404', () => {
                    expect(env.responseCode).to.equal(404);
                });

            });

        });

    });

    describe('the /places endpoint', () => {

        describe('via GET', () => {

            let handler = makeHandler('/places', 'GET');

            beforeEach(() => {
                sandbox.stub(GameDB.Places, 'all').returns(['all of the places']);
                //sandbox.spy(GameDB.Places, 'search');
            });

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Places.all', () => {
                    expect(GameDB.Places.all).to.not.have.been.called;
                });

                it.skip('should have not called GameDB.Places.search', () => {
                    expect(GameDB.Places.search).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Places.all', () => {
                    expect(GameDB.Places.all).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should have not called GameDB.Places.all', () => {
                    expect(GameDB.Places.all).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when there are no query params', () => {

                let adminticketuid;

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;


                    handler(done);
                });

                it('should return all the Places in a list', () => {
                    expect(env.result).to.deep.equal({
                        places: ['all of the places'],
                        numPlaces: 1
                    });
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(adminticketuid);
                });

                it('should have called GameDB.Places.all', () => {
                    expect(GameDB.Places.all).to.have.been.calledOnce;
                });

                it('should return code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should return type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

            });

        });

        describe('via POST', () => {

            let handler = makeHandler('/places', 'POST');

            describe('when your request is invalid', () => {

                beforeEach(() => {
                    env.request.headers = {};
                    sandbox.stub(GameDB.Places, 'add');
                });

                describe('because of the missing custom auth headers', () => {

                    beforeEach((done) => {
    
                        sandbox.spy(Auth, 'retrieveTicket');
    
                        GameDB.Accounts.add(normaltestaccount);
                        
                        env.request.headers['auth-a'] = normaltestaccount.uid;
                        //env.request.headers['auth-t'] = 'goit';
    
                        handler(done);
                    });
    
                    it('should reply with an error', () => {
                        expect(env.result).to.equal('Authentication headers required');
                    });
    
                    it('should reply with code 401', () => {
                        expect(env.responseCode).to.equal(401);
                    });
    
                    it('should not call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.not.have.been.called;
                    });
    
                    it('should have not called GameDB.Places.all', () => {
                        expect(GameDB.Places.add).to.not.have.been.called;
                    });
    
                    it('should have return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });
    
                });

                describe('because the requesting account is not logged in', () => {

                    beforeEach((done) => {

                        sandbox.spy(Auth, 'retrieveTicket');
    
                        GameDB.Accounts.add(normaltestaccount);
    
                        env.request.payload = {
                            name: 'test location',
                            description: 'a test',
                            size: 20
                        };
    
                        env.request.headers['content-type'] = 'application/json';
                        env.request.headers['accept'] = 'application/json';
    
                        env.request.headers['auth-a'] = normaltestaccount.uid;
                        env.request.headers['auth-t'] = 'goit';
    
                        handler(done);
                    });
    
                    it('should reply with an error', () => {
                        expect(env.result).to.equal('Please log in to use this resource');
                    });
    
                    it('should reply with code 401', () => {
                        expect(env.responseCode).to.equal(401);
                    });
    
                    it('should call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                    });
    
                    it('should have not called GameDB.Places.add', () => {
                        expect(GameDB.Places.add).to.not.have.been.called;
                    });
    
                    it('should have return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });
                });
    
                describe('because a non-admin account sent the request', () => {
                  
                    let normalticketuid;  
    
                    beforeEach((done) => {
                  
                        sandbox.spy(Auth, 'retrieveTicket');
        
                        GameDB.Accounts.add(normaltestaccount);
                        normalticketuid = Auth.generateTicket(normaltestaccount);
    
                        env.request.payload = {
                            name: 'test location',
                            description: 'a test',
                            size: 20
                        };
    
                        env.request.headers['content-type'] = 'application/json';
                        env.request.headers['accept'] = 'application/json';
    
                        env.request.headers['auth-a'] = normaltestaccount.uid;
                        env.request.headers['auth-t'] = normalticketuid;
    
                        handler(done);
                    });
    
                    it('should reply with an error', () => {
                        expect(env.result).to.equal('Normal users are not allowed to use this resource');
                    });
    
                    it('should reply with code 401', () => {
                        expect(env.responseCode).to.equal(401);
                    });
    
                    it('should call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                    });
    
                    it('should not have called GameDB.Places.add', () => {
                        expect(GameDB.Places.add).not.to.have.been.called;
                    });
    
                    it('should have return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                });

                describe('because of an incorrect Accept header', () => {

                    beforeEach((done) => {

                        sandbox.spy(Auth, 'retrieveTicket');

                        env.request.headers['content-type'] = 'application/json';

                        env.request.headers['auth-a'] = 'goit';
                        env.request.headers['auth-t'] = 'goit';

                        handler(done);
                    });

                    it('should have returned an error', () => {
                        expect(env.result).to.equal('Server can only generate application/json');
                    });

                    it('should not have called GameDB.Places.add', () => {
                        expect(GameDB.Places.add).not.to.have.been.called;
                    });

                    it('should not call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.not.have.been.called;
                    });

                    it('should have returned with code 406', () => {
                        expect(env.responseCode).to.equal(406);
                    });

                    it('should have returned type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                });

                describe('because of an incorrect Content-Type header', () => {

                    beforeEach((done) => {
                        sandbox.spy(Auth, 'retrieveTicket');

                        env.request.headers['accept'] = 'application/json';
                        env.request.headers['auth-a'] = 'goit';
                        env.request.headers['auth-t'] = 'goit';

                        handler(done);
                    });

                    it('should have returned an error', () => {
                        expect(env.result).to.equal('Server can only accept application/json');
                    });

                    it('should not have called GameDB.Places.add', () => {
                        expect(GameDB.Places.add).not.to.have.been.called;
                    });

                    it('should not call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.not.have.been.called;
                    });

                    it('should have returned with code 406', () => {
                        expect(env.responseCode).to.equal(415);
                    });

                    it('should have returned type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                });

                describe('because of a malformed payload', () => {

                    beforeEach((done) => {
                        sandbox.spy(Auth, 'retrieveTicket');

                        env.request.headers['accept'] = 'application/json';
                        env.request.headers['content-type'] = 'application/json';
                        env.request.headers['auth-a'] = 'goit';
                        env.request.headers['auth-t'] = 'goit';

                        env.request.payload = {};
                        handler(done);
                    });

                    it('should have returned an error', () => {
                        expect(env.result).to.equal('Cannot create a place from input. Requires a name, description, and size.');
                    });

                    it('should have returned with code 400', () => {
                        expect(env.responseCode).to.equal(400);
                    });
                    
                    it('should not call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.not.have.been.called;
                    });

                    it('should have returned with type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                    it('should not have called GameDB.Places.add', () => {
                        expect(GameDB.Places.add).not.to.have.been.called;
                    });

                });

            });

            describe('when your request is valid', () => {

                let adminticketuid;

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers = {
                        accept: 'application/json',
                        'content-type': 'application/json',
                        'auth-t': adminticketuid,
                        'auth-a': admintestaccount.uid
                    };

                    env.request.payload = {
                        name: 'test location',
                        description: 'a test',
                        size: 20
                    };

                    sandbox.stub(GameDB.Places, 'add', (place) => {
                        env.sideEffect = place;
                    });

                    handler(done);
                });

                it('should have returned the created place', () => {
                    expect(env.result).to.deep.equal(asRequestObj(env.sideEffect));
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(adminticketuid);
                });

                it('should have called GameDB.Places.add', () => {
                    expect(GameDB.Places.add).to.have.been.calledOnce;
                });

                it('should have returned with code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should have returned with type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

            });

        });

    });

    describe('the /places/{id} endpoint', () => {

        describe('via GET', () => {

            let handler = makeHandler('/places/{id}', 'GET');

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Places, 'byId');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request = {
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Places.byId', () => {
                    expect(GameDB.Places.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                let place;

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Places, 'byId');

                    GameDB.Accounts.add(normaltestaccount);

                    place = new Place('test', 'nothing', 1);
                    GameDB.Places.add(place);

                    env.request = {
                        params: {
                            id: 'goit'
                        },
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Places.byId', () => {
                    expect(GameDB.Places.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid,
                    place;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Places, 'byId');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    place = new Place('test','test',1);
                    GameDB.Places.add(place);

                    env.request = {
                        params: {
                            id: 'goit'
                        },
                        headers: {
                        }
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should have not called GameDB.Places.byId', () => {
                    expect(GameDB.Places.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('with a valid place UID', () => {

                let place;

                beforeEach((done) => {
                    place = new Place('test', 'nothing', 1);
                    GameDB.Places.add(place);
                    GameDB.Accounts.add(admintestaccount);

                    env.request = {
                        params: {
                            id: place.uid
                        },
                        headers: {
                            'auth-a': admintestaccount.uid,
                            'auth-t': Auth.generateTicket(admintestaccount)
                        }
                    };

                    sandbox.spy(GameDB.Places, 'byId');
                    sandbox.spy(Auth, 'retrieveTicket');

                    handler(done);
                });

                it('should have called GameDB.Places.byId', () => {
                    expect(GameDB.Places.byId).to.have.been.calledWith(place.uid);
                });

                it('should return the player in JSON format', () => {
                    expect(env.result).to.deep.equal(asRequestObj(place));
                });

                it('should have return code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(env.request.headers['auth-t']);
                });

                it('should response with type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });
            });

            describe('with an invalid place uid', () => {

                beforeEach((done) => {

                    GameDB.Accounts.add(admintestaccount);

                    env.request = {
                        params: {
                            id: 'test'
                        },
                        headers: {
                            'auth-a': admintestaccount.uid,
                            'auth-t': Auth.generateTicket(admintestaccount)

                        }
                    };

                    sandbox.spy(GameDB.Places, 'byId');
                    sandbox.spy(Auth, 'retrieveTicket');

                    handler(done);
                });

                it('should return an error', () => {
                    expect(env.result).to.equal('No Place with uid test');
                });

                it('should return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

                it('should return code 404', () => {
                    expect(env.responseCode).to.equal(404);
                });

            });

        });

    });

    describe('the /realms endpoint', () => {

        describe('via GET', () => {

            let handler = makeHandler('/realms', 'GET');

            beforeEach(() => {
                env.request.headers = {};
            });

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Realms, 'all');
                    //sandbox.spy(GameDB.Realms, 'search');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Realms.all', () => {
                    expect(GameDB.Realms.all).to.not.have.been.called;
                });

                it.skip('should not have called GameDB.Realms.search', () => {
                    expect(GameDB.Realms.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Realms, 'all');
                    //sandbox.spy(GameDB.Realms, 'search');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Realms.all', () => {
                    expect(GameDB.Realms.all).to.not.have.been.called;
                });

                it.skip('should not have called GameDB.Realms.search', () => {
                    expect(GameDB.Realms.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Realms, 'all');
                    //sandbox.spy(GameDB.Realms, 'search');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should have not called GameDB.Realms.all', () => {
                    expect(GameDB.Realms.all).to.not.have.been.called;
                });

                it.skip('should not have called GameDB.Realms.search', () => {
                    expect(GameDB.Realms.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when you have an invalid Accept header', () => {

                let adminticketuid;

                beforeEach((done) => {
                    env.request.headers.accept = 'literally anything but application/json';
                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    sandbox.stub(GameDB.Realms, 'all');
                    handler(done);
                });

                it('should return an error', () => {
                    expect(env.result).to.equal('Accept must be application/json');
                });

                it('should return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

                it('should return code 406', () => {
                    expect(env.responseCode).to.equal(406);
                });

                it('should not have called GameDB.Realms.all', () => {
                    expect(GameDB.Realms.all).not.to.have.been.called;
                });

            });

            describe('when you have a valid Accept header', () => {

                let adminticketuid;

                beforeEach(() => {
                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);
                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;
                    env.request.headers.accept = 'application/json';
                });

                describe('when there are no query params', () => {

                    beforeEach((done) => {

                        sandbox.stub(GameDB.Realms, 'all').returns('the realms');

                        handler(done);
                    });

                    it('should return all the realms', () => {
                        expect(env.result).to.deep.equal({
                            realms: 'the realms',
                            totalRealms: GameDB.Realms.totalCount
                        });
                    });

                    it('should return type application/json', () => {
                        expect(env.responseType).to.equal('application/json');
                    });

                    it('should return code 200', () => {
                        expect(env.responseCode).to.equal(200);
                    });

                    it('should have called GameDB.Realms.all', () => {
                        expect(GameDB.Realms.all).to.have.been.calledOnce;
                    });

                });

            });

        });

        describe('via POST', () => {

            let handler = makeHandler('/realms', 'POST');
            let sideEffect;

            beforeEach(() => {
                env.request.headers = {};
                env.request.payload = {};

                sandbox.stub(GameDB.Realms, 'add', sideEffect);
            });

            describe('when your request headers are invalid', () => {

                describe('because of the missing custom auth headers', () => {

                    beforeEach((done) => {
    
                        sandbox.spy(Auth, 'retrieveTicket');
    
                        GameDB.Accounts.add(normaltestaccount);
                        
                        env.request.headers['auth-a'] = normaltestaccount.uid;
                        //env.request.headers['auth-t'] = 'goit';
    
                        handler(done);
                    });
    
                    it('should reply with an error', () => {
                        expect(env.result).to.equal('Authentication headers required');
                    });
    
                    it('should reply with code 401', () => {
                        expect(env.responseCode).to.equal(401);
                    });
    
                    it('should not call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.not.have.been.called;
                    });
    
                    it('should have not called GameDB.Realms.all', () => {
                        expect(GameDB.Realms.add).to.not.have.been.called;
                    });
    
                    it('should have return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });
    
                });

                describe('because the requesting account is not logged in', () => {

                    beforeEach((done) => {

                        sandbox.spy(Auth, 'retrieveTicket');
    
                        GameDB.Accounts.add(normaltestaccount);
    
                        env.request.payload = {
                            name: 'test location',
                            description: 'a test',
                            size: 20
                        };
    
                        env.request.headers['content-type'] = 'application/json';
                        env.request.headers['accept'] = 'application/json';
    
                        env.request.headers['auth-a'] = normaltestaccount.uid;
                        env.request.headers['auth-t'] = 'goit';
    
                        handler(done);
                    });
    
                    it('should reply with an error', () => {
                        expect(env.result).to.equal('Please log in to use this resource');
                    });
    
                    it('should reply with code 401', () => {
                        expect(env.responseCode).to.equal(401);
                    });
    
                    it('should call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                    });
    
                    it('should have not called GameDB.Realms.add', () => {
                        expect(GameDB.Realms.add).to.not.have.been.called;
                    });
    
                    it('should have return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });
                });
    
                describe('because a non-admin account sent the request', () => {
                  
                    let normalticketuid;  
    
                    beforeEach((done) => {
                  
                        sandbox.spy(Auth, 'retrieveTicket');
        
                        GameDB.Accounts.add(normaltestaccount);
                        normalticketuid = Auth.generateTicket(normaltestaccount);
    
                        env.request.payload = {
                            name: 'test location',
                            description: 'a test',
                            size: 20
                        };
    
                        env.request.headers['content-type'] = 'application/json';
                        env.request.headers['accept'] = 'application/json';
    
                        env.request.headers['auth-a'] = normaltestaccount.uid;
                        env.request.headers['auth-t'] = normalticketuid;
    
                        handler(done);
                    });
    
                    it('should reply with an error', () => {
                        expect(env.result).to.equal('Normal users are not allowed to use this resource');
                    });
    
                    it('should reply with code 401', () => {
                        expect(env.responseCode).to.equal(401);
                    });
    
                    it('should call Auth.retrieveTicket', () => {
                        expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                    });
    
                    it('should not have called GameDB.Realms.add', () => {
                        expect(GameDB.Realms.add).not.to.have.been.called;
                    });
    
                    it('should have return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                });

                describe('because of an invalid accept', () => {

                    beforeEach((done) => {
                        env.request.headers['auth-a'] = 'goit';
                        env.request.headers['auth-t'] = 'goit';

                        env.request.headers['content-type'] = 'application/json';
                        handler(done);
                    });

                    it('should return an error', () => {
                        expect(env.result).to.equal('server can only return json');
                    });

                    it('should return code 406', () => {
                        expect(env.responseCode).to.equal(406);
                    });

                    it('should return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                    it('should not have called GameDB.Realms.add', () => {
                        expect(GameDB.Realms.add).not.to.have.been.called;
                    });

                });

                describe('because of an invalid content-type', () => {

                    beforeEach((done) => {
                        env.request.headers['auth-a'] = 'goit';
                        env.request.headers['auth-t'] = 'goit';

                        env.request.headers.accept = 'application/json';
                        handler(done);
                    });

                    it('should return an error', () => {
                        expect(env.result).to.equal('server can only accept json');
                    });

                    it('should return code 415', () => {
                        expect(env.responseCode).to.equal(415);
                    });

                    it('should return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                    it('should not have called GameDB.Realms.add', () => {
                        expect(GameDB.Realms.add).not.to.have.been.called;
                    });

                });

                describe('because of an invalid accept and content-type', () => {

                    beforeEach((done) => {
                        env.request.headers['auth-a'] = 'goit';
                        env.request.headers['auth-t'] = 'goit';

                        handler(done);
                    });

                    it('should return an error', () => {
                        expect(env.result).to.equal('server can only return json');
                    });

                    it('should return code 406', () => {
                        expect(env.responseCode).to.equal(406);
                    });

                    it('should return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                    it('should not have called GameDB.Realms.add', () => {
                        expect(GameDB.Realms.add).not.to.have.been.called;
                    });

                });

            });

            describe('when your request headers are correct', () => {

                let adminticketuid;

                beforeEach(() => {

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers.accept = 'application/json';
                    env.request.headers['content-type'] = 'application/json';
                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                });

                describe('when your request body is empty', () => {

                    beforeEach((done) => {
                        handler(done);
                    });

                    it('should return an error', () => {
                        expect(env.result).to.equal('Cannot create a nameless Realm!');
                    });

                    it('should return code 400', () => {
                        expect(env.responseCode).to.equal(400);
                    });

                    it('should return type text/plain', () => {
                        expect(env.responseType).to.equal('text/plain');
                    });

                    it('should not have called GameDB.Realms.add', () => {
                        expect(GameDB.Realms.add).not.to.have.been.called;
                    });

                });

                describe('when your request body has a description', () => {

                    beforeEach(() => {
                        env.request.payload.description = 'test realm';
                    });

                    describe('and no name', () => {

                        beforeEach((done) => {
                            handler(done);
                        });

                        it('should return an error', () => {
                            expect(env.result).to.equal('Cannot create a nameless Realm!');
                        });

                        it('should return code 400', () => {
                            expect(env.responseCode).to.equal(400);
                        });

                        it('should return type text/plain', () => {
                            expect(env.responseType).to.equal('text/plain');
                        });

                        it('should not have called GameDB.Realms.add', () => {
                            expect(GameDB.Realms.add).not.to.have.been.called;
                        });

                    });

                    describe('and a name', () => {

                        beforeEach(() => {
                            env.request.payload.name = 'test';
                        });

                        describe('and succeeds', () => {

                            beforeEach((done) => {
                                sideEffect = (realm) => {
                                    env.sideEffect = realm;
                                };

                                handler(done);
                            });

                            it('should have called GameDB.Realms.add', () => {
                                expect(GameDB.Realms.add).to.have.been.calledOnce;
                            });

                            it('should return the added realm', () => {
                                expect(env.result).to.deep.equal(asRequestObj(env.sideEffect));
                            });

                            it('should return with code 200', () => {
                                expect(env.responseCode).to.equal(200);
                            });

                            it('should return with type application/json', () => {
                                expect(env.responseType).to.equal('application/json');
                            });

                        });

                        describe('and there is an error', () => {

                            beforeEach((done) => {
                                sideEffect = () => {
                                    throw new Error('test error');
                                };

                                handler(done);
                            });

                            it('should have called GameDB.Realms.add', () => {
                                expect(GameDB.Realms.add).to.have.been.calledOnce;
                            });

                            it('should return an error', () => {
                                expect(env.result).to.equal('test error');
                            });

                            it('should return type text/plain', () => {
                                expect(env.responseType).to.equal('text/plain');
                            });

                            it('should return code 500', () => {
                                expect(env.responseCode).to.equal(500);
                            });

                        });

                    });

                });

            });

        });

    });

    /*BEGIN Account TESTS*/

    describe('the /accounts endpoint', () => {

        describe('via GET', () => {

            let accounts;

            let handler = makeHandler('/accounts', 'GET');

            beforeEach(() => {
                accounts = [];

                env.request = {
                    query: {},
                    headers: {}
                };

                sandbox.spy(GameDB.Accounts, 'all');
                sandbox.spy(GameDB.Accounts, 'search');
            });

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Accounts.all', () => {
                    expect(GameDB.Accounts.all).to.not.have.been.called;
                });

                it('should not have called GameDB.Accounts.search', () => {
                    expect(GameDB.Accounts.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Accounts.all', () => {
                    expect(GameDB.Accounts.all).to.not.have.been.called;
                });

                it('should not have called GameDB.Accounts.search', () => {
                    expect(GameDB.Accounts.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid;  

                beforeEach((done) => {
              
                    sandbox.spy(Auth, 'retrieveTicket');

                    GameDB.Accounts.add(normaltestaccount);
                    normalticketuid = Auth.generateTicket(normaltestaccount);

                    env.request.headers = {
                    };

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = normalticketuid;

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(normalticketuid);
                });

                it('should have not called GameDB.Accounts.all', () => {
                    expect(GameDB.Accounts.all).to.not.have.been.called;
                });

                it('should not have called GameDB.Accounts.search', () => {
                    expect(GameDB.Accounts.search).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when there are no query params', () => {

                let adminticketuid;

                beforeEach((done) => {
                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);
                    accounts.push(admintestaccount);

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    sandbox.spy(Auth, 'retrieveTicket');

                    handler(done);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(adminticketuid);
                });

                it('should have called GameDB.Accounts.all', () => {
                    expect(GameDB.Accounts.all).to.have.been.called;
                });

                it('should not have called GameDB.Accounts.search', () => {
                    expect(GameDB.Accounts.search).not.to.have.been.called;
                });

                it('should return all the accounts', () => {
                    expect(env.result).to.deep.equal(asRequestObj({
                        totalAccounts: accounts.length,
                        accounts: accounts
                    }));
                });

                it('should have return type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

                it('should return with code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

            });

            describe('when there are query params', () => {

                let adminticketuid;

                beforeEach((done) => {
                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    sandbox.spy(Auth, 'retrieveTicket');

                    env.request.query.email = 'test@goit.com';
                    env.request.query.search = true;

                    handler(done);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith(adminticketuid);
                });

                it('should have called GameDB.Accounts.search', () => {
                    expect(GameDB.Accounts.search).to.have.been.calledWith({
                        email: 'test@goit.com'
                    });
                });

                it('should not have called GameDB.Accounts.all', () => {
                    expect(GameDB.Accounts.all).not.to.have.been.called;
                });

            });

        });

        describe('via POST', () => {

            let handler = makeHandler('/accounts', 'POST');

            beforeEach(() => {
                sandbox.stub(GameDB.Accounts, 'add');
                
                env.request = {
                    headers: {}
                };
            });

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Accounts.all', () => {
                    expect(GameDB.Accounts.add).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when invoked by an account that is not logged in', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');

                    env.request.payload = {
                        name: 'test'
                    };

                    env.request.headers['content-type'] = 'application/json';

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Accounts.add', () => {
                    expect(GameDB.Accounts.add).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                beforeEach((done) => {
              
                    sandbox.stub(Auth, 'retrieveTicket', () => {
                        return 'test';
                    });

                    sandbox.stub(GameDB.Accounts, 'byId', () => {
                        return {
                            account_type:'goit'
                        };
                    });
                   
                    env.request.payload = {
                        name: 'test'
                    };

                    env.request.headers['content-type'] = 'application/json';

                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'test';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('test');
                });

                it('should not have called GameDB.Accounts.add', () => {
                    expect(GameDB.Accounts.add).not.to.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });
            
            describe('when your content-type is not application/json', () => {

                let adminticketuid;

                beforeEach((done) => {

                    sandbox.stub(GameDB.Accounts, 'byId', () => {
                        return {
                            account_type: 'admin'
                        };
                    });

                    sandbox.stub(Auth, 'retrieveTicket', () => {
                        return {
                            uid: 'test',
                            account_uid: 'test'
                        };  
                    });

                    env.request.headers['auth-a'] = 'test';
                    env.request.headers['auth-t'] = 'test';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Can only accept JSON');
                });

                it('should have response type 415', () => {
                    expect(env.responseCode).to.equal(415);
                });

                it('should not have called GameDB.Accounts.add', () => {
                    expect(GameDB.Accounts.add).not.to.have.been.called;
                });

                it('should have response type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when the payload is null', () => {

                let adminticketuid;

                beforeEach((done) => {

                    sandbox.stub(GameDB.Accounts, 'byId', () => {
                        return {
                            account_type: 'admin'
                        };
                    });

                    sandbox.stub(Auth, 'retrieveTicket', () => {
                        return {
                            uid: 'test',
                            account_uid: 'test'
                        };  
                    });

                    env.request.headers['auth-a'] = 'test';
                    env.request.headers['auth-t'] = 'test';

                    env.request.payload = null;
                    env.request.headers['content-type'] = 'application/json';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Cannot create an account from input null');
                });

                it('should have response type 415', () => {
                    expect(env.responseCode).to.equal(415);
                });

                it('should not have called GameDB.Accounts.add', () => {
                    expect(GameDB.Accounts.add).not.to.have.been.called;
                });

                it('should have response type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when the payload has no email', () => {

                let adminticketuid;

                beforeEach((done) => {
                    env.request.payload = {
                        email: ''
                    };

                    sandbox.stub(GameDB.Accounts, 'byId', () => {
                        return {
                            account_type: 'admin'
                        };
                    });

                    sandbox.stub(Auth, 'retrieveTicket', () => {
                        return {
                            uid: 'test',
                            account_uid: 'test'
                        };  
                    });

                    env.request.headers['auth-a'] = 'test';
                    env.request.headers['auth-t'] = 'test';

                    env.request.headers['content-type'] = 'application/json';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Cannot create Account without an email address');
                });

                it('should have response type 415', () => {
                    expect(env.responseCode).to.equal(400);
                });

                it('should not have called GameDB.Accounts.add', () => {
                    expect(GameDB.Accounts.add).not.to.have.been.called;
                });

                it('should have response type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('when your request is valid', () => {

                let adminticketuid;

                beforeEach((done) => {
                    env.request.payload = {
                        email: 'test@goit.com',
                        password: 'test'
                    };

                    sandbox.stub(GameDB.Accounts, 'byId', () => {
                        return {
                            account_type: 'admin'
                        };
                    });

                    sandbox.stub(Auth, 'retrieveTicket', () => {
                        return {
                            uid: 'test',
                            account_uid: 'test'
                        };  
                    });

                    env.request.headers['auth-a'] = 'test';
                    env.request.headers['auth-t'] = 'test';

                    env.request.headers['content-type'] = 'application/json';

                    handler(done);
                });

                it('should reply with the test account', () => {
                    let result = env.result;
                    let account = GameDB.Accounts.add.getCall(0).args[0];

                    expect(result).to.deep.equal(asRequestObj(account));
                });

                it('should have response type 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should have called GameDB.Accounts.add with an account object', () => {
                    expect(GameDB.Accounts.add.getCall(0).args[0] instanceof Account).to.be.true;
                });

                it('should have response type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

            });

        });

    });

    describe('the /accounts/{id} endpoint', () => {

        describe('via GET', () => {

            let handler = makeHandler('/accounts/{id}', 'GET');

            describe('when invoked without any of the custom auth headers', () => {

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Accounts, 'byId');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request = {
                        headers: {}
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    //env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Authentication headers required');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should not call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.not.have.been.called;
                });

                it('should have not called GameDB.Accounts.byId', () => {
                    expect(GameDB.Accounts.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });


            describe('when invoked by an account that is not logged in', () => {

                let player;

                beforeEach((done) => {

                    sandbox.spy(Auth, 'retrieveTicket');
                    sandbox.spy(GameDB.Accounts, 'byId');

                    GameDB.Accounts.add(normaltestaccount);

                    env.request = {
                        params: {},
                        headers: {}
                    };
                    
                    env.request.headers['auth-a'] = normaltestaccount.uid;
                    env.request.headers['auth-t'] = 'goit';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Please log in to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('goit');
                });

                it('should have not called GameDB.Accounts.byId', () => {
                    expect(GameDB.Accounts.byId).to.not.have.been.called;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });
            });

            describe('when invoked by a non-admin account', () => {
              
                let normalticketuid,
                    player;  

                beforeEach((done) => {

                    sandbox.stub(GameDB.Accounts, 'byId', () => {
                        return {
                            account_type: 'goit'
                        };
                    });

                    sandbox.stub(Auth, 'retrieveTicket', () => {
                        return {
                            uid: 'test',
                            account_uid: 'test'
                        };  
                    });

                    
                    env.request = {
                        params: {},
                        headers: {}
                    };
                    
                    env.request.headers['auth-a'] = 'test';
                    env.request.headers['auth-t'] = 'test';

                    handler(done);
                });

                it('should reply with an error', () => {
                    expect(env.result).to.equal('Normal users are not allowed to use this resource');
                });

                it('should reply with code 401', () => {
                    expect(env.responseCode).to.equal(401);
                });

                it('should call Auth.retrieveTicket', () => {
                    expect(Auth.retrieveTicket).to.have.been.calledWith('test');
                });

                it('should call GameDB.Accounts.byId once to check the invoking account\'s type', () => {
                    expect(GameDB.Accounts.byId).to.have.been.calledOnce;
                });

                it('should have return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

            });

            describe('with a valid account uid', () => {

                let account, adminticketuid;

                beforeEach((done) => {
                    account = new Account('test@goit.com', 'goit');
                    GameDB.Accounts.add(account);

                    env.request = {
                        params: {
                            id: account.uid
                        },
                        headers: {}
                    };

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    sandbox.spy(GameDB.Accounts, 'byId');

                    handler(done);
                });

                it('should have called GameDB.Accounts.byId', () => {
                    expect(GameDB.Accounts.byId).to.have.been.calledWith(account.uid);
                });

                it('should return the account in JSON format', () => {
                    expect(env.result).to.deep.equal(asRequestObj(account));
                });

                it('should have return code 200', () => {
                    expect(env.responseCode).to.equal(200);
                });

                it('should response with type application/json', () => {
                    expect(env.responseType).to.equal('application/json');
                });

            });

            describe('with an invalid account uid', () => {

                let adminticketuid;

                beforeEach((done) => {
                    env.request = {
                        params: {
                            id: 'test'
                        },
                        headers: {}
                    };

                    GameDB.Accounts.add(admintestaccount);
                    adminticketuid = Auth.generateTicket(admintestaccount);

                    env.request.headers['auth-a'] = admintestaccount.uid;
                    env.request.headers['auth-t'] = adminticketuid;

                    handler(done);
                });

                it('should return an error', () => {
                    expect(env.result).to.equal('No Account with uid test');
                });

                it('should return type text/plain', () => {
                    expect(env.responseType).to.equal('text/plain');
                });

                it('should return code 404', () => {
                    expect(env.responseCode).to.equal(404);
                });

            });

        });

    });

    /*BEGIN authentication route tests*/

    describe('the /login endpoint (POST)',  () => {

        let handler = makeHandler('/login', 'POST'),
            dummyAccount;

        beforeEach(() => {

            dummyAccount = new Account('test@goit.com');

            sandbox.spy(GameDB.Accounts, 'byEmail');          
            env.request = {
                headers: {}
            };
        });
				
        describe('when the sent application type isn\'t \'application/json\'', () => {

            beforeEach((done) => {
          
                sandbox.spy(Auth, 'validateLogin');

                handler(done);
            });
 
            it('should reply with an error', () => {
                expect(env.result).to.equal('Can only accept JSON');
            });

            it('should have response type 415', () => {
                expect(env.responseCode).to.equal(415);
            });

            it('should not have called Auth.validateLogin', () => {
                expect(Auth.validateLogin).not.to.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when the payload is null', () => {

            beforeEach((done) => {
                sandbox.spy(Auth, 'validateLogin');

                env.request.payload = null;
                env.request.headers['content-type'] = 'application/json';
                handler(done);
            });
          
            it('should reply with an error', () => {
                expect(env.result).to.equal('No login credentials provided');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should not have called Auth.validateLogin', () => {
                expect(Auth.validateLogin).not.to.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when the payload has no email', () => {

            beforeEach((done) => {

                sandbox.spy(Auth, 'validateLogin');

                env.request.payload = {
                    password: 'plampgoit'
                };

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should reply with an error', () => {
                expect(env.result).to.equal('Email address required for login');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should not have called Auth.validateLogin', () => {
                expect(Auth.validateLogin).to.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when the payload has a garbage \'email\' address/an email with no associated Account', () => {

            beforeEach((done) => {
                env.request.payload = {
                    email: 'kylebuschisawhinybitch',
                    password: 'plampgoit'
                };

                sandbox.spy(Auth, 'validateLogin');

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should reply with an error', () => {
                expect(env.result).to.equal('Invalid Login');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should have called Auth.validateLogin', () => {
                expect(Auth.validateLogin).to.have.been.calledWith('kylebuschisawhinybitch');
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when your request is valid, but has incorrect credentials', () => {

            beforeEach((done) => {

                let orphan = new Account('kyle.busch.is.a@goit.com');

                GameDB.Accounts.add(orphan);

                sandbox.spy(Auth, 'validateLogin');
                sandbox.spy(Auth, 'generateTicket');

                env.request.payload = {
                    email: 'test@goit.com'
                };

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should call Auth.validateLogin', () => {
                expect(Auth.validateLogin).to.have.been.calledWith('test@goit.com');
            });

            it('should not call Auth.generateTicket', () => {
                expect(Auth.generateTicket).to.not.have.been.called;
            });


            it('should reply with an error saying the login attempt failed', () => {
                expect(env.result).to.equal('Invalid Login');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when your request is valid with correct credentials', () => {

            beforeEach((done) => {

                GameDB.Accounts.add(dummyAccount);
             
                sandbox.spy(Auth, 'validateLogin');
                sandbox.spy(Auth, 'generateTicket');

                

                env.request.payload = {
                    email: 'test@goit.com'
                };

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should call Auth.validateLogin', () => {
                expect(Auth.validateLogin).to.have.been.calledWith('test@goit.com');
            });

            it('should call Auth.generateTicket', () => {
                expect(Auth.generateTicket).to.have.been.calledWith(dummyAccount);
            });

            it('should reply with a session ticket UID', () => {
                expect(env.result.ticket_uid).to.not.be.undefined;
            });

            it('should reply with the target account\'s UID', () => {
                expect(env.result.account_uid).to.equal(dummyAccount.uid);
            });

            it('should add the ticket to the AuthService\'s list of current valid tickets', () => {
                expect(Auth.retrieveTicket(env.result.ticket_uid)).to.not.be.undefined;
            });
            
            it('should have response type 200', () => {
                expect(env.responseCode).to.equal(200);
            });

            it('should have response type application/json', () => {
                expect(env.responseType).to.equal('application/json');
            });

        });

    });

    describe('the /register endpoint (POST)',  () => {

        let handler = makeHandler('/register', 'POST');

        beforeEach(() => {
    
            sandbox.spy(Auth,'validateNewAccount');
            sandbox.spy(GameDB.Accounts,'add');
                                    
            env.request = {
                headers: {}
            };
        });
				
        describe('when the sent application type isn\'t \'application/json\'', () => {

            beforeEach((done) => {
                handler(done);
            });
 
            it('should reply with an error', () => {
                expect(env.result).to.equal('Can only accept JSON');
            });

            it('should have response type 415', () => {
                expect(env.responseCode).to.equal(415);
            });

            it('should not have called Auth.validateNewAccount', () => {
                expect(Auth.validateNewAccount).not.to.have.been.called;
            });

            it('should not have called GameDB.Accounts.add', () => {
                expect(GameDB.Accounts.add).to.not.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when the payload is null', () => {

            beforeEach((done) => {
                env.request.payload = null;
                env.request.headers['content-type'] = 'application/json';
                handler(done);
            });
          
            it('should reply with an error', () => {
                expect(env.result).to.equal('No registration credentials provided');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should not have called Auth.validateNewAccount', () => {
                expect(Auth.validateNewAccount).not.to.have.been.called;
            });

            it('should not have called GameDB.Accounts.add', () => {
                expect(GameDB.Accounts.add).to.not.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when the payload has no email', () => {

            beforeEach((done) => {
                env.request.payload = {
                    email: null
                };

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should reply with an error', () => {
                expect(env.result).to.equal('Cannot create Account without an email address');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should not have called Auth.validateNewAccount', () => {
                expect(Auth.validateNewAccount).not.to.have.been.called;
            });

            it('should not have called GameDB.Accounts.add', () => {
                expect(GameDB.Accounts.add).to.not.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });

        describe('when your request is valid, and the credentials are also valid', () => {

            beforeEach((done) => {

                env.request.payload = {
                    email: 'goit@goit.com',
                    account_type: 'test'
                };

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should call Auth.validateNewAccount', () => {
                expect(Auth.validateNewAccount).to.have.been.called;
            });

            it('should call GameDB.Accounts.add', () => {
                expect(GameDB.Accounts.add).to.have.been.called;
            });

            it('should reply with the newly created Account\'s UID', () => {
                expect(env.result.account_uid).to.not.be.undefined;
            });

            it('should have response type 200', () => {
                expect(env.responseCode).to.equal(200);
            });

            it('should have response type application/json', () => {
                expect(env.responseType).to.equal('application/json');
            });

        });

        describe('when the payload is valid, but the credentials are invalid', () => {

            beforeEach((done) => {
                env.request.payload = {
                    email: 'plamp'
                };

                env.request.headers['content-type'] = 'application/json';

                handler(done);
            });

            it('should reply with an error', () => {
                expect(env.result).to.equal('Invalid Email');
            });

            it('should have response type 401', () => {
                expect(env.responseCode).to.equal(401);
            });

            it('should not have called Auth.validateNewAccount', () => {
                expect(Auth.validateNewAccount).to.have.been.called;
            });

            it('should not have called GameDB.Accounts.add', () => {
                expect(GameDB.Accounts.add).to.not.have.been.called;
            });

            it('should have response type text/plain', () => {
                expect(env.responseType).to.equal('text/plain');
            });

        });


    });

});
