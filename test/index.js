'use strict';

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

const Hapi = require('hapi');

const ServerService = require('../lib/services/ServerService');

before(function () {

});

describe('running the server', function () {

    let mockServer = {};

    beforeEach(function () {
        sinon.stub(Hapi, 'Server').returns(mockServer);
    });

    afterEach(function () {
        Hapi.Server.restore();
    });

    it('should set the host and port, add the routes, and start the server');

});
