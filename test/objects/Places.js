'use strict';

const chai = require('chai');
const expect = chai.expect;

const Place = require('../../lib/objects/things/Place');
const GameDB = require('../../lib/services/GameDBService');
const config = require('../../lib/services/ConfigService');

describe('Places object', function () {

    let place;
    let dupeLobby = new Place('Lobby', 'The Test Lobby', 1200);
    dupeLobby.uid = config.option('lobby-id');
    let goodlist = [
        new Place('Taco Bell', 'a', 10),
        new Place('A.J. Foyt\'s Ass', 'b', 1),
        dupeLobby
    ];
    
    let badlist = [
        new Place('Taco Bell', 'a', 10),
        new Place('A.J. Foyt\'s Ass', 'b', 1)
    ];

    before(function () {
        config.init('INFO');
        GameDB.init();
    });

    beforeEach(function () {
        GameDB.reset();
        place = new Place('test place', 'test place', 2);
    });

    describe('lobby', function () {

        it('is created by default', function () {
            expect(GameDB.Places.Lobby).to.not.equal(undefined);
        });

        it('is in the Ether', function () {
            expect(GameDB.Places.Lobby.location).to.deep.equal({
                realm: GameDB.Realms.Ether.uid,
                X: 0,
                Y: 0
            });
        });

        it('should use the config defined uid', function () {
            expect(GameDB.Places.Lobby.uid).to.equal(config.option('lobby-id'));
        });

    });

    describe('byName function', function () {

        it('returns empty if no such place exists', function () {
            expect(GameDB.Places.byName('blah')).to.deep.equal([]);
        });

        it('returns the place if it exists', function () {
            GameDB.Places.add(place);
            expect(GameDB.Places.byName('test place')).to.deep.equal([place]);
        });

        it('returns all places with that name', function () {
            let newPlace = new Place('test place', 'second test place', 2);
            GameDB.Places.add(place);
            GameDB.Places.add(newPlace);

            expect(GameDB.Places.byName('test place')).to.deep.equal([
                place,
                newPlace
            ]);
        });

    });

    describe('add function', function () {

        it('adds a valid place', function () {
            expect(function () {
                GameDB.Places.add(place);
            }).to.not.throw(Error);

            expect(GameDB.Places.byId(place.uid)).to.equal(place);
        });

        it('throws an error if you try to add something that is not a place', function () {
            expect(function () {
                GameDB.Places.add({name: 'test', description: 'test', type: 'Place'});
            }).to.throw(Error, 'Cannot add object of type object to GameDB.Places');
        });

        it('does not let you add the same place more than once', function () {
            expect(function () {
                GameDB.Places.add(place);
                GameDB.Places.add(place);
            }).to.throw(Error, 'Place with uid ' + place.uid + ' already exists');
        });

    });

    describe('totalCount', function () {

        it('should be one if you have not added any places', function () {
            expect(GameDB.Places.totalCount).to.equal(1);
        });

        it('should be two if a single place has been added', function () {
            GameDB.Places.add(place);
            expect(GameDB.Places.totalCount).to.equal(2);
        });

    });

    describe('all function', function () {

        it('should return a list of every valid location', function () {
            expect(GameDB.Places.all()).to.deep.equal([GameDB.Places.Lobby]);
            GameDB.Places.add(place);
            expect(GameDB.Places.all()).to.deep.equal([
                GameDB.Places.Lobby,
                place
            ]);

        });

    });

    describe('remove function', function() {
        it('should remove the place with a given valid uid', function() {
            
            GameDB.Places.add(place);
            expect(GameDB.Places.all()).to.deep.equal([
                GameDB.Places.Lobby,
                place
            ]);

            GameDB.Places.remove(place.uid);
            expect(GameDB.Places.all()).to.deep.equal([
                GameDB.Places.Lobby
            ]);
        });

        it('should not remove a nonexistent place with a nonexistent uid', () => {
            expect(function() {
                GameDB.Places.remove('goit');
            }).to.throw(Error, 'Place with uid goit never existed');
        });

        it('should not be allowed to remove the lobby', () => {
            expect(function() {
                GameDB.Places.remove('LOBBY');
            }).to.throw(Error, 'Cannot remove the lobby');
        });
    });

    describe('restoreFromList function', function() {
        
        it('should replace the spawned lobby with the file\'s lobby', () => {
            
            expect(GameDB.Places.all()).to.deep.equal([
                GameDB.Places.Lobby
            ]);

            GameDB.Places.restoreFromList(goodlist);  

            expect(GameDB.Places.byId('LOBBY')).to.deep.equal(dupeLobby);
        });

        it('should replace the server\'s places with the file\'s places', () => {
            
            expect(GameDB.Places.all()).to.deep.equal([
                GameDB.Places.Lobby
            ]);

            GameDB.Places.restoreFromList(goodlist);  

            expect(GameDB.Places.all()).to.deep.equal(goodlist);

            expect(GameDB.Places.totalCount).to.equal(goodlist.length);
        });

        it('should not restore with an empty list', () => {
            expect(function(){
                GameDB.Places.restoreFromList([]);
            }).to.throw(Error, 'Cannot restore Server\'s places from an empty list');
        });

        it('should not allow restore without a lobby in the list', () => {
            expect(function(){
                GameDB.Places.restoreFromList(badlist);
            }).to.throw(Error, 'Cannot restore Server\'s places without a lobby');
        });


    });

    describe('byId function', function () {

        it('should return undefined if no Place with that id exists', function () {
            expect(GameDB.Places.byId('bad')).to.be.undefined;
        });

        it('should return a matching place', function () {
            GameDB.Places.add(place);
            expect(GameDB.Places.byId(GameDB.Places.Lobby.uid)).to.equal(GameDB.Places.Lobby);
            expect(GameDB.Places.byId(place.uid)).to.equal(place);
        });

    });

});
