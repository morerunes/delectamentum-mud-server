'use strict';

const _ = require('lodash');
const chai = require('chai');
const expect = chai.expect;

const Account = require('../../lib/objects/things/Account');
const GameDB = require('../../lib/services/GameDBService');
const config = require('../../lib/services/ConfigService');

config.init('ERROR');

describe('The Accounts object\'s', function () {

    let accList = [
        new Account('a@a.com', 'goit'),
        new Account('b@b.com', 'goit')
    ];

    before(function () {
        config.init('INFO');
    });

    beforeEach(function () {
        GameDB.reset();
    });

    describe('add function', function () {
        let a = new Account('goit@plamp.com', 'a'),
            b = new Account('goit@plamp.com', 'a');

        it('adds a valid account', function () {
            expect(function () {
                GameDB.Accounts.add(a);
            }).to.not.throw(Error);

            expect(GameDB.Accounts.byId(a.uid)).to.equal(a);
        });

        it('throws an error if you try to add a non-Account object', function () {
            expect(function () {
                GameDB.Accounts.add({goit: 'wat', honk: 'nope'});
            }).to.throw(Error, 'Cannot add object of type object to Game.Accounts');
        });

        it('does not let you add an account with an email already associated with another account', function () {
            expect(function () {
                GameDB.Accounts.add(a);
                GameDB.Accounts.add(b);
            }).to.throw(Error, 'Account with email goit@plamp.com already exists');
        });

        it('does not allow you to add the same account more than once', function () {
            expect(function () {
                GameDB.Accounts.add(a);
                GameDB.Accounts.add(a);
            }).to.throw(Error, 'Account with UID ' + a.uid + ' already exists');
        });
    });

    describe('remove function', function () {

        let testAcc = new Account('goit@goit.com','goit'),
            testVictim = new Account('goit@plamp.com');

        it('shouldn\'t allow a non-user account to be deleted', function () {

            GameDB.Accounts.add(testAcc);
            expect(() => {
                GameDB.Accounts.remove(testAcc.uid);
            }).to.throw(Error, 'Cannot remove non-standard user accounts');

        });

        it('should remove the account with a given valid uid', function () {

            GameDB.Accounts.add(testAcc);
            GameDB.Accounts.add(testVictim);
            expect(GameDB.Accounts.all()).to.deep.equal([
                testAcc,
                testVictim
            ]);

            GameDB.Accounts.remove(testVictim.uid);
            expect(GameDB.Accounts.all()).to.deep.equal([
                testAcc
            ]);
        });

        it('should not remove a nonexistent player with a nonexistent uid', () => {
            expect(function () {
                GameDB.Accounts.remove('goit');
            }).to.throw(Error, 'Account with uid goit never existed');
        });
    });

    describe('restoreFromList function', function () {

        let inputAccountList;
        let initialAccountList;

        describe('when given an empty list', function () {

            beforeEach(function () {
                inputAccountList = [];
                GameDB.Accounts.add(new Account('test','test'));
                initialAccountList = GameDB.Accounts.all();
            });

            it('should not have modified the game accounts list', function () {
                expect(() => {
                    GameDB.Accounts.restoreFromList(inputAccountList);
                }).to.throw(Error, 'Cannot restore Server\'s accounts from an empty list');

                expect(GameDB.Accounts.all()).to.deep.equal(initialAccountList);
            });

        });

        describe('when given a list of valid accounts', function () {

            beforeEach(function () {
                inputAccountList = [
                    new Account('replacement test','test'),
                    new Account('another one', 'test')
                ];

                GameDB.Accounts.add(new Account('test', 'goit'));
                initialAccountList = GameDB.Accounts.all();

                GameDB.Accounts.restoreFromList(inputAccountList);
            });

            it('should replace the server\'s accounts with the list\'s accounts', () => {
                expect(GameDB.Accounts.all()).to.deep.equal(inputAccountList);
            });

        });

    });

    describe('totalCount attribute', function () {
        it('should be zero if you haven\'t added any accounts', function () {
            expect(GameDB.Accounts.totalCount).to.equal(0);
        });

        it('should be one after adding a single account', function () {
            GameDB.Accounts.add(new Account('a@a.com', 'goit'));
            expect(GameDB.Accounts.totalCount).to.equal(1);
        });
    });

    describe('all function', function () {

        it('should return a list of every valid account', function () {
            expect(GameDB.Accounts.all()).to.deep.equal([]);

            GameDB.Accounts.add(accList[0]);
            GameDB.Accounts.add(accList[1]);

            expect(GameDB.Accounts.all()).to.deep.equal(accList);

        });
    });

    describe('search function', function () {
        let env;

        beforeEach(() => {
            env = {
                error: null,
                model: null,
                terms: {},
                search: false
            };
        });

        function doSearch(done) {
            try {
                env.model = GameDB.Accounts.search(env.terms, env.search);
            } catch (err) {
                env.error = err;
            } finally {
                done();
            }
        }

        describe('when called with no input', function () {

            beforeEach((done) => {
                env.terms = null;
                doSearch(done);
            });

            it('should cause an error', function () {
                expect(env.error.message).to.equal('Cannot perform search on empty input');
            });

            it('should not return anything', function () {
                expect(env.model).to.equal(null);
            });

        });

        describe('when called with invalid search terms', function () {

            beforeEach(() => {
                env.terms.goit = false;
                env.terms.plamp = false;
                env.terms.honk = 'wat';
            });

            describe('and \'search\' is true', function () {
                beforeEach((done) => {
                    env.search = true;
                    doSearch(done);
                });

                it('should return an empty list', function () {
                    expect(env.model).to.deep.equal([]);
                });

                it('should not cause an error', function () {
                    expect(env.error).to.equal(null);
                });
            });

            describe('and \'search\' is false', function () {
                beforeEach((done) => {
                    doSearch(done);
                });

                it('should return an empty list', function () {
                    expect(env.model).to.deep.equal([]);
                });

                it('should not cause an error', function () {
                    expect(env.error).to.equal(null);
                });
            });
        });

        describe('when called with valid search terms', function () {

            let accs;

            beforeEach(() => {
                accs = [
                    new Account('a@goit.moe', 'test1'),
                    new Account('ab@goit.moe', 'test2'),
                    new Account('abc@goit.moe', 'test3'),
                    new Account('b@goit.moe', 'test1'),
                    new Account('bc@goit.moe', 'test2'),
                    new Account('bcd@goit.moe', 'test3')
                ];

                _.each(accs, (acc) => {
                    GameDB.Accounts.add(acc);
                });

            });

            describe('when \'search\' is true', function () {
                beforeEach((done) => {
                    env.terms.email = 'GOIT.MOE';
                    env.terms.account_type = '1';
                    env.search = true;

                    doSearch(done);
                });

                it('should intersect terms ignoring case', function () {
                    expect(env.model).to.deep.equal([
                        accs[0],
                        accs[3]
                    ]);
                });

                it('should not cause an error', function () {
                    expect(env.error).to.equal(null);
                });

            });

            describe('when \'search\' is false', function () {

                beforeEach((done) => {
                    env.terms.email = 'a@goit.moe';
                    env.terms.account_type = 'test1';
                    env.search = false;

                    doSearch(done);
                });

                it('should intersect terms', function () {
                    expect(env.model).to.deep.equal([
                        accs[0]
                    ]);
                });

                it('should not cause an error', function () {
                    expect(env.error).to.equal(null);
                });

            });

            describe('when there are multiple matches', () => {

                beforeEach((done) => {
                    env.terms.email = 'b';
                    env.search = true;

                    doSearch(done);
                });

                it('should return all results', () => {
                    expect(env.model).to.deep.equal([
                        accs[1],
                        accs[2],
                        accs[3],
                        accs[4],
                        accs[5]
                    ]);
                });

                it('should not cause an error', () => {
                    expect(env.error).to.equal(null);
                });

            });

        });
    });
});
