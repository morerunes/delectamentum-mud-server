'use strict';

const chai = require('chai');
const expect = chai.expect;

const config = require('../../lib/services/ConfigService');
const GameDB = require('../../lib/services/GameDBService');
const Realms = require('../../lib/objects/Realms');
const Cartograph = require('../../lib/objects/things/Cartograph');

describe('the Realms object', function () {

    let realms, cartograph;

    let c2 = new Cartograph('goit', 'goit');

    let dupeEther = new Cartograph('Ether', 'The Test Ether');
    dupeEther.uid = config.option('ether-id');


    let goodlist = [
        dupeEther,
        c2  
    ];
    let badlist = [
        c2
    ];

    before(function () {
        config.init('INFO');
    });

    beforeEach(function () {
        realms = new Realms();
        cartograph = new Cartograph('test', 'test');
    });

    describe('add function', function () {

        it('should only accept Cartograph objects', function () {
            expect(function () {
                realms.add('not a Cartograph');
            }).to.throw(Error, 'Cannot add thing of type string to Realms');

            expect(realms.totalCount).to.equal(1);
        });

        it('should allow you to add a Cartograph', function () {
            expect(function () {
                realms.add(cartograph);
            }).to.not.throw(Error);

            expect(realms.totalCount).to.equal(2);
        });

        it('should not allow you to add the same Cartograph more than once', function () {
            expect(function () {
                realms.add(cartograph);
                realms.add(cartograph);
            }).to.throw(Error, 'Realm test already exists');

            expect(realms.totalCount).to.equal(2);
        });

    });

    describe('byId function', function () {

        it('should return undefined if there is no Cartograph with that uuid', function () {
            expect(realms.byId()).to.be.undefined;
        });

        it('should return a Cartograph with a matching uuid', function () {
            realms.add(cartograph);
            expect(realms.byId(cartograph.uid)).to.equal(cartograph);
        });

    });

    describe('byName function', function () {

        it('should return undefined if there is no Cartograph with that name', function () {
            expect(realms.byName()).to.deep.equal([]);
        });

        it('should return every Cartograph with a matching name', function () {
            let cartograph2 = new Cartograph('test', 'test2');
            realms.add(cartograph);
            realms.add(cartograph2);
            realms.add(new Cartograph('test3', 'since the name is different, this will not be in results'));

            expect(realms.byName('test')).to.deep.equal([
                cartograph,
                cartograph2
            ]);
        });

    });

    describe('remove function', function() {
        
        beforeEach(() => {
            GameDB.reset();
        });

        it('should remove the realm with a given valid uid', function() {
            
            GameDB.Realms.add(cartograph);
            expect(GameDB.Realms.all()).to.deep.equal([
                GameDB.Realms.Ether,
                cartograph
            ]);

            GameDB.Realms.remove(cartograph.uid);
            expect(GameDB.Realms.all()).to.deep.equal([
                GameDB.Realms.Ether
            ]);
        });

        it('should not remove a nonexistent place with a nonexistent uid', () => {
            expect(function() {
                GameDB.Realms.remove('goit');
            }).to.throw(Error, 'Realm with uid goit never existed');
        });

        it('should not be allowed to remove the Ether', () => {
            expect(function() {
                GameDB.Realms.remove('ETHER');
            }).to.throw(Error, 'Cannot remove the Ether');
        });
    });

    describe('restoreFromList function', function() {

        beforeEach(() => {
            GameDB.reset();
        });
        
        it('should replace the spawned Ether with the file\'s Ether', () => {
            
            expect(GameDB.Realms.all()).to.deep.equal([
                GameDB.Realms.Ether
            ]);

            GameDB.Realms.restoreFromList(goodlist);  

            expect(GameDB.Realms.byId('ETHER')).to.deep.equal(dupeEther);
        });

        it('should replace the server\'s realms with the file\'s realms', () => {
            
            expect(GameDB.Realms.all()).to.deep.equal([
                GameDB.Realms.Ether
            ]);

            GameDB.Realms.restoreFromList(goodlist);  

            expect(GameDB.Realms.all()).to.deep.equal(goodlist);

            expect(GameDB.Realms.totalCount).to.equal(goodlist.length);
        });

        it('should not restore with an empty list', () => {
            expect(function(){
                GameDB.Realms.restoreFromList([]);
            }).to.throw(Error, 'Cannot restore Server\'s realms from an empty list');
        });

        it('should not allow restore without an Ether in the list', () => {
            expect(function(){
                GameDB.Realms.restoreFromList(badlist);
            }).to.throw(Error, 'Cannot restore Server\'s realms without an Ether');
        });


    });

    describe('all function', function () {

        it('should start out containing only the Ether', function () {
            expect(realms.all()).to.deep.equal([realms.Ether]);
        });

        it('should be comprehensive', function () {
            let c2 = new Cartograph('test2', 'test2');
            let c3 = new Cartograph('test', 'test3');
            realms.add(cartograph);
            realms.add(c2);
            realms.add(c3);

            expect(realms.all()).to.deep.equal([
                realms.Ether,
                cartograph,
                c2,
                c3
            ]);
        });

    });

    describe('Ether', function () {

        it('should be a Cartograph', function () {
            expect(realms.Ether instanceof Cartograph).to.be.true;
        });

        it.skip('should contain the lobby', function () {
            expect(realms.Ether.at(0, 0)).to.equal(GameDB.Places.Lobby);
        });

        it('should use the config defind uuid', function () {
            expect(realms.Ether.uid).to.equal(config.option('ether-id'));
        });

    });

});
