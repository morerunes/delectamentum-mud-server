'use strict';

const chai = require('chai');
const expect = chai.expect;

const Cartograph = require('../../../lib/objects/things/Cartograph');
const Thing = require('../../../lib/objects/things/Thing');
const Place = require('../../../lib/objects/things/Place');
const config = require('../../../lib/services/ConfigService');

describe('The Cartograph object', function () {
    let place, testMap;

    before(function () {
        config.init('INFO');
    });

    beforeEach(function () {
        place = new Place('test', 'test place', 2);
        testMap = new Cartograph('test', 'test');
    });

    it('should be an instance of Thing', function () {
        let testMap = new Cartograph('test', 'test');

        expect(testMap instanceof Cartograph).to.equal(true);
        expect(testMap instanceof Thing).to.equal(true);
        expect(testMap instanceof Object).to.equal(true);
    });

    it('Should have a name and description', function () {
        let testMap = new Cartograph('test', 'a test');
        expect(testMap.name).to.equal('test');
        expect(testMap.description).to.equal('a test');
    });

    it('should have a type of Cartograph', function () {
        let testMap = new Cartograph('test');
        expect(testMap.type).to.equal('Cartograph');
    });

    describe('the addPlace function', function () {
        it('should exist', function () {
            expect(testMap.addPlace).to.be.a('function');
        });

        it('should store places, retrieved by the at function', function () {
            expect(testMap.at(0, 0)).to.equal(undefined);
            testMap.addPlace(place, 0, 0);
            expect(testMap.at(0, 0)).to.equal(place);
        });

        it('should not let you add things that are not places to it', function () {
            expect(function () {
                testMap.addPlace('not a real place', 0, 0);
            }).to.throw(Error, 'Cannot add thing of type string to Cartograph');
        });
    });

    describe('the at function', function () {
        it('should exist', function () {
            expect(testMap.at).to.be.a('function');
        });

        it('should return undefined if nothing is there', function () {
            expect(testMap.at(0,0)).to.equal(undefined);
        });

        it('should return a room if it exists', function () {
            testMap.addPlace(place, 0, 0);
            expect(testMap.at(0, 0)).to.equal(place);
        });
    });
});
