'use strict';

const PlayerSpec = require('../../../lib/objects/things/PlayerSpec');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const expect = chai.expect;

describe('A Player specification', () => {

    let stdAttSet = [
        'strength',
        'agility',
        'accuracy',
        'intel',
        'charisma'
    ];

    let spec;

    describe('when initialized', () => {

        beforeEach(() => {
            spec = new PlayerSpec();
        });

        it('contains the standard set of attributes', () => {
            expect(() => {
                for(let idx = 0; idx < stdAttSet.length; idx++)
                    if(!spec.attribs[stdAttSet[idx]])
                        throw new Error('Missing ' + stdAttSet[idx]);
            }).to.not.throw(Error);
        });

        it('has its level and experience set to one and zero, respectively', () => {
            expect(spec.level).to.equal(1);
            expect(spec.xp).to.equal(0);
        });

        it('has an empty skill set', () => {
            expect(spec.skills).to.deep.equal({});
        });
        
        it('has an empty accessory set', () => {
            expect(spec.accessories).to.deep.equal({});
        });
    });

});
