'use strict';

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

const GameDB = require('../../../lib/services/GameDBService');
const Thing = require('../../../lib/objects/things/Thing');
const Player = require('../../../lib/objects/things/Player');
const log = require('../../../lib/services/LogService');
const config = require('../../../lib/services/ConfigService');
const Place = require('../../../lib/objects/things/Place');

describe('The Player object', function () {

    let player;

    before(function () {
        config.init('INFO');
    });

    beforeEach(function () {
        player = new Player('test');
    });

    it('should be an instance of Thing', function () {
        expect(player instanceof Player).to.equal(true);
        expect(player instanceof Thing).to.equal(true);
        expect(player instanceof Object).to.equal(true);
    });

    describe('constructor', function () {

        it('should set the name to the provided name', function () {
            expect(player.name).to.equal('test');
        });

        it('should generate a description if one is not provided', function () {
            expect(player.description).to.equal('A Player named test');
        });

        it('should allow you to specify a description', function () {
            player = new Player('test', 'test');
            expect(player.description).to.equal('test');
        });

        it.skip('should put the player into the Lobby', function () {
            expect(player.location).to.equal(GameDB.Places.Lobby.uid);
        });

        it('should throw an error if you try to make a player with no name', function () {
            sinon.stub(log, 'error');

            expect(function () {
                new Player();
            }).to.throw(Error, 'Cannot instantiate new Thing(Player) with no name');

            expect(log.error).to.have.been.calledWith('Tried to create a Thing with no name');

            log.error.restore();
        });

    });

    describe('moveTo function', function () {

        it.skip('should only let you move to a place that is in Game.Places', function () {
            let place = new Place('test', 'test', 2);

            expect(function () {
                player.moveTo(place.uid);
            }).to.throw(Error, place.uid + ' is not a valid Place');
        });

        it('should update the player location', function () {
            let place = new Place('test', 'test', 2);

            GameDB.Places.add(place);
            player.moveTo(place.uid);

            expect(player.location).to.equal(place.uid);
        });

    });

});
