'use strict';

/*
 *
 * Account.js
 *
 * An entity that is identified by email address. Contains the UIDs of players created under the specific email
 *
 */

const chai = require('chai');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

const GameDB = require('../../../lib/services/GameDBService');
const Account = require('../../../lib/objects/things/Account');
const Player = require('../../../lib/objects/things/Player');
const config = require('../../../lib/services/ConfigService');

describe('The Account object', function() {
    
    let acc;
    let acc2;

    before(() => {
        config.init('CRITICAL');
    });

    beforeEach(function() {
        acc = new Account('goit@goit.com');
        acc2 = new Account('plamp@goit.com','test');
    });  

    afterEach(function() {
        GameDB.reset();
    });

    describe('\'s constructor', function() {
      
        it('should set the email to the provided email', function() {
            expect(acc.email).to.equal('goit@goit.com');
        });  

        it('should set the type to \'user\' by default when not given an additional type', function() {
            expect(acc.account_type).to.equal('user');
        });

        it('should set the type to the provided addtional type', function() {
            expect(acc2.account_type).to.equal('test');
        });

        it('should throw an error if an account is created without an email', function() {

            expect(() => {
                new Account();
            }).to.throw(Error, 'Cannot create Account without an email address');
        });

    });

    describe('\'s addPlayer function', function() {

        let player = new Player('goit','goit'),
            dummy;  //intentionally left undefined

        beforeEach(function() {
            GameDB.Players.add(player);
            acc = new Account('goit@goit.com', 'a');
        });

        describe('given a valid distinct player UID', function() {

            beforeEach(function() {
                acc.addPlayer(player);
            });

            it('should add the player UID to the account\'s UID list', function() {
                expect(acc.getPlayer(player)).to.equal(player.uid);
            });
            
        });

        describe('given a duplicate player UID', function() {

            beforeEach(function() {
                acc.addPlayer(player);
            });

            it('should not add the player UID to the account\'s UID list', function() {
                expect(() => {
                    acc.addPlayer(player);
                }).to.throw(Error, 'Player UID ' + player.uid + ' already associated with this account');
            });

        });

        describe('given an empty player UID/undefined thing', function() {

            it('should not add the empty UID', function() {
                expect(() => {
                    acc.addPlayer('');
                }).to.throw(Error, 'Cannot give an Account an empty player UID');

                expect(() => {
                    acc.addPlayer(dummy);
                }).to.throw(Error, 'Cannot give an Account an empty player UID');
            });

        });

    });

});
