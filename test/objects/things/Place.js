'use strict';

const chai = require('chai');
const expect = chai.expect;

const Place = require('../../../lib/objects/things/Place');
const GameDB = require('../../../lib/services/GameDBService');
const config = require('../../../lib/services/ConfigService');

describe('the Place object', function () {

    let place;

    before(function () {
        config.init('INFO');
    });

    beforeEach(function () {
        place = new Place('test', 'test', 1);
    });

    describe('instantiation', function () {

        it('should require a size', function () {

            expect(function () {
                new Place('testName', 'testDescription');
            }).to.throw(Error, 'Cannot create a Place with no size');

        });

        it('should start in the Ether', function () {
            expect(place.location.realm).to.equal(GameDB.Realms.Ether.uid);
        });

    });

});
