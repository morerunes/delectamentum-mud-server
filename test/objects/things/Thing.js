'use strict';

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const _ = require('lodash');

const log = require('../../../lib/services/LogService');
const config = require('../../../lib/services/ConfigService');
const Thing = require('../../../lib/objects/things/Thing');

describe('A Thing', function () {

    before(function () {
        config.init('INFO');
    });

    describe('instantiation', function () {

        it('should have a name and description', function () {
            let testThing = new Thing('Test Thing', 'This thing is for testing');

            expect(testThing.name).to.equal('Test Thing');
            expect(testThing.description).to.equal('This thing is for testing');
        });

        it('should allow you to specify a type', function () {
            let testThingBook = new Thing('test', null, 'Book');
            expect(testThingBook.type).to.equal('Book');
        });

        it('should have a type of Thing by default', function () {
            expect((new Thing('test')).type).to.equal('Thing');
        });

        it('should have a UUID', function () {
            expect(typeof (new Thing('test')).uid).to.equal('string');
        });

        it('should complain when you create a thing with no name', function () {
            sinon.stub(log, 'error');

            expect(function () {
                new Thing();
            }).to.throw(Error, 'Cannot instantiate new Thing(Thing) with no name');

            expect(log.error).to.have.been.calledWith('Tried to create a Thing with no name');

            log.error.restore();
        });

        it('should use the correct Thing type in error messages', function () {
            sinon.stub(log, 'error');

            expect(function () {
                new Thing(null, null, 'Book');
            }).to.throw(Error, 'Cannot instantiate new Thing(Book) with no name');

            expect(log.error).to.have.been.calledWith('Tried to create a Thing with no name');

            log.error.restore();
        });

    });
    
    describe('the give function', function () {
        
        let thing;
        
        beforeEach(function () {
            thing = new Thing('test');
        });
        
        it('should exist', function () {
            expect(thing.give).to.be.a('function');
        });

        it('should add it to the contents if its type is accepted', function () {
            let otherThing = new Thing('book', null, 'Book');

            thing.accept(new Thing('test', null, 'Book'));
            thing.give(otherThing);

            expect(_.includes(thing.contents, otherThing)).to.equal(true);
        });

        it('should throw an error if its type is not accepted', function () {
            expect(function () {
                thing.give(new Thing('book', null, 'Book'));
            }).to.throw(Error, 'Cannot add Thing of type Book to test');
        });

        it('should throw an error if it is not a thing', function () {
            expect(function () {
                thing.give('honk');
            }).to.throw(Error, 'Can only give Things to Things, but tried to give a string');
        });

    });

    describe('the accept function', function () {

        let thing;

        beforeEach(function () {
            thing = new Thing('test');
        });

        it('should exist', function () {
            expect(thing.accept).to.be.a('function');
        });

        it('should accept items it is told to accept', function () {
            expect(function () {
                thing.accept(new Thing('test', null, 'Book'));
            }).not.to.throw(Error);

            expect(thing.accepts(new Thing('test2', null, 'Book'))).to.be.true;
        });

        it('should not accept things that are not things', function () {
            expect(function () {
                thing.accept('test');
            }).to.throw(Error, 'A Thing can only accept other Things');
        });

    });

    describe('public view function', () => {
        let thing, thingPV, forbiddenValues;

        beforeEach(() => {
            thing = new Thing('test','should be visible', 'should NOT be visible');
            thingPV = thing.publicView();
            forbiddenValues = new Set();

            forbiddenValues.add(thing.type);
            forbiddenValues.add(thing.uid);
            forbiddenValues.add(thing.contents);
            forbiddenValues.add(thing.acceptedThingTypes);
        });

        it('should exist', () => {
            expect(thing.publicView).to.be.a('function');
        });

        it('should return the name and description', () => {
            expect(thingPV.name).to.equal('test');
            expect(thingPV.description).to.equal('should be visible');
        });

        it('should not return anything that is not the name or description', () => {
            expect(() => {
                let thingValues = _.values(thingPV);
                for(let i = 0; i < thingValues.length; i++)                
                    if(forbiddenValues.has(thingValues[i]))
                        throw new Error();
            }).to.not.throw(Error);
        });
    });

    describe('take [another thing] function', () => {
        let t1, t2;

        beforeEach(() => {
            t1 = new Thing('test','test');
            t2 = new Thing('diamond','fancy thing','stone');

            t1.acceptedThingTypes.push(t2.type);
            t1.give(t2);
        });

        it('should return the desired thing with the given name', () => {
            expect(t1.take(t2.name)).to.deep.equal(t2);
        });

        it('should remove the taken thing from the thing that owned it', () => {
            t1.take(t2.name);
            expect(t1.take(t2.name)).to.be.undefined;
        });

    });

});
