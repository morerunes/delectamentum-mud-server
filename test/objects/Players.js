'use strict';

const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chai = require('chai');
chai.use(sinonChai);
const expect = chai.expect;
const _ = require('lodash');

const GameDB = require('../../lib/services/GameDBService');
const Player = require('../../lib/objects/things/Player');
const config = require('../../lib/services/ConfigService');

describe('Players object', function () {

    let player,
        sandbox;

    let testPlayer = new Player('Kasey', 'Old has-been'),
        testVictim = new Player('Gaston', 'goit');

    let testlist = [
        testPlayer,
        testVictim
    ];

    before(function () {
        config.init('INFO');
    });

    beforeEach(function () {
        player = new Player('test');
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        GameDB.reset();
        sandbox.restore();
    });

    describe('add function', function () {

        it('stores a player', function () {
            GameDB.Players.add(player);
            expect(GameDB.Players.byId(player.uid)).to.equal(player);
        });

        it('increments totalCount if it adds a player', function () {
            GameDB.Players.add(player);
            expect(GameDB.Players.totalCount).to.equal(1);
        });

        it('does not store the same player more than once', function () {
            expect(function () {
                GameDB.Players.add(player);
                GameDB.Players.add(player);
            }).to.throw(Error, 'Player id ' + player.uid + ' already exists.');
        });

        it('does not store more than one player with the same name', function () {
            expect(function () {
                GameDB.Players.add(player);
                GameDB.Players.add(new Player(player.name, player.description));
            }).to.throw(Error, 'Player name ' + player.name + ' is already taken');
        });

        it('does not store things that are not players', function () {
            expect(function () {
                GameDB.Players.add('goit');
            }).to.throw(Error, 'Cannot add object of type string to GameDB.Players');
        });

    });

    describe('remove function', function() {

        it('should remove the player with a given valid uid', function() {
            
            GameDB.Players.add(testPlayer);
            GameDB.Players.add(testVictim);
            expect(GameDB.Players.all()).to.deep.equal([
                testPlayer,
                testVictim
            ]);

            GameDB.Players.remove(testVictim.uid);
            expect(GameDB.Players.all()).to.deep.equal([
                testPlayer
            ]);
        });

        it('should not remove a nonexistent player with a nonexistent uid', () => {
            expect(function() {
                GameDB.Players.remove('goit');
            }).to.throw(Error, 'Player with uid goit never existed');
        });
    });

    describe('restoreFromList function', function() {
        
        it('should replace the server\'s players with the list\'s players', () => {
            
            GameDB.Players.restoreFromList(testlist);  

            expect(GameDB.Players.all()).to.deep.equal(testlist);

            expect(GameDB.Players.totalCount).to.equal(testlist.length);
        });

        it('should not restore with an empty list', () => {
            expect(function(){
                GameDB.Players.restoreFromList([]);
            }).to.throw(Error, 'Cannot restore Server\'s players from an empty list');
        });

    });

    describe('all function', function () {
        it('returns every added player in a list', function () {
            let tmpPlayer = new Player('test2');

            GameDB.Players.add(player);
            GameDB.Players.add(tmpPlayer);

            expect(GameDB.Players.all()).to.deep.equal([player, tmpPlayer]);
        });

        it('returns an empty list if there are no players', function () {
            expect(GameDB.Players.all()).to.deep.equal([]);
        });

        it('does not let me mess with the actual Game Players data', function () {
            let tmpPlayer = new Player('tmp');
            GameDB.Players.add(player);
            GameDB.Players.all().push(tmpPlayer);

            expect(GameDB.Players.all()).to.not.contain(tmpPlayer);
            expect(GameDB.Players.totalCount).to.equal(1);
        });
    });

    describe('byId function', function () {
        it('returns a player that exists by id', function () {
            GameDB.Players.add(player);
            expect(GameDB.Players.byId(player.uid)).to.equal(player);
        });

        it('returns undefined if no such player exists', function () {
            expect(GameDB.Players.byId(player.uid)).to.equal(undefined);
        });
    });

    describe('search function', () => {

        let env;

        beforeEach(() => {
            env = {
                error: null,
                model: null,
                terms: {},
                search: false
            };
        });

        function doSearch(done) {
            try {
                env.model = GameDB.Players.search(env.terms, env.search);
            } catch (err) {
                env.error = err;
            } finally {
                done();
            }
        }

        describe('when called with no input', () => {

            beforeEach((done) => {
                env.terms = null;

                doSearch(done);
            });

            it('should cause an error', () => {
                expect(env.error.message).to.equal('Cannot perform search on empty input');
            });

            it('should not have returned data', () => {
                expect(env.model).to.equal(null);
            });

        });

        describe('when searching with invalid terms', () => {

            beforeEach(() => {
                env.terms.wumbo = false;
                env.terms.ackle = false;
                env.terms.name = 'test';
            });

            describe('and search is true', () => {

                beforeEach((done) => {
                    env.search = true;
                    doSearch(done);
                });

                it('should return an empty list', () => {
                    expect(env.model).to.deep.equal([]);
                });

                it('should not have caused an error', () => {
                    expect(env.error).to.equal(null);
                });

            });

            describe('and search is false', () => {

                beforeEach((done) => {
                    doSearch(done);
                });

                it('should return an empty list', () => {
                    expect(env.model).to.deep.equal([]);
                });

                it('should not have caused an error', () => {
                    expect(env.error).to.equal(null);
                });

            });

        });

        describe('when searching for valid terms', () => {

            let players;

            beforeEach(() => {
                players = [
                    new Player('a', 'test1'),
                    new Player('ab', 'test2'),
                    new Player('abc', 'test3'),
                    new Player('b', 'test1'),
                    new Player('bc', 'test2'),
                    new Player('bcd', 'test3'),
                    new Player('c', 'foo1'),
                    new Player('cd', 'foo2'),
                    new Player('cde', 'foo3'),
                    new Player('d', 'foo1'),
                    new Player('de', 'foo2'),
                    new Player('def', 'foo3')
                ];

                _.each(players, (player) => {
                    GameDB.Players.add(player);
                });
            });

            describe('when search is true', () => {

                beforeEach((done) => {
                    env.terms.name = 'B';
                    env.terms.description = '2';
                    env.search = true;

                    doSearch(done);
                });

                it('should intersect terms ignoring case', () => {
                    expect(env.model).to.deep.equal([
                        players[1],
                        players[4]
                    ]);
                });

                it('should not error', () => {
                    expect(!env.error).to.equal(true);
                });

            });

            describe('when search is false', () => {

                beforeEach((done) => {
                    env.terms.name = 'a';
                    env.terms.description = 'test1';
                    env.search = false;

                    doSearch(done);
                });

                it('should intersect terms', () => {
                    expect(env.model).to.deep.equal([
                        players[0]
                    ]);
                });

                it('should not error', () => {
                    expect(!env.error).to.equal(true);
                });

            });

            describe('when there are multiple matches', () => {

                beforeEach((done) => {
                    env.terms.description = '1';
                    env.search = true;

                    doSearch(done);
                });

                it('should return all results', () => {
                    expect(env.model).to.deep.equal([
                        players[0],
                        players[3],
                        players[6],
                        players[9]
                    ]);
                });

                it('should not error', () => {
                    expect(!env.error).to.equal(true);
                });

            });

        });

    });

});
