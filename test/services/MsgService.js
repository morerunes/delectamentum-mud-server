'use strict';

const Promise = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const _ = require('lodash');
const expect = chai.expect;

const Hapi = require('hapi');
const SocketIO = require('socket.io');
const SocketIOClient = require('socket.io-client');

const MsgSvc = require('../../lib/services/MsgService');
const Account = require('../../lib/objects/things/Account');
const Player = require('../../lib/objects/things/Player');
const PlayerSpec = require('../../lib/objects/things/PlayerSpec');
const GameDB = require('../../lib/services/GameDBService');
const Game = require('../../lib/services/GameService');
const Auth = require('../../lib/services/AuthService');


describe('The Message service', () => {

    let sandbox,
        server,
        testSocket,
        testPlayer,
        testAccount;

    before(() => {
        GameDB.init();
        server = new Hapi.Server();
        server.connection({
            port: 8000
        });

        server.start(() => {
            MsgSvc.start(server.listener);
        });
    });

    beforeEach(() => {
        testPlayer = new Player('Goit', 'a plamping goit', new PlayerSpec());
        testAccount = new Account('honk@plamp.com', 'admin');
        GameDB.Accounts.add(testAccount);
        GameDB.Players.add(testPlayer);
        testAccount.addPlayer(testPlayer);
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        GameDB.reset();
        sandbox.restore();
    });

    describe('when a new user connects', () => {

        let newUser;

        beforeEach((done) => {
            testSocket = SocketIOClient('http://localhost:8000');
            testSocket.on('welcome', () => {
                done();
            });
        });

        it('tracks the initially un-logged in connection\'s Account and Player UIDs', () => {
            let connCopy = MsgSvc.inspectConnection(testSocket.id);
            expect(connCopy).to.deep.equal({
                socket_id: testSocket.id,
                player: 'not_logged_in',
                account: 'not_logged_in'
            });
        });
    });

    describe('when a user attempts to registers a new Account by typing \'register\'', () => {

        let response;

        describe('with something not even resembling an email address', () => {

            beforeEach((done) => {

                sandbox.stub(Auth, 'validateNewAccount').returns({
                    isValidAccount: false,
                    err_message: 'test'
                });

                sandbox.spy(GameDB.Accounts, 'byEmail');
                sandbox.spy(GameDB.Accounts, 'add');

                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('register:email', () => {
                        testSocket.emit('register:email', 'I am a plamping goit!');
                    });

                    testSocket.on('register:email:invalid', (r) => {
                        response = r;
                        done();
                    });

                    testSocket.emit('register');
                });
            });

            it('responds to the user stating the attempt failed', () => {
                expect(response).to.equal('test');
            });

            it('calls the Authentication service\'s Account validator', () => {
                expect(Auth.validateNewAccount).to.be.called;
            });

            it('checks the entity database for an Account', () => {
                expect(GameDB.Accounts.byEmail).to.be.calledWith('I am a plamping goit!');
            });

            it('does not create a new Account/add the invalid account to the entity database', () => {
                expect(GameDB.Accounts.add).to.not.be.called;
                expect(GameDB.Accounts.byEmail('I am a plamping goit!')).to.be.undefined;
            });

        });

        describe('with a currently unregistered email', () => {

            beforeEach((done) => {

                sandbox.stub(Auth, 'validateNewAccount').returns({
                    isValidAccount: true
                });
                sandbox.spy(GameDB.Accounts, 'byEmail');
                sandbox.spy(GameDB.Accounts, 'add');

                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('register:email', () => {
                        testSocket.emit('register:email', 'plamp@plampingthegoit.com');
                    });

                    testSocket.on('register:email:valid', (r) => {
                        response = r;
                        done();
                    });

                    testSocket.emit('register');
                });
            });

            it('responds to the user stating the attempt succeeded', () => {
                expect(response).to.be.a('String');
            });

            it('calls the Authentication service\'s Account validator', () => {
                expect(Auth.validateNewAccount).to.be.called;
            });

            it('checks the entity database for an Account under the given email', () => {
                expect(GameDB.Accounts.byEmail).to.be.calledWith('plamp@plampingthegoit.com');
            });

            it('creates a new Account which is added to the entity database', () => {
                expect(GameDB.Accounts.add).to.be.calledOnce;
                expect(GameDB.Accounts.byEmail('plamp@plampingthegoit.com')).to.not.be.undefined;
            });

        });

        describe('with an already registered email', () => {

            beforeEach((done) => {

                sandbox.spy(Auth, 'validateNewAccount');
                sandbox.spy(GameDB.Accounts, 'byEmail');
                sandbox.spy(GameDB.Accounts, 'add');

                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('register:email', () => {
                        testSocket.emit('register:email', testAccount.email);
                    });

                    testSocket.on('register:email:dupe', (r) => {
                        response = r;
                        done();
                    });

                    testSocket.emit('register');
                });
            });

            it('responds to the user stating the attempt failed', () => {
                expect(response).to.be.a('String');
            });

            it('does not call the Authentication service\'s Account validator', () => {
                expect(Auth.validateNewAccount).to.not.be.called;
            });

            it('checks the entity database for an Account under the given email', () => {
                expect(GameDB.Accounts.byEmail).to.be.calledWith(testAccount.email);
            });

            it('does not create a new Account/add the dupe Account to the entity database', () => {
                expect(GameDB.Accounts.add).to.not.be.called;
                expect(GameDB.Accounts.byEmail(testAccount.email)).to.not.be.undefined;
            });

        });

    });

    describe('when a user attempts to log into the MUD by literally saying \'login\'', () => {

        let response;

        describe('then supplies a bunk/unregistered/nonexistant email', () => {

            beforeEach((done) => {

                sandbox.spy(Game, 'attemptAddPlayer');
                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', 'plamp@goittheplamp.com');
                    });

                    testSocket.on('login:email:fail', (r) => {
                        response = r;
                        done();
                    });
                });

                testSocket.emit('login');

            });

            it('responds to the user stating that the provided email cannot be used to login', () => {
                expect(response).to.be.a('String');
            });

            it('maintains the connection\'s unlogged MUD state', () => {
                let connectionState = MsgSvc.inspectConnection(testSocket.id);
                expect(connectionState.account).to.equal('not_logged_in');
                expect(connectionState.player).to.equal('not_logged_in');
            });

            it('does not tell the Game service to spawn a player', () => {
                expect(Game.attemptAddPlayer).to.not.be.called;
            });


        });

        describe('then supplies a registered email that is not logged in', () => {

            beforeEach((done) => {

                sandbox.spy(Game, 'attemptAddPlayer');
                sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
                sandbox.stub(GameDB.Accounts, 'byId').returns(testAccount);
                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });

                    testSocket.on('login:email:success', (r) => {
                        response = r;
                        done();
                    });
                });

                testSocket.emit('login');

            });

            it('responds to the user stating that the provided email cannot be used to login', () => {
                expect(response).to.be.a('String');
            });

            it('checks with the entity database for an Account with the corresponding email', () => {
                expect(GameDB.Accounts.byEmail).to.be.calledWith(testAccount.email);
            });

            it('updates the connection\'s MUD status', () => {
                let state = MsgSvc.inspectConnection(testSocket.id);
                expect(state.account).to.equal(testAccount.uid);
            });

        });

        describe('then supplies a registered email that is already logged in', () => {

            let dupeSocket;

            beforeEach((done) => {

                sandbox.spy(Game, 'attemptAddPlayer');
                sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
                testSocket = SocketIOClient('http://localhost:8000');
                dupeSocket = SocketIOClient('http://localhost:8000');

                dupeSocket.on('welcome', () => {
                    dupeSocket.on('login:email', () => {
                        dupeSocket.emit('login:email:attempt', testAccount.email);
                    });
                    dupeSocket.on('login:email:dupe', (r) => {
                        response = r;
                        done();
                    });
                });

                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });

                    testSocket.on('login:email:success', (r) => {
                        dupeSocket.emit('login');
                    });
                });

                testSocket.emit('login');

            });

            it('responds to the user stating that the provided email cannot be used to login', () => {
                expect(response).to.be.a('String');
            });

            it('maintains the connection\'s unlogged MUD state', () => {
                let connectionState = MsgSvc.inspectConnection(dupeSocket.id);
                expect(connectionState.account).to.equal('not_logged_in');
                expect(connectionState.player).to.equal('not_logged_in');
            });

            it('does not tell the Game service to spawn a player', () => {
                expect(Game.attemptAddPlayer).to.not.be.called;
            });

        });

        describe('then wants to login with a nonexistent player', () => {

            beforeEach((done) => {
                sandbox.spy(Game, 'attemptAddPlayer');
                sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
                sandbox.stub(GameDB.Accounts, 'byId').returns(testAccount);
                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });

                    testSocket.on('login:email:success', (r) => {
                        testSocket.emit('login:player', 'Plamping the Goit');
                    });

                    testSocket.on('login:player:fail', (r) => {
                        response = r;
                        done();
                    });
                });

                testSocket.emit('login');
            });

            it('responds to the user stating the specified player does not exist', () => {
                expect(response).to.be.a('String');
            });

            it('does not update the connection\'s MUD player status', () => {
                let state = MsgSvc.inspectConnection(testSocket.id);
                expect(state.account).to.equal(testAccount.uid);
                expect(state.player).to.equal('not_logged_in');
            });

            it('does not tell the Game service to spawn a player', () => {
                expect(Game.attemptAddPlayer).to.not.be.called;
            });


        });

        describe('then wants to login with an existing player', () => {
            beforeEach((done) => {
                sandbox.spy(Game, 'attemptAddPlayer');
                sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
                sandbox.stub(GameDB.Accounts, 'byId').returns(testAccount);
                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });

                    testSocket.on('login:email:success', (r) => {
                        testSocket.emit('login:player', testPlayer.name);
                    });

                    testSocket.on('login:player:success', (r) => {
                        response = r;
                        done();
                    });
                });

                testSocket.emit('login');
            });

            it('responds to the user stating the specified player is now spawned into the game', () => {
                expect(response).to.be.a('String');
            });

            it('updates the connection\'s MUD player status', () => {
                let state = MsgSvc.inspectConnection(testSocket.id);
                expect(state.account).to.equal(testAccount.uid);
                expect(state.player).to.equal(testPlayer.uid);
            });

            it('checks with the entity database for an Account with the corresponding email', () => {
                expect(GameDB.Accounts.byEmail).to.be.calledWith(testAccount.email);
            });

            it('signals the game to spawn the selected player', () => {
                expect(Game.attemptAddPlayer).to.be.calledWith({
                    account_uid: testAccount.uid,
                    player_uid: testPlayer.uid
                });
            });
        });

        describe('then wants to create a new player', () => {

            beforeEach(() => {
                sandbox.spy(Game, 'attemptAddPlayer');
                sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
                sandbox.stub(GameDB.Accounts, 'byId').returns(testAccount);
                sandbox.spy(GameDB.Players, 'search');
                sandbox.spy(GameDB.Players, 'add');
                sandbox.spy(testAccount, 'addPlayer');
            });

            describe('that has the name of an existing player', () => {

                beforeEach((done) => {
                    testSocket = SocketIOClient('http://localhost:8000');
                    testSocket.on('welcome', () => {
                        testSocket.on('login:email', () => {
                            testSocket.emit('login:email:attempt', testAccount.email);
                        });

                        testSocket.on('login:email:success', () => {
                            testSocket.emit('create');
                        });

                        testSocket.on('create:player', () => {
                            testSocket.emit('create:player:name', testPlayer.name);
                        });

                        testSocket.on('create:player:dupe', (r) => {
                            response = r;
                            done();
                        });
                    });

                    testSocket.emit('login');
                });

                it('responds stating that the player will not be created', () => {
                    expect(response).to.be.a('String');
                });

                it('searches the entity data base for a Player with the provided name', () => {
                    expect(GameDB.Players.search).to.be.called;
                });

                it('does not add a Player object to the entity database', () => {
                    expect(GameDB.Players.add).to.not.be.called;
                });

                it('does not re-add the Player\'s UID to the invoking Account\'s Player UID list', () => {
                    expect(testAccount.addPlayer).to.not.be.called;
                });
            });

            describe.skip('that has a unique and unused name', () => {

                it('adds the newly created Player to the entity database', () => {
                });

                it('adds the newly created Player to the invoking Account\'s Player UID list', () => {
                });

                it('', () => {
                });

            });
        });

    });

    describe('when told to notify a single user/socket/connection', () => {

        let responsePacket;

        describe('via a response from the MUD (assuming the Player is logged in)', () => {

            beforeEach((done) => {

                sandbox.stub(Game, 'offerTransaction', () => {
                    MsgSvc.notifySingleUser({
                        player_uid: testPlayer.uid,
                        account_uid: testAccount.uid,
                        response: 'I am a test message'
                    });
                });

                testSocket = SocketIOClient('http://localhost:8000');

                testSocket.on('welcome', () => {

                    testSocket.on('game:response', (r) => {
                        responsePacket = r;
                        done();
                    });

                    testSocket.emit('login:email:attempt', testAccount.email);

                    testSocket.on('login:email:success', () => {
                        testSocket.emit('login:player', testPlayer.name);
                    });

                    testSocket.on('login:player:success', () => {
                        testSocket.emit('playing', 'test');
                    });

                });
            });

            it('emits a response packet to the requested recipient via Player name', () => {
                expect(responsePacket.player_uid).to.equal(testPlayer.uid);
                expect(responsePacket.account_uid).to.equal(testAccount.uid);
                expect(responsePacket.response).to.equal('I am a test message');
            });

        });
    });

    describe('when a user wants to leave the MUD', () => {

        beforeEach(() => {
            sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
            sandbox.stub(GameDB.Players, 'byId').returns(testPlayer);
            sandbox.spy(Game, 'offerTransaction');
        });

        before(() => {
            Game.stop();
            Game.start();
        });

        after(() => {
            Game.stop();
        });

        describe('via \'leave\'', () => {

            beforeEach((done) => {

                testSocket = SocketIOClient('http://localhost:8000');
                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });

                    testSocket.on('login:email:success', () => {
                        testSocket.emit('login:player', testPlayer.name);
                    });

                    testSocket.on('login:player:success', () => {
                        testSocket.emit('playing', 'leave');
                    });

                    testSocket.on('game:response', () => {
                        testSocket.emit('playing:quit');
                        done();
                    });
                });
                testSocket.emit('login');
            });

            it('signals the game to despawn the player', () => {
                expect(Game.offerTransaction.getCall(0).args[0].command).to.equal('leave');
            });

            it('resets the connection\'s player MUD status to an unlogged state', (done) => {
                let i = setInterval(() => {
                    if(MsgSvc.inspectConnection(testSocket.id).player == 'not_logged_in') {
                        clearInterval(i);
                        done();
                    }
                }, 5);
            });

            it('does not reset the connection\'s account state', () => {
                expect(MsgSvc.inspectConnection(testSocket.id).account).to.equal(testAccount.uid);
            });

        });

        describe('via \'quit\'', () => {

            beforeEach((done) => {

                testSocket = SocketIOClient('http://localhost:8000');
                testSocket.on('welcome', () => {

                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });

                    testSocket.on('login:email:success', () => {
                        testSocket.emit('login:player', testPlayer.name);
                    });

                    testSocket.on('login:player:success', () => {

                        testSocket.emit('playing', 'quit');

                    });

                    testSocket.on('game:response', () => {
                        testSocket.emit('playing:quit');
                        done();
                    });

                });

                testSocket.emit('login');

            });

            it('signals the game to despawn the player', () => {
                expect(Game.offerTransaction.getCall(0).args[0].command).to.equal('quit');
            });

            it('resets the connection\'s player MUD status to an unlogged state', (done) => {
                let i = setInterval(() => {
                    if(MsgSvc.inspectConnection(testSocket.id).player == 'not_logged_in') {
                        clearInterval(i);
                        done();
                    }
                }, 5);
            });

            it('does not reset the connection\'s account state', () => {
                expect(MsgSvc.inspectConnection(testSocket.id).account).to.equal(testAccount.uid);
            });

        });

    });

    describe('when a user disconnects from the server', () => {

        beforeEach(() => {
            sandbox.spy(Game, 'offerTransaction');
            sandbox.stub(GameDB.Accounts, 'byEmail').returns(testAccount);
        });

        describe('while not logged into the MUD', () => {

            describe('without either Account or Player credentials', () => {

                beforeEach(() => {
                    testSocket = SocketIOClient('http://localhost:8000');
                    testSocket.on('welcome', () => {
                        testSocket.disconnect();
                    });
                });

                it('does not signal the Game service to remove a player from the MUD', () => {
                    expect(Game.offerTransaction).to.not.be.called;
                });

                it('no longer tracks the disconnected user\'s MUD state', () => {
                    expect(MsgSvc.inspectConnection(testSocket.id)).to.be.undefined;
                });

            });

            describe('without Player credentials', () => {

                beforeEach((done) => {
                    testSocket = SocketIOClient('http://localhost:8000');
                    testSocket.on('welcome', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                        testSocket.on('login:email:success', () => {
                            expect(MsgSvc.inspectConnection(testSocket.id).account).to.equal(testAccount.uid);
                            expect(MsgSvc.inspectConnection(testSocket.id).player).to.equal('not_logged_in');
                            testSocket.disconnect();
                            done();
                        });
                    });
                });

                it('does not signal the Game service to remove a player from the MUD', () => {
                    expect(Game.offerTransaction).to.not.be.called;
                });

                it('no longer tracks the disconnected user\'s MUD state', () => {
                    expect(MsgSvc.inspectConnection(testSocket.id)).to.be.undefined;
                });

            });

        });

        describe('while logged into the MUD', () => {

            before(() => {
                Game.start();
            });

            after(() => {
                Game.stop();
            });

            beforeEach((done) => {
                testSocket = SocketIOClient('http://localhost:8000');
                testSocket.on('welcome', () => {
                    testSocket.on('login:email', () => {
                        testSocket.emit('login:email:attempt', testAccount.email);
                    });
                    testSocket.on('login:email:success', () => {
                        expect(MsgSvc.inspectConnection(testSocket.id).account).to.equal(testAccount.uid);
                        expect(MsgSvc.inspectConnection(testSocket.id).player).to.equal('not_logged_in');
                        testSocket.emit('login:player', testPlayer.name);
                    });
                    testSocket.on('login:player:success', () => {
                        testSocket.disconnect();
                        let i = setInterval(() => {
                            if(!MsgSvc.inspectConnection(testSocket.id)) {
                                clearInterval(i);
                                done();
                            }
                        }, 5);
                    });
                });

                testSocket.emit('login');


            });

            it('signals the game to despawn the logged in Player', () => {
                expect(Game.offerTransaction.getCall(0).args[0].command).to.equal('leave');
            });

            it('no longer tracks the disconnected user\'s MUD state', () => {
                expect(MsgSvc.inspectConnection(testSocket.id)).to.be.undefined;
            });

        });

    });
});
