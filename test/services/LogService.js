'use strict';

const chai = require('chai');
const expect = chai.expect;

const log = require('../../lib/services/LogService');

describe('The LogService', function () {

    it('should have an info function', function () {
        expect(log.info).to.be.a('function');
    });

    it('should have a debug function', function () {
        expect(log.debug).to.be.a('function');
    });

    it('should have an error function', function () {
        expect(log.error).to.be.a('function');
    });

});
