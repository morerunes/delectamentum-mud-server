'use strict';

const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chai = require('chai');
chai.use(sinonChai);
const expect = chai.expect;

const Auth = require('../../lib/services/AuthService');
const config = require('../../lib/services/ConfigService');
const GameDB = require('../../lib/services/GameDBService');
const log = require('../../lib/services/LogService');
const Account = require('../../lib/objects/things/Account');
const Player = require('../../lib/objects/things/Player');
const Joi = require('joi');

describe('the Authentication Service', function () {
  
    let sandbox;

    beforeEach(function() {
        sandbox = sinon.sandbox.create();      
    });

    afterEach(function() {
        sandbox.restore();
        GameDB.reset();
    });

    describe('when validating a newly created Account',function() {

        let accounts;
        let result;

        beforeEach(() => {
                        
            accounts = [
                new Account('a@goit.com'),
                new Account('goit'),
                new Account('goit@goit.com')
            ];

        });


        describe('and the Account doesn\'t have a valid email address', () => {
          
            it('should invalidate the attempt for an invalid email address', () => {
                result = Auth.validateNewAccount(accounts[1]);

                expect(result.isValidAccount).to.equal(false);
                expect(result.error_msg).to.equal('Invalid Email');
            });
        });

        describe('and the account comes with an array of player UIDs', () => {

            let oddAcc;
            
            beforeEach(() => {
                oddAcc = new Account('goit@goit.com');
                oddAcc.addPlayer(new Player('goit','goit'));
            });

            it('should invalidate the attempt for the account having a non-empty player uid list', () => {
                result = Auth.validateNewAccount(oddAcc);
                expect(result.isValidAccount).to.equal(false);
                expect(result.error_msg).to.equal('Can\'t make new Account with a non-empty player uid list');
            });

        });

        describe('and is given something other than an account', () => {
            it('should throw an error for not calling the validation funciton with an account', () => {
                expect(() => {
                    Auth.validateNewAccount('goit');
                }).to.throw(Error,'This isn\'t an Account');
            });
        });

        describe('and is given an Account with a valid email', () => {

            let valResult,
                acc;

            beforeEach(() => {
                
                acc = new Account('kyle.busch.is.a@goit.com');
                valResult = Auth.validateNewAccount(acc);

            });

            it('should flag the validation attempt as valid', () => {
                expect(valResult.isValidAccount).to.equal(true);
                expect(valResult.error_msg).to.be.null;
            });

        });
        
    });

    describe('when validating a login attempt', () => {
      
        let acc,
            valP,
            accHash;

        beforeEach(() => {

            sandbox.spy(GameDB.Accounts, 'byEmail');

            acc = new Account('kyle.busch.is.a@goit.com');

            GameDB.Accounts.add(acc);
        });

        it('does not attempt to validate without an email address', () => {
            expect(() => {
                Auth.validateLogin(undefined);
            }).to.throw(Error, 'Email address required for login');
        });

        it('fails the validation attempt when an account with the given email address does not exist', () => {
            expect(() => {
                Auth.validateLogin('plamp@plamp.com');
            }).to.throw(Error, 'Invalid Login');
        });

        it('passes the validation attempt when the credentials are correct', () => {
            expect(Auth.validateLogin(acc.email)).to.deep.equal(acc);
        });

    });

    describe('when creating an API ticket', () => {
      
        let acc1,
            acc2;  

        beforeEach(() => {
  
            sandbox.spy(GameDB.Accounts, 'byId');

            acc1 = new Account('goit@goit.com');

            acc2 = new Account('plamp@goit.com');

            GameDB.Accounts.add(acc1);
        });

        describe('for an Account not tracked by the Game/that doesn\'t exist', () => {

            it('throws an error for making a ticket for a non-existant account', () => {
                expect(() => {
                    Auth.generateTicket(acc2);
                }).to.throw(Error, 'Cannot create ticket for an untracked Account');

                expect(GameDB.Accounts.byId).to.have.been.calledWith(acc2.uid);

            });
           
        });

        describe('for an Account tracked by the Game', () => {
            
            let t_uid;

            beforeEach(() => {
                t_uid = Auth.generateTicket(acc1);
            });
                        
            it('returns a ticket uid', () => {
                expect(t_uid).to.not.be.undefined;
            });

            it('calls GameDB.Accounts.byId', () => {
                expect(GameDB.Accounts.byId).to.have.been.calledWith(acc1.uid);
            });

        });

        describe('for something that is not an Account', () => {
          
            it('should throw an error', () => {
                expect(() => {
                    Auth.generateTicket('goit');
                }).to.throw(Error, 'This isn\'t an Account');
            });
            
        });

    });

    describe('when retrieving an API ticket', () => {
      
        let ticket_uid,
            account;

        beforeEach(() => {
            account = new Account('goit@goit.com');
            GameDB.Accounts.add(account);
            ticket_uid = Auth.generateTicket(account);
        });  

        describe('that exists', () => {
            it('should successfully get the ticket', () => {
                expect(Auth.retrieveTicket(ticket_uid).account_uid).to.equal(account.uid);
            });
        });

        describe('that does not exist', () => {
            it('should throw an error', () => {
                expect(() => {
                    Auth.retrieveTicket('goit');
                }).to.throw(Error);
            });
        });

    });

});
