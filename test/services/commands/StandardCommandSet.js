'use strict';

const Promise = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const _ = require('lodash');
const expect = chai.expect;

const Game = require('../../../lib/services/GameService');
const Player = require('../../../lib/objects/things/Player');
const Account = require('../../../lib/objects/things/Account');
const PlayerSpec = require('../../../lib/objects/things/PlayerSpec');
const Place = require('../../../lib/objects/things/Place');
const Thing = require('../../../lib/objects/things/Thing');
const GameDB = require('../../../lib/services/GameDBService');
const config = require('../../../lib/services/ConfigService');
const ActiveUIDs = require('../../../lib/services/ActiveSessionCache');

const StandardCommands = require('../../../lib/services/commands');

describe('The standard MUD command(s)', () => {

    let cmdset,
        testPlayer,
        sandbox,
        admin,
        normal,
        testPacket;

    before(() => {
        GameDB.init();
        Game.start();
        cmdset = {};
        _.forEach(StandardCommands, (cmdDef) => {
            _.forEach(cmdDef, (command) => {
                cmdset[command.name] = command;
            });
        });
    });

    beforeEach(function () {
        sandbox = sinon.sandbox.create();
        config.init('ERROR');

        testPlayer = new Player('test','guinea pig', new PlayerSpec());
        admin = new Account('test@goit.com', 'admin');
        normal = new Account('a@plamp.goit');
        admin.addPlayer(testPlayer);

        testPlayer.location = 'LOBBY';

        testPacket = {
            account_uid: admin.uid,
            player_uid: testPlayer.uid,
            __playerDelta: {}
        };

        GameDB.Players.add(testPlayer);
        GameDB.Accounts.add(admin);
    });

    afterEach(() => {
        GameDB.reset();
        Game.reset();
        ActiveUIDs.reset();
        sandbox.restore();
    });

    describe('for requesting a detailed message on command usage', () => {

        let cmds,
            response,
            flatresponse;

        beforeEach(() => {
            flatresponse = [];
            cmds = [
                'help',
                'h'
            ];

            _.forEach(cmds, (cmd) => {
                testPacket.command = cmd;
                testPacket._baseCommand = cmd;

                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });
        });

        it('responds with a string, at minimum (a.k.a. you can be as unhelpful as you want)', () => {
            _.forEach(flatresponse, (r) => {
                expect(r).to.be.a('string');
            });

        });

    });

    describe('for requesting one\'s own XP amount', () => {

        let cmds,
            response,
            flatresponse;

        beforeEach(() => {
            flatresponse = [];
            cmds = [
                'experience',
                'xp',
                'exp'
            ];

            sandbox.spy(GameDB.Players, 'byId');

            _.forEach(cmds, (cmd) => {
                testPacket.command = cmd;
                testPacket._baseCommand = cmd;

                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });
        });

        it('responds back a string that contains the player\' XP amount', () => {
            _.forEach(flatresponse, (response) => {
                expect(response).to.equal('Your XP: ' + testPlayer.spec.xp);
            });
        });

        it('attempts to retrieve the corresponding Player object from the active entity database', () => {
            expect(GameDB.Players.byId.callCount).to.equal(3);
        });
    });

    describe('for movement', () => {

        let result,
            pn,
            ps,
            pw,
            pe;

        beforeEach(() => {

            pn = new Place('N','a more northern area', 1);
            ps = new Place('S','a more southern area', 1);
            pw = new Place('W','a more western area', 1);
            pe = new Place('E','a more eastern area', 1);

            GameDB.Places.add(pn);
            GameDB.Places.add(ps);
            GameDB.Places.add(pe);
            GameDB.Places.add(pw);

            GameDB.Realms.Ether.addPlace(pn, 0, 1);
            GameDB.Realms.Ether.addPlace(ps, 0, -1);
            GameDB.Realms.Ether.addPlace(pw, -1, 0);
            GameDB.Realms.Ether.addPlace(pe, 1, 0);

        });

        describe('should handle all four directions of movement', () => {

            let moveset,
                route,
                responses,
                playerLocations,
                flattenedResponses,
                pDeltas,
                defaultMsg;

            beforeEach(() => {
                defaultMsg = 'You now find yourself in ';
                responses = [];
                playerLocations = [];
                flattenedResponses = [];
                pDeltas = [];
            });

            describe('using the full commands (north, south, east, west)', () => {

                beforeEach(() => {
                    moveset = [
                        'north',
                        'south',
                        'west',
                        'east'
                    ];
                });

                describe('with at least one nonexistant destination', () => {

                    beforeEach(() => {
                        route = [
                            moveset[0],
                            moveset[0],
                            moveset[2],
                            moveset[3],
                            moveset[1],
                            moveset[1]
                        ];
                    });

                    describe.skip('and at least one room containing a post-move trap/counteraction', () => {
                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            chai.assert.fail();
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            chai.assert.fail();
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            chai.assert.fail();
                        });
                    });

                    describe('and no rooms containing a post-move trap/counteraction', () => {
                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            expect(playerLocations[0]).to.equal(pn.uid);
                            expect(playerLocations[1]).to.equal(pn.uid);
                            expect(playerLocations[2]).to.equal(pn.uid);
                            expect(playerLocations[3]).to.equal(pn.uid);
                            expect(playerLocations[4]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[5]).to.equal(ps.uid);
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            expect(flattenedResponses[0]).to.equal(defaultMsg + pn.description);
                            expect(flattenedResponses[1]).to.equal('You walk into a wall');
                            expect(flattenedResponses[2]).to.equal('You walk into a wall');
                            expect(flattenedResponses[3]).to.equal('You walk into a wall');
                            expect(flattenedResponses[4]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[5]).to.equal(defaultMsg + ps.description);
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            expect(pDeltas[0]).to.equal(pn.uid);
                            expect(pDeltas[1]).to.equal('none');
                            expect(pDeltas[2]).to.equal('none');
                            expect(pDeltas[3]).to.equal('none');
                            expect(pDeltas[4]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[5]).to.equal(ps.uid);
                        });
                    });
                });

                describe('when all destinations exist', () => {

                    beforeEach(() => {
                        route = [
                            moveset[0],
                            moveset[1],
                            moveset[2],
                            moveset[3],
                            moveset[3],
                            moveset[2],
                            moveset[1],
                            moveset[0]
                        ];
                    });

                    describe.skip('and at least one room containing a post-move trap/counteraction', () => {
                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            chai.assert.fail();
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            expect(flattenedResponses[1]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[3]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[5]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[7]).to.equal(defaultMsg + GameDB.Places.Lobby.description);

                            expect(flattenedResponses[0]).to.equal(defaultMsg + pn.description);
                            expect(flattenedResponses[2]).to.equal(defaultMsg + pw.description);
                            expect(flattenedResponses[4]).to.equal(defaultMsg + pe.description);
                            expect(flattenedResponses[6]).to.equal(defaultMsg + ps.description);
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            chai.assert.fail();
                            expect(pDeltas[0]).to.equal(pn.uid);
                            expect(pDeltas[1]).to.equal('none');
                            expect(pDeltas[2]).to.equal('none');
                            expect(pDeltas[3]).to.equal('none');
                            expect(pDeltas[4]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[5]).to.equal(ps.uid);
                        });
                    });

                    describe('and no rooms containing a post-move trap/counteraction', () => {

                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            expect(playerLocations[1]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[3]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[5]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[7]).to.equal(GameDB.Places.Lobby.uid);

                            expect(playerLocations[0]).to.equal(pn.uid);
                            expect(playerLocations[2]).to.equal(pw.uid);
                            expect(playerLocations[4]).to.equal(pe.uid);
                            expect(playerLocations[6]).to.equal(ps.uid);
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            expect(flattenedResponses[1]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[3]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[5]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[7]).to.equal(defaultMsg + GameDB.Places.Lobby.description);

                            expect(flattenedResponses[0]).to.equal(defaultMsg + pn.description);
                            expect(flattenedResponses[2]).to.equal(defaultMsg + pw.description);
                            expect(flattenedResponses[4]).to.equal(defaultMsg + pe.description);
                            expect(flattenedResponses[6]).to.equal(defaultMsg + ps.description);
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            expect(pDeltas[1]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[3]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[5]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[7]).to.equal(GameDB.Places.Lobby.uid);

                            expect(pDeltas[0]).to.equal(pn.uid);
                            expect(pDeltas[2]).to.equal(pw.uid);
                            expect(pDeltas[4]).to.equal(pe.uid);
                            expect(pDeltas[6]).to.equal(ps.uid);
                        });
                    });

                });

            });

            describe('using the abbreviated commands (n, s, w, e)', () => {

                beforeEach(() => {
                    moveset = [
                        'n',
                        's',
                        'w',
                        'e'
                    ];
                });

                describe('with at least one nonexistant destination', () => {

                    beforeEach(() => {
                        route = [
                            moveset[0],
                            moveset[0],
                            moveset[2],
                            moveset[3],
                            moveset[1],
                            moveset[1]
                        ];
                    });

                    describe.skip('and at least one room containing a post-move trap/counteraction', () => {
                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            chai.assert.fail();
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            chai.assert.fail();
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            chai.assert.fail();
                        });
                    });

                    describe('and no rooms containing a post-move trap/counteraction', () => {
                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            expect(playerLocations[0]).to.equal(pn.uid);
                            expect(playerLocations[1]).to.equal(pn.uid);
                            expect(playerLocations[2]).to.equal(pn.uid);
                            expect(playerLocations[3]).to.equal(pn.uid);
                            expect(playerLocations[4]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[5]).to.equal(ps.uid);
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            expect(flattenedResponses[0]).to.equal(defaultMsg + pn.description);
                            expect(flattenedResponses[1]).to.equal('You walk into a wall');
                            expect(flattenedResponses[2]).to.equal('You walk into a wall');
                            expect(flattenedResponses[3]).to.equal('You walk into a wall');
                            expect(flattenedResponses[4]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[5]).to.equal(defaultMsg + ps.description);
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            expect(pDeltas[0]).to.equal(pn.uid);
                            expect(pDeltas[1]).to.equal('none');
                            expect(pDeltas[2]).to.equal('none');
                            expect(pDeltas[3]).to.equal('none');
                            expect(pDeltas[4]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[5]).to.equal(ps.uid);
                        });
                    });
                });

                describe('when all destinations exist', () => {

                    beforeEach(() => {
                        route = [
                            moveset[0],
                            moveset[1],
                            moveset[2],
                            moveset[3],
                            moveset[3],
                            moveset[2],
                            moveset[1],
                            moveset[0]
                        ];
                    });

                    describe.skip('and at least one room containing a post-move trap/counteraction', () => {
                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            chai.assert.fail();
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            expect(flattenedResponses[1]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[3]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[5]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[7]).to.equal(defaultMsg + GameDB.Places.Lobby.description);

                            expect(flattenedResponses[0]).to.equal(defaultMsg + pn.description);
                            expect(flattenedResponses[2]).to.equal(defaultMsg + pw.description);
                            expect(flattenedResponses[4]).to.equal(defaultMsg + pe.description);
                            expect(flattenedResponses[6]).to.equal(defaultMsg + ps.description);
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            chai.assert.fail();
                            expect(pDeltas[0]).to.equal(pn.uid);
                            expect(pDeltas[1]).to.equal('none');
                            expect(pDeltas[2]).to.equal('none');
                            expect(pDeltas[3]).to.equal('none');
                            expect(pDeltas[4]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[5]).to.equal(ps.uid);
                        });
                    });

                    describe('and no rooms containing a post-move trap/counteraction', () => {

                        beforeEach(() => {
                            _.forEach(route, (move) => {
                                testPacket.command = move;
                                responses.push(cmdset[move].execute(testPacket));
                                playerLocations.push(testPlayer.location);
                                pDeltas.push(testPacket.__playerDelta.location);
                            });

                            _.forEach(responses, (cmdRespList) => {
                                _.forEach(cmdRespList, (response) => {
                                    flattenedResponses.push(response);
                                });
                            });
                        });

                        it('moves the player to the correct location [uid] at each move command', () => {
                            expect(playerLocations[1]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[3]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[5]).to.equal(GameDB.Places.Lobby.uid);
                            expect(playerLocations[7]).to.equal(GameDB.Places.Lobby.uid);

                            expect(playerLocations[0]).to.equal(pn.uid);
                            expect(playerLocations[2]).to.equal(pw.uid);
                            expect(playerLocations[4]).to.equal(pe.uid);
                            expect(playerLocations[6]).to.equal(ps.uid);
                        });

                        it('responds with the correct messages and [potentially lack of] place descriptions', () => {
                            expect(flattenedResponses[1]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[3]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[5]).to.equal(defaultMsg + GameDB.Places.Lobby.description);
                            expect(flattenedResponses[7]).to.equal(defaultMsg + GameDB.Places.Lobby.description);

                            expect(flattenedResponses[0]).to.equal(defaultMsg + pn.description);
                            expect(flattenedResponses[2]).to.equal(defaultMsg + pw.description);
                            expect(flattenedResponses[4]).to.equal(defaultMsg + pe.description);
                            expect(flattenedResponses[6]).to.equal(defaultMsg + ps.description);
                        });

                        it('reports the correct player delta (change) for location change', () => {
                            expect(pDeltas[1]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[3]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[5]).to.equal(GameDB.Places.Lobby.uid);
                            expect(pDeltas[7]).to.equal(GameDB.Places.Lobby.uid);

                            expect(pDeltas[0]).to.equal(pn.uid);
                            expect(pDeltas[2]).to.equal(pw.uid);
                            expect(pDeltas[4]).to.equal(pe.uid);
                            expect(pDeltas[6]).to.equal(ps.uid);
                        });
                    });

                });
            });
        });
    });

    describe('for item interaction', () => {

        let flatresponse,
            response;

        beforeEach(() => {
            flatresponse = [];
        });

        describe('via usage', () => {

        });

        describe('via acquisition from a room', () => {
            let contraband,
                eldorado,
                nothing,
                cmd,
                result;

            beforeEach(() => {
                contraband = new Thing('shite-shaped gold', 'all that glitters is... crap');
                eldorado = new Place('El Dungado', 'A room full of crap', 1);
                nothing = new Place('Nothing', 'A room with jack-squat in it', 1);
                eldorado.accept(contraband);
                eldorado.give(contraband);

                GameDB.Places.add(eldorado);
                GameDB.Places.add(nothing);

                expect(GameDB.Places.byId(eldorado.uid).contents['shite-shaped gold']).to.equal(contraband);

                sandbox.spy(testPlayer, 'give');
                sandbox.spy(testPlayer, 'take');

                sandbox.spy(eldorado, 'give');
                sandbox.spy(eldorado, 'take');
            });

            describe('that has something in it', () => {

                beforeEach(() => {
                    GameDB.Realms.Ether.addPlace(eldorado, 0, 1);
                    testPlayer.location = eldorado.uid;
                });

                describe('that a player cannot take', () => {
                    beforeEach(() => {
                        testPacket.command = 'take shite-shaped gold';
                        testPacket._baseCommand = 'take';
                        testPacket._parameters = 'shite-shaped gold';

                        response = cmdset[testPacket._baseCommand].execute(testPacket);

                        _.forEach(response, (r) => {
                            flatresponse.push(r);
                        });
                    });

                    it('responds saying the player failed to take the item', () => {
                        expect(flatresponse[0]).to.equal('You are unable to take the shite-shaped gold');
                    });

                    it('does not add the item to the player inventory', () => {
                        expect(testPlayer.give).to.not.be.called;
                        expect(GameDB.Players.byId(testPlayer.uid).contents['shite-shaped gold']).to.be.undefined;
                    });

                    it('attempts to remove the item from the place inventory, but replaces it after', () => {
                        expect(GameDB.Places.byId(eldorado.uid).contents['shite-shaped gold']).to.equal(contraband);
                    });
                });

                describe('that a player can take', () => {
                    beforeEach(() => {
                        testPlayer.accept(contraband);
                        testPacket.command = 'get shite-shaped gold';
                        testPacket._baseCommand = 'get';
                        testPacket._parameters = 'shite-shaped gold';

                        response = cmdset[testPacket._baseCommand].execute(testPacket);

                        _.forEach(response, (r) => {
                            flatresponse.push(r);
                        });
                    });

                    it('responds saying the player obtained the item', () => {
                        expect(flatresponse[0]).to.equal('You take the shite-shaped gold');
                    });

                    it('adds the item to the player\'s inventory', () => {
                        expect(testPlayer.give).to.be.calledWith(contraband);
                        expect(GameDB.Players.byId(testPlayer.uid).contents['shite-shaped gold']).to.equal(contraband);
                    });

                    it('removes the item from the place\'s inventory', () => {
                        expect(eldorado.take).to.be.calledWith('shite-shaped gold');
                        expect(GameDB.Places.byId(eldorado.uid).contents['shite-shaped gold']).to.be.undefined;
                    });
                });

                describe('that turns out to be a form of currency', () => {

                    beforeEach(() => {
                        eldorado.contents['currency_amt'] = 500;
                    });

                    describe('specifying a not-whole number', () => {
                        beforeEach(() => {
                            testPacket.command = 'take .90';
                            testPacket._baseCommand = 'take';
                            testPacket._parameters = '.90';

                            response = cmdset[testPacket._baseCommand].execute(testPacket);

                            _.forEach(response, (r) => {
                                flatresponse.push(r);
                            });
                        });

                        it('responds saying the player cannot do that', () => {
                            expect(flatresponse[0]).to.equal('I don\'t deal in cents, fam');
                        });

                        it('does not modify the player\'s or the place\'s currency amounts', () => {
                            expect(eldorado.contents.currency_amt).to.equal(500);
                            expect(testPlayer.contents.currency_amt).to.equal(0);
                        });
                    });

                    describe('specifying a negative amount', () => {

                        beforeEach(() => {
                            testPacket.command = 'take -10';
                            testPacket._baseCommand = 'take';
                            testPacket._parameters = '-10';

                            response = cmdset[testPacket._baseCommand].execute(testPacket);

                            _.forEach(response, (r) => {
                                flatresponse.push(r);
                            });
                        });

                        it('responds saying the player is not supposed to do that', () => {
                            expect(flatresponse[0]).to.equal('Nice try, goit. Negative numbers don\'t get you anywhere');
                        });

                        it('does not modify the stored currency amounts of the player or place', () => {
                            expect(eldorado.contents.currency_amt).to.equal(500);
                            expect(testPlayer.contents.currency_amt).to.equal(0);
                        });

                    });

                    describe('that the room has enough of', () => {

                        beforeEach(() => {
                            testPacket.command = 'take 250';
                            testPacket._baseCommand = 'take';
                            testPacket._parameters = '250';

                            response = cmdset[testPacket._baseCommand].execute(testPacket);

                            _.forEach(response, (r) => {
                                flatresponse.push(r);
                            });
                        });

                        it('responds stating the specified amount of currency was taken', () => {
                            expect(flatresponse[0]).to.equal('You snatched 250 gold from the room');
                        });

                        it('deducts the specified amount from the room', () => {
                            expect(eldorado.contents.currency_amt).to.equal(250);

                        });

                        it('adds the specified amount to the player\'s inventory', () => {
                            expect(testPlayer.contents.currency_amt).to.equal(250);
                        });

                    });

                    describe('that the room does not have enough of', () => {

                        beforeEach(() => {
                            testPacket.command = 'take 1000';
                            testPacket._baseCommand = 'take';
                            testPacket._parameters = '1000';

                            response = cmdset[testPacket._baseCommand].execute(testPacket);

                            _.forEach(response, (r) => {
                                flatresponse.push(r);
                            });
                        });

                        it('responds stating that the room\'s available amount of currency was taken', () => {
                            expect(flatresponse[0]).to.equal('You took only 500 gold from the room, because it didn\'t have 1000 gold in it');
                        });

                        it('deducts the available amount from the room (or zeroes the room\'s amount)', () => {
                            expect(eldorado.contents.currency_amt).to.equal(0);
                        });

                        it('adds the available amount to the player\'s inventory', () => {
                            expect(testPlayer.contents.currency_amt).to.equal(500);
                        });
                    });
                });
            });

            describe('that has nothing in it', () => {

                let origplayer_inv,
                    origplace_inv;

                beforeEach(() => {
                    testPlayer.location = nothing.uid;
                    testPacket.command = 'take plamp';

                    origplayer_inv = JSON.parse(JSON.stringify(testPlayer.contents));
                    origplace_inv = JSON.parse(JSON.stringify(nothing.contents));

                    expect(GameDB.Players.byId(testPlayer.uid).contents).to.deep.equal(origplayer_inv);
                    expect(GameDB.Places.byId(nothing.uid).contents).to.deep.equal(origplace_inv);

                    testPacket._baseCommand = 'take';
                    testPacket._parameters = 'plamp';

                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('responds saying the player took nothing', () => {
                    expect(flatresponse[0]).to.equal('You just tried to take a whole load of nothing');
                });

                it('does not change the inventory/contents of the place or player', () => {
                    expect(GameDB.Players.byId(testPlayer.uid).contents).to.deep.equal(origplayer_inv);
                    expect(GameDB.Places.byId(nothing.uid).contents).to.deep.equal(origplace_inv);
                });
            });
        });

        describe('via dropping something in the room', () => {
            let contraband,
                dumproom,
                fullroom,
                cmd,
                result,
                response,
                flatresponse;

            beforeEach(() => {

                flatresponse = [];
                contraband = new Thing('beer can','something that makes you drunk');
                dumproom = new Place('Dump room','I want stuff!', 1);
                fullroom = new Place('Full room','I do not want stuff',1);
                dumproom.accept(contraband);
                GameDB.Places.add(dumproom);
                GameDB.Places.add(fullroom);
                GameDB.Realms.Ether.addPlace(dumproom, 0, 1);
                GameDB.Realms.Ether.addPlace(fullroom, 1, 0);
                GameDB.Players.byId(testPlayer.uid).accept(contraband);
                GameDB.Players.byId(testPlayer.uid).give(contraband);

                expect(GameDB.Players.byId(testPlayer.uid).contents['beer can']).to.equal(contraband);

                sandbox.spy(testPlayer, 'take');
                sandbox.spy(testPlayer, 'give');
                sandbox.spy(dumproom, 'give');
                sandbox.spy(fullroom, 'give');
            });

            describe('that can accept the thing', () => {

                beforeEach(() => {
                    testPlayer.location = GameDB.Realms.Ether.at(0,1).uid;
                    testPacket.command = 'dump beer can';
                    testPacket._baseCommand = 'dump';
                    testPacket._parameters = 'beer can';
                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('removes the thing from the players inventory', () => {
                    expect(testPlayer.take).to.be.calledWith('beer can');
                    expect(GameDB.Players.byId(testPlayer.uid).contents['beer can']).to.be.undefined;
                });

                it('adds the thing to the place\'s contents', () => {
                    expect(dumproom.give).to.be.calledWith(contraband);
                    expect(GameDB.Places.byId(dumproom.uid).contents['beer can']).to.equal(contraband);
                });

                it('responds with a message indicating the item was dropped', () => {
                    expect(flatresponse[0]).to.equal('You dropped the beer can');
                });

                it('gives a second response indicating a potential after-effect', () => {
                    expect(flatresponse[1]).to.be.a.String;
                });
            });

            describe('that cannot accept the thing', () => {

                beforeEach(() => {
                    testPlayer.location = GameDB.Realms.Ether.at(1,0).uid;
                    testPacket.command = 'dump beer can';
                    testPacket._baseCommand = 'dump';
                    testPacket._parameters = 'beer can';
                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('does not modify the contents of the place, but attempts to give the thing to the place', () => {
                    expect(testPlayer.take).to.be.calledWith('beer can');
                    expect(dumproom.give).to.not.be.called;
                    expect(testPlayer.give).to.be.calledWith(contraband);
                    expect(GameDB.Players.byId(testPlayer.uid).contents['beer can']).to.equal(contraband);
                    expect(GameDB.Places.byId(fullroom.uid).contents['beer can']).to.be.undefined;
                });

                it('responds saying the room does not accept the unwanted item', () => {
                    expect(flatresponse[0]).to.equal('Some odd force prevents you from leaving the beer can behind');
                });

                it('gives a second response indicating a potential after-effect', () => {
                    expect(flatresponse[1]).to.be.a.String;
                });
            });
        });
    });

    describe('for hitting something in the face', () => {
        let playermark,
            npcmark,
            copy,
            pit,
            result,
            response,
            flatresponse;

        beforeEach(() => {
            flatresponse = [];
            playermark = new Player('Goit', 'Wants to get plamped', new PlayerSpec());
            npcmark = new Player('Stooge', 'A Scrub', new PlayerSpec());
            pit = new Place('pit','test',10);
            normal.addPlayer(playermark);
            GameDB.Accounts.add(normal);
            GameDB.Players.add(playermark);
            GameDB.NPCs.add(npcmark);
            GameDB.Places.add(pit);
            GameDB.Realms.Ether.addPlace(pit, 0, 1);

            ActiveUIDs.addPlayer(playermark, false);
            ActiveUIDs.addPlayer(npcmark, true);

            testPacket.account_uid = normal.uid;
            testPacket.player_uid = playermark.uid;

            copy = JSON.parse(JSON.stringify(testPlayer));
        });

        describe('that is not present in the game world', () => {

            let no_hit;

            beforeEach(() => {
                testPacket.account_uid = admin.uid;
                testPacket.player_uid = testPlayer.uid;

            });

            describe('via \'attack\'', () => {

                beforeEach(() => {
                    testPacket.command = 'attack Scrub';
                    testPacket._baseCommand = 'attack';
                    testPacket._parameters = 'Scrub';
                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('responds with the attacker hitting nothing', () => {
                    expect(flatresponse[0]).to.equal(testPlayer.name + ' tried to fight air');
                });

                it('does not change the state of the attacker', () => {
                    expect(GameDB.Players.byId(testPlayer.uid)).to.deep.equal(copy);
                });
            });

            describe('via \'a\'', () => {

                beforeEach(() => {
                    testPacket.command = 'a Scrub';
                    testPacket._baseCommand = 'a';
                    testPacket._parameters = 'Scrub';
                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('responds with the attacker hitting nothing', () => {
                    expect(flatresponse[0]).to.equal(testPlayer.name + ' tried to fight air');
                });

                it('does not change the state of the attacker', () => {
                    expect(GameDB.Players.byId(testPlayer.uid)).to.deep.equal(copy);
                });
            });
        });

        describe('that is currently roaming the world', () => {

            let hit;

            beforeEach(() => {
                testPacket.account_uid = admin.uid;
                testPacket.player_uid = testPlayer.uid;
                testPlayer.location = pit.uid;
                playermark.location = pit.uid;
                npcmark.location = pit.uid;
            });

            describe('as a Player', () => {

                beforeEach(() => {
                    testPacket.command = 'attack Goit';
                    testPacket._baseCommand = 'attack';
                    testPacket._parameters = 'Goit';
                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('responds with a message demonstrating intent to hit the player', () => {
                    expect(flatresponse[0]).to.equal(testPlayer.name + ' smacks ' + playermark.name + ' in the face');
                });

                it.skip('modifies the target Player\'s specification for the worse if hit is successful', () => {
                    chai.assert.fail();
                });
            });

            describe('as an NPC', () => {

                beforeEach(() => {
                    testPacket.command = 'attack Stooge';
                    testPacket._baseCommand = 'attack';
                    testPacket._parameters = 'Stooge';
                    response = cmdset[testPacket._baseCommand].execute(testPacket);

                    _.forEach(response, (r) => {
                        flatresponse.push(r);
                    });
                });

                it('responds with a message demonstrating intent to hit the NPC', () => {
                    expect(flatresponse[0]).to.deep.equal(testPlayer.name + ' smacks ' + npcmark.name + ' in the face');
                });

                it.skip('modifies the target NPC\'s specification for the worse if hit is successful', () => {
                    chai.assert.fail();
                });

            });

        });
    });

    describe('for kicking out a troublesome player', () => {
        let kick,
            goit,
            response,
            flatresponse;

        beforeEach(() => {
            flatresponse = [];
            goit = new Player('Goit', 'Griefing son of a sea cook', new PlayerSpec());
            GameDB.Players.add(goit);
            normal.addPlayer(goit);
            GameDB.Accounts.add(normal);

            testPacket.account_uid = admin.uid;
            testPacket.player_uid = testPlayer.uid;

            ActiveUIDs.addPlayer(goit);

            sandbox.spy(ActiveUIDs, 'getStoredUID');
            sandbox.spy(ActiveUIDs, 'removeStoredUID');
        });

        describe('as a user that is not at least op-level', () => {

        });

        describe('that is not logged in/does not exist', () => {
            beforeEach(() => {
                testPacket.command = 'kick Plamp';
                testPacket._baseCommand = 'kick';
                testPacket._parameters = 'Plamp';
                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });

            it('responds to the invoking operator saying the player is not there and nobody was kicked', () => {
                expect(flatresponse[0]).to.equal('Player \"Plamp\" is not in the game');
            });
        });

        describe('that is logged in', () => {

            let result;

            beforeEach(() => {
                testPacket.command = 'kick Goit';
                testPacket._baseCommand = 'kick';
                testPacket._parameters = 'Goit';
                expect(ActiveUIDs.isOnline(goit.uid)).to.equal(true);
                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });

            it('responds stating the player was kicked from the game', () => {
                expect(flatresponse[0]).to.equal('Player Goit (' + goit.uid + ') has been kicked from the game');
            });

            it('removes the player UID from the active UID list', () => {
                expect(ActiveUIDs.getStoredUID).to.be.calledWith('Goit');
                expect(ActiveUIDs.removeStoredUID).to.be.calledWith('Goit');
                expect(ActiveUIDs.isOnline(goit.uid)).to.equal(false);
            });

            it('clears the kicked player\'s location', () => {
                expect(goit.location).to.equal('');
            });
        });
    });

    describe('for displaying the help message', () => {

    });

    describe('for leaving the world', () => {
        let response,
            flatresponse;

        beforeEach(() => {
            flatresponse = [];
            ActiveUIDs.addPlayer(testPlayer);
            sandbox.spy(ActiveUIDs, 'getStoredUID');
            sandbox.spy(ActiveUIDs, 'removeStoredUID');
        });

        describe('via \'quit\'', () => {

            beforeEach(() => {
                testPacket.command = 'quit';
                testPacket._baseCommand = 'quit';
                expect(ActiveUIDs.isOnline(testPlayer.uid)).to.equal(true);
                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });

            it('removes the player\' uid from the active uid list', () => {
                expect(ActiveUIDs.removeStoredUID).to.be.calledWith(testPlayer.name);
                expect(flatresponse[0]).to.deep.equal('You disappear from the realm');
                expect(ActiveUIDs.isOnline(testPlayer.uid)).to.equal(false);
            });

            it('resets the player\'s location', () => {
                expect(testPlayer.location).to.equal('');
            });
        });

        describe('via \'leave\'', () => {

            beforeEach(() => {
                testPacket.command = 'leave';
                testPacket._baseCommand = 'leave';
                expect(ActiveUIDs.isOnline(testPlayer.uid)).to.equal(true);
                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });

            it('removes the player\' uid from the active uid list', () => {
                expect(ActiveUIDs.removeStoredUID).to.be.calledWith(testPlayer.name);
                expect(flatresponse[0]).to.deep.equal('You disappear from the realm');
                expect(ActiveUIDs.isOnline(testPlayer.uid)).to.equal(false);
            });

            it('resets the player\'s location', () => {
                expect(testPlayer.location).to.equal('');
            });
        });

        describe('via \'part\'', () => {

            beforeEach(() => {
                testPacket.command = 'part';
                testPacket._baseCommand = 'part';
                expect(ActiveUIDs.isOnline(testPlayer.uid)).to.equal(true);
                response = cmdset[testPacket._baseCommand].execute(testPacket);

                _.forEach(response, (r) => {
                    flatresponse.push(r);
                });
            });

            it('removes the player\' uid from the active uid list', () => {
                expect(ActiveUIDs.removeStoredUID).to.be.calledWith(testPlayer.name);
                expect(flatresponse[0]).to.deep.equal('You disappear from the realm');
                expect(ActiveUIDs.isOnline(testPlayer.uid)).to.equal(false);
            });

            it('resets the player\'s location', () => {
                expect(testPlayer.location).to.equal('');
            });
        });
    });
});
