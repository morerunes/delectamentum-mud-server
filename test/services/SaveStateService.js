'use strict';

const Promise = require('bluebird');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chai = require('chai');
chai.use(sinonChai);
const expect = chai.expect;

const GameDB = require('../../lib/services/GameDBService');
const SaveState = require('../../lib/services/SaveStateService');
const Player = require('../../lib/objects/things/Player');
const PlayerSpec = require('../../lib/objects/things/PlayerSpec');
const Place = require('../../lib/objects/things/Place');
const Cartograph = require('../../lib/objects/things/Cartograph');
const IO = require('../../lib/services/IOService');

describe('the SaveStateService', function () {

    let sandbox;

    let playersRef = [
        new Player('Jeff', 'a', new PlayerSpec()),
        new Player('Dale', 'b', new PlayerSpec()),
        new Player('Goit', 'c', new PlayerSpec())
    ];

    let placesRef = [
        new Place('Hell', '1', 2),
        new Place('Satan\'s Butt', '2', 2),
        new Place('Taco Bell','3', 2)
    ];

    let realmsRef = [
        new Cartograph('test','goit')
    ];
		
    beforeEach(() => {

        sandbox = sinon.sandbox.create();

        sandbox.stub(IO, 'pWrite'); // changed these to stubs, as we don't really want it to write out a file.        
        

    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('when writing Player data', () => {

        beforeEach(() => {
            sandbox.stub(GameDB.Players, 'all');
            sandbox.spy(SaveState, 'attemptSavePlayers');
        });
        
        describe('with at least one player present in memory', () => {

            beforeEach(() => {
                GameDB.Players.all.returns(playersRef);

                SaveState.attemptSavePlayers();
            });

            it('should have called GameDB.Players.all()', () => {
                expect(GameDB.Players.all).to.have.been.called;
            });

            it('should have attempted to write the players out as JSON', () => {
                expect(IO.pWrite).to.have.been.calledWith('Players.mss', JSON.stringify(playersRef));
            });

        });
      
        describe('with no players present in memory', () => {

            beforeEach(() => {
                GameDB.Players.all.returns([]);

                SaveState.attemptSavePlayers();
            });

            it('does not execute the write function', () => {
                expect(IO.pWrite).to.not.have.been.called;
            });
        });

    });

    describe('when writing Place data', () => {
        
        beforeEach(() => {
            sandbox.stub(GameDB.Places, 'all');
        });
        
        describe('with at least one place present in memory', () => {
            beforeEach(() => {
                GameDB.Places.all.returns(placesRef);

                SaveState.attemptSavePlaces();
            });

            it('should have called GameDB.Places.all()', () => {
                expect(GameDB.Places.all).to.have.been.called;
            });

            it('should have attempted to write the places out as JSON', () => {
                expect(IO.pWrite).to.have.been.calledWith('Places.mss', JSON.stringify(placesRef));
            });

        });
      
        describe('with no places in memory', () => {
            
            beforeEach(() => {
                GameDB.Places.all.returns([]);

                SaveState.attemptSavePlaces();
            });
            
            it('does not execute the write function', () => {
                expect(IO.pWrite).to.not.have.been.called;
            });
        });

    });

    describe('when writing Realm data', () => {

        beforeEach(() => {
            sandbox.stub(GameDB.Realms, 'all');
        });
        
        describe('with at least one realm present in memory', () => {
        
            beforeEach(() => {
                GameDB.Realms.all.returns(realmsRef);

                SaveState.attemptSaveRealms();
            });
  
            
            it('should have called GameDB.Realms.all()', () => {
                expect(GameDB.Realms.all).to.have.been.called;
            });

            it('should have attempted to write the realms out as JSON', () => {
                expect(IO.pWrite).to.have.been.calledWith('Realms.mss', JSON.stringify(realmsRef));
            });

        });
      
        describe('with no realms in memory', () => {

            beforeEach(() => {
                GameDB.Realms.all.returns([]);

                SaveState.attemptSaveRealms();
            });

            it('does not execute the write function', () => {
                expect(IO.pWrite).to.not.have.been.called;
            });

        });

    });

    describe('when reading Player data', () => {

        let readPromise;
        let data;
        
        describe('using a populated file', () => {
            
            beforeEach(() => {

                sandbox.stub(IO, 'pRead', () => {
                    return new Promise(function(resolve) {
                        resolve(data);
                    });
                });

                data = JSON.stringify(playersRef);
                
                readPromise = SaveState.attemptReadPlayers();
                
            });

            it('should extract the file\'s contents as a list of players', () => {
                return readPromise
                        .then(function(playerList){
                            expect(playerList).to.deep.equal(playersRef);
                        });
            });

            it('should call IO.pRead', () => {
                expect(IO.pRead).to.have.been.calledWith('Players.mss');
            });
            
        });
      
        describe('using a file that doesn\'t exist', () => {

            beforeEach(() => {

                sandbox.stub(IO, 'pRead', () => {
                    return new Promise(function(resolve, reject) {
                        reject('goit');
                    });
                });

                readPromise = SaveState.attemptReadPlayers();
                
            });

            it('should reject with an error', () => {

                return readPromise.then(function(){
                    chai.assert.fail('should have rejected');
                }).catch(function(err){
                    expect(err.message).to.equal('Players file does not exist');
                });
            });

        });

    });

    describe('when reading Place data', () => {
      
        let readPromise;  
        let data;

        describe('using a populated file', () => {

            beforeEach(() => {

                sandbox.stub(IO, 'pRead', () => {
                    return new Promise(function(resolve) {
                        resolve(data);
                    });
                });

                data = JSON.stringify(placesRef);

                readPromise = SaveState.attemptReadPlaces();
                
            });

            it('should extract the file\'s contents as a list of places', () => {
                return readPromise
                        .then(function(placeList){
                            expect(placeList).to.deep.equal(placesRef);
                        });
            });

            it('should call IO.pRead', () => {
                expect(IO.pRead).to.have.been.calledWith('Places.mss');
            });

        });
      
        describe('using a file that doesn\'t exist', () => {

            beforeEach(() => {

                sandbox.stub(IO, 'pRead', () => {
                    return new Promise(function(resolve, reject) {
                        reject('goit');
                    });
                });

                readPromise = SaveState.attemptReadPlaces();
                
            });

            it('should reject with an error', () => {
                return readPromise.then(function(){
                    chai.assert.fail('should have rejected');
                }).catch(function(err){
                    expect(err.message).to.equal('Places file does not exist');
                });
            });

        });

    });

    describe('when reading Realm data', () => {

        let readPromise;
        let data;
        
        describe('using a populated file', () => {
        
            beforeEach(() => {

                sandbox.stub(IO, 'pRead', () => {
                    return new Promise(function(resolve) {
                        resolve(data);
                    });
                });

                data = JSON.stringify(realmsRef);

                readPromise = SaveState.attemptReadRealms();

            });

            it('should extract the file\'s contents as a list of realms', () => {
                return readPromise
                        .then(function(realmList){
                            expect(realmList).to.deep.equal(realmsRef);
                        });
            });

            it('should call IO.pRead', () => {
                expect(IO.pRead).to.have.been.calledWith('Realms.mss');
            });

        });
      
        describe('using a file that doesn\'t exist', () => {

            beforeEach(() => {

                sandbox.stub(IO, 'pRead', () => {
                    return new Promise(function(resolve, reject) {
                        reject('goit');
                    });
                });

                readPromise = SaveState.attemptReadRealms();
                
            });

            it('should reject with an error', () => {
                return readPromise.then(function(){
                    chai.assert.fail('should have rejected');
                }).catch(function(err){
                    expect(err.message).to.equal('Realms file does not exist');
                });
            });

        });

    });
    
});
