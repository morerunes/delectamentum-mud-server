'use strict';

const Promise = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const _ = require('lodash');
const expect = chai.expect;

const Cartograph = require('../../lib/objects/things/Cartograph');
const config = require('../../lib/services/ConfigService');
const log = require('../../lib/services/LogService');
const Game = require('../../lib/services/GameService');
const GameDB = require('../../lib/services/GameDBService');
const Player = require('../../lib/objects/things/Player');
const Account = require('../../lib/objects/things/Account');
const PlayerSpec = require('../../lib/objects/things/PlayerSpec');
const Place = require('../../lib/objects/things/Place');
const Thing = require('../../lib/objects/things/Thing');
const MsgSvc = require('../../lib/services/MsgService');

describe('the GameService\'s', function () {

    let cmdset,
        testPlayer,
        sandbox,
        admin,
        normal;

    this.timeout(5000);

    before(() => {
        GameDB.init();
    });

    beforeEach(function () {
        sandbox = sinon.sandbox.create();
        sandbox.stub(MsgSvc, 'notifySingleUser');
        config.init('CRITICAL');
    });

    afterEach(() => {
        GameDB.reset();
        sandbox.restore();
    });

    describe('reset function', function () {

        beforeEach(() => {
            testPlayer = new Player('test','guinea pig', new PlayerSpec());
            admin = new Account('test@goit.com', 'admin');
            normal = new Account('a@plamp.goit');
            admin.addPlayer(testPlayer);

            GameDB.Players.add(testPlayer);
            GameDB.Accounts.add(admin);
        });

        it('stops the game', (done) => {
            Game.start();
            Game.onStop(() => { done(); });
            Game.reset();
        });

        it('clears the command buffer', () => {
            Game.attemptAddPlayer({
                account_uid: admin.uid,
                player_uid: testPlayer.uid
            });
            Game.start();
            expect(Game.queuedTransactionCount()).to.equal(0);
            Game.offerTransaction({
                account_uid: admin.uid,
                player_uid: testPlayer.uid,
                command: 'north'
            });
            Game.offerTransaction({
                account_uid: admin.uid,
                player_uid: testPlayer.uid,
                command: 'east'
            });
            expect(Game.queuedTransactionCount()).to.equal(2);
            Game.reset();
            expect(Game.queuedTransactionCount()).to.equal(0);
        });

        it('clears the set of active players[\' uids]', (done) => {
            Game.start();
            Game.attemptAddPlayer({
                account_uid: admin.uid,
                player_uid: testPlayer.uid
            });
            expect(Game.onlinePlayerCount()).to.equal(1);
            Game.reset();
            Game.onStop(() => {
                expect(Game.onlinePlayerCount()).to.equal(0);
                done();
            });
        });

        it('clears the set of active NPC uids', (done) => {

            let scrubPC = new Player('A Scrub', 'A Stooge');

            GameDB.NPCs.add(scrubPC);

            Game.start();
            Game.forceSpawnNPC(scrubPC.uid);
            expect(Game.onlineNPCCount()).to.equal(1);
            Game.onStop(() => {
                expect(Game.onlineNPCCount()).to.equal(0);
                done();
            });
            Game.reset();
        });

    });

    describe('Game Loop', function () {

        afterEach(() => {
            Game.reset();
        });

        describe('onInit function', function () {

            it('should only accept functions', () => {
                expect(function () {
                    Game.onInit('honk');
                }).to.throw(Error, 'Can only add functions as onInit callbacks');

                expect(function () {
                    Game.onInit(function () {});
                }).to.not.throw(Error);
            });

        });

        describe('start function', function () {

            it('should run every init callback', (done) => {

                let finished = 0;

                let promises = [Promise.defer(), Promise.defer(), Promise.defer()];
                _.each(promises, function (promise) {
                    Game.onInit(function () {
                        finished += 1;
                        promise.resolve();
                    });
                });

                Promise.all(_.flatMap(promises, (promise) => {
                    return promise.promise;
                })).then(function () {
                    expect(finished).to.equal(3);
                    done();
                });

                Game.start();
                Game.stop();

            });

            it('should not let you start the game while it is already running', (done) => {
                Game.start();

                expect(function () {
                    Game.start();
                }).to.throw(Error, 'Game is already running');

                Game.onStop(done);
                Game.stop();
            });

        });

        describe('onStop function', function () {

            it('should only accept functions', () => {
                expect(function () {
                    Game.onStop('hello');
                }).to.throw(Error, 'Can only give functions to onStop');

                expect(function () {
                    Game.onStop(function () {});
                }).to.not.throw(Error);
            });

        });

        describe('when stopped', function () {

            it('should run every stop callback', (done) => {

                let finished = 0;
                let promises = [Promise.defer(), Promise.defer(), Promise.defer()];
                _.each(promises, function (promise) {
                    Game.onStop(function () {
                        finished += 1;
                        promise.resolve();
                    });
                });

                Promise.all(_.flatMap(promises, (promise) => {
                    return promise.promise;
                })).then(function () {
                    expect(finished).to.equal(3);
                    done();
                });

                Game.start();
                Game.stop();
            });
        });

        describe('when offered a command', () => {

            let command,
                normal,
                testPacket,
                waitforCmd,
                transactionId;

            beforeEach(() => {

                return Game.start().then(function () {

                    testPlayer = new Player('test','guinea pig', new PlayerSpec());
                    admin = new Account('test@goit.com', 'admin');
                    normal = new Account('plamp@plamp.com');

                    admin.addPlayer(testPlayer);

                    testPacket = {
                        account_uid: admin.uid,
                        player_uid: testPlayer.uid
                    };

                    GameDB.Players.add(testPlayer);
                    GameDB.Accounts.add(admin);
                });
            });

            afterEach(() => {
                Game.reset();
            });

            describe('without the invoking account\' UID, the player\'s UID, or a [empty at minimum] command', () => {
                it('rejects the bad request immediately', () =>{
                    expect(() => {
                        Game.offerTransaction({});
                    }).to.throw(Error, 'Packet sent in wrong format');
                });
            });

            describe('with a uid of an offline player', () => {
                it('rejects the request immediately', () => {
                    testPacket.player_uid = 'goit';
                    testPacket.command = 'plamp';
                    expect(() => {
                        Game.offerTransaction(testPacket);
                    }).to.throw(Error, 'Player goit is not active');
                });
            });

            describe('containing an unsupported command', () => {
                beforeEach((done) => {
                    Game.attemptAddPlayer(testPacket);
                    testPacket.command = 'goit';
                    transactionId = Game.offerTransaction(testPacket);

                    waitforCmd = setInterval(() => {
                        if (!Game.inspectTransaction(transactionId)) {
                            clearInterval(waitforCmd);
                            done();
                        }
                    }, 1);
                });

                it('responds with a message indicating nothing happened', () => {
                    let result = MsgSvc.notifySingleUser.getCall(0).args[0];

                    expect(result.account_uid).to.equal(testPacket.account_uid);
                    expect(result.player_uid).to.equal(testPacket.player_uid);
                    expect(result.response).to.equal('What the hell is this? :L');
                });
            });

            describe('containing a supported command', () => {

                beforeEach((done) => {
                    Game.attemptAddPlayer(testPacket);
                    testPacket.command = 'xp';
                    transactionId = Game.offerTransaction(testPacket);
                    expect(Game.queuedTransactionCount()).to.equal(1);

                    waitforCmd = setInterval(() => {
                        if (!Game.inspectTransaction(transactionId)) {
                            clearInterval(waitforCmd);
                            done();
                        }
                    }, 1);
                });

                it('gives back the command\'s responses for further processing', () => {
                    let result = MsgSvc.notifySingleUser.getCall(0).args[0];

                    expect(result.account_uid).to.equal(testPacket.account_uid);
                    expect(result.player_uid).to.equal(testPacket.player_uid);
                    expect(result.response.length).to.be.gte(1);
                });

            });
        });
    });
});
