'use strict';

const Promise = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const _ = require('lodash');
const expect = chai.expect;

const config = require('../../lib/services/ConfigService');
const Realms = require('../../lib/objects/Realms');
const Places = require('../../lib/objects/Places');
const Players = require('../../lib/objects/Players');
const Accounts = require('../../lib/objects/Accounts');
const Player = require('../../lib/objects/things/Player');
const Account = require('../../lib/objects/things/Account');
const PlayerSpec = require('../../lib/objects/things/PlayerSpec');
const Place = require('../../lib/objects/things/Place');
const Cartograph = require('../../lib/objects/things/Cartograph');
const GameDB = require('../../lib/services/GameDBService');

describe('The Game DB service', () => {

    beforeEach(() => {
        config.init('CRITICAL');
        GameDB.init();
    });

    afterEach(() => {
        GameDB.reset();
    });

    describe('when started', () => {
        it('sets up the Ether with the Lobby', () => {
            expect(GameDB.Realms.Ether.at(0, 0)).to.deep.equal(GameDB.Places.Lobby);
        });
    });

    describe('when asked to restore previously persisted data', () => {

        let playerlist,
            placelist,
            realmlist;

        beforeEach(() => {

            playerlist = [
                new Player('a','a'),
                new Player('b','b'),
                new Player('c','c')
            ];

            placelist = [
            ];

            realmlist = [
            ];

        });

    });

    describe('when moving a place', function () {

        let place;

        beforeEach(() => {
            place = new Place('test place', 'test place', 1);
        });

        it('should error if the place uid does not match any place', function () {
            expect(function () {
                GameDB.movePlace('bad');
            }).to.throw(Error, 'bad does not match any known Places');
        });

        it('should throw an error if the Realm uid does not match any Cartograph', function () {
            GameDB.Places.add(place);

            expect(function () {
                GameDB.movePlace(place.uid, 'bad', 0, 0);
            }).to.throw('bad does not match any known Cartograph');
        });

        it('should throw an error if you try to move the Lobby', function () {
            expect(function () {
                GameDB.movePlace(GameDB.Places.Lobby.uid, 'any cartograph', 0, 0);
            }).to.throw(Error, 'Cannot move the Lobby');
        });

        it('should throw an error if you try to move something to an occupied space', function () {
            GameDB.Places.add(place);

            expect(function () {
                GameDB.movePlace(place.uid, GameDB.Realms.Ether.uid, 0, 0);
            }).to.throw(Error, 'Space [0, 0] is already occupied by Lobby in Ether');
        });

        it('should relocate the place if possible', function () {
            GameDB.Places.add(place);
            GameDB.movePlace(place.uid, GameDB.Realms.Ether.uid, 100, 100);
            expect(place.location).to.deep.equal({
                realm: GameDB.Realms.Ether.uid,
                X: 100,
                Y: 100
            });

            expect(GameDB.Realms.Ether.at(100, 100)).to.equal(place);
        });

    });


    describe('when asked to reset', () => {

        it('resets Players', () => {

            GameDB.Players.add(new Player('test'));
            expect(GameDB.Players.totalCount).to.equal(1);

            GameDB.reset();
            expect(GameDB.Players.totalCount).to.equal(0);
        });

        it('resets Places', () => {

            GameDB.Places.add(new Place('test', 'test place', 1));
            GameDB.reset();
            expect(GameDB.Places.totalCount).to.equal(1);

        });

        it('resets Realms', () => {

            GameDB.Realms.add(new Cartograph('test', 'test'));
            GameDB.reset();
            expect(GameDB.Realms.totalCount).to.equal(1);

        });

        it('resets Accounts', () => {
            GameDB.Accounts.add(new Account('test@test.com'));
            expect(GameDB.Accounts.totalCount).to.equal(1);
            GameDB.reset();
            expect(GameDB.Accounts.totalCount).to.equal(0);
        });

    });

    it('has all the entity sets immediately available', () => {
        expect(GameDB.Players).to.not.be.undefined;
        expect(GameDB.Places).to.not.be.undefined;
        expect(GameDB.Realms).to.not.be.undefined;
        expect(GameDB.Accounts).to.not.be.undefined;
    });

    it('has the Lobby and Ether by default', () => {
        expect(GameDB.Realms.Ether).to.not.be.undefined;
        expect(GameDB.Places.Lobby).to.not.be.undefined;
    });
});
