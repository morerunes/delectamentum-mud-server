'use strict';

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const expect = chai.expect;
const fs = require('fs');

const IO = require('../../lib/services/IOService');

describe('the IOService', function () {

    let sandbox;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('when writing a file', function () {

        let error;

        beforeEach(() => {

            sandbox.stub(fs, 'writeFile', (_filename, _data, _options, errorHandler) => {
                errorHandler(error);
            });

        });

        describe('when there is an error', () => {

            let writePromise;

            beforeEach(() => {
                error = new Error('some problem');

                writePromise = IO.pWrite('some filename', 'some data');
            });

            it('should have called fs.writeFile', () =>{
                expect(fs.writeFile).to.have.been.calledWith('some filename', 'some data', 'utf-8');
                writePromise.catch(err => 'subdue unhandled exception');
            });

            it('should reject with an error', () => {

                let me = this;
                
                return writePromise
                       .then(function() {
                           me.fail('should have rejected');
                       })
                       .catch(function(err) {
                           expect(err).to.equal('some problem');
                       });
            });

        });

        describe('when there is no error', () => {

            let writePromise;
            let me;

            beforeEach(() => {
                error = false;

                writePromise = IO.pWrite('some filename', 'some data');
            });

            it('should have called fs.writeFile', () =>{
                expect(fs.writeFile).to.have.been.calledWith('some filename', 'some data', 'utf-8');
            });

            it('should not reject with an error', function() {
                
                return writePromise
                       .then(function() {
                           expect(error).to.equal(false); 
                       })
                       .catch(function(error){
                           me.fail('shouldn\'t have failed');
                       });      
            });

        });

    });

    describe('when reading a file', function () {

        let error;
        let data;
        let readPromise;

        beforeEach(() => {
            sandbox.stub(fs, 'readFile', (path, options, callback) => {
                callback(error, data);
            });

            data = 'some data';
        });

        describe('and there is no error', function () {

            beforeEach(() => {
                readPromise = IO.pRead('some filename');
            });

            it('should have called fs.readFile', function () {
                expect(fs.readFile).to.have.been.calledWith('some filename', 'utf-8');
            });

            it('should return the data in the test file', () => {

                return readPromise.then(function (result) {
                    expect(result).to.equal('some data');
                });

            });

        });

        describe('and there is an error', function () {

            beforeEach(() => {
                error = new Error('some error');
                readPromise = IO.pRead('some filename');
            });

            it('should have called fs.readFile', function () {
                expect(fs.readFile).to.have.been.calledWith('some filename', 'utf-8');
                readPromise.catch(err => 'subdue unhandled exception');
            });

            it('should reject with the error message', function () {
                let me = this;

                return readPromise.then(function () {
                    me.fail('should have rejected');
                }).catch(function (err) {
                    expect(err).to.equal('some error');
                });

            });

        });

    });

});
