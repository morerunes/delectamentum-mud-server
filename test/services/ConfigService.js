'use strict';

const _ = require('lodash');
const chai = require('chai');
const expect = chai.expect;

const config = require('../../lib/services/ConfigService');
const configFile = require('../../config.json');

describe('The config service', function () {

    beforeEach(function () {
        config.init();
    });

    it('should exist', function () {
        expect(config).to.be.an('object');
    });
    
    it('should have an init function to load config', function () {
        expect(config.init).to.be.a('function');
    });

    it('should load config options from config.js on init', function () {
        config.set('logLevel', 'test');
        expect(config.option('logLevel')).to.equal('test');
        config.init();
        expect(config.option('logLevel')).to.equal(configFile.logLevel);
    });

    it('should override the log level if provided to init', function () {
        config.init('ERROR');
        expect(config.option('logLevel')).to.equal('ERROR');
    });

    it('should have a set function that overwrites config options', function () {
        config.set('logLevel', 'ERROR');
        expect(config.option('logLevel')).to.equal('ERROR');
    });
    
    it('should have an option function to retrieve config values', function () {
        expect(config.option).to.be.a('function');
        expect(config.option('logLevel')).to.equal('DEBUG');
    });

    it('should load every option from config.json on init', function () {
        _.each(configFile, function (value, key) {
            expect(config.option(key)).to.equal(value);
        });
    });

});
