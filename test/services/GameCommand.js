'use strict';

const Promise = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const _ = require('lodash');
const expect = chai.expect;

const GameCommand = require('../../lib/services/GameCommand');
const GameDB = require('../../lib/services/GameDBService');
const config = require('../../lib/services/ConfigService');

describe('A GameCommand object', () => {

    let sandbox;

    before(() => {
        GameDB.init();
        config.init('CRITICAL');
    });

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    describe('when created', () => {
        describe('without a command name', () => {
            it('throws an error for a bad command name', () => {
                expect(() => {
                    let cmd = new GameCommand();
                }).to.throw(Error, 'A command name is required');
            });
        });

        describe('with an invalid name containing white space', () => {
            it('throws an error for a bad command name', () => {
                expect(() => {
                    let cmd = new GameCommand('   goit   ');
                }).to.throw(Error, 'A command name can only take letters');
            });
        });

        describe('with an invalid name containing special characters', () => {
            it('throws an error for a bad command name', () => {
                expect(() => {
                    let cmd = new GameCommand('go!t');
                }).to.throw(Error, 'A command name can only take letters');
            });
        });

        describe('with an invalid name containing numbers', () => {
            it('throws an error for a bad command name', () => {
                expect(() => {
                    let cmd = new GameCommand('g01t');
                }).to.throw(Error, 'A command name can only take letters');
            });
        });

        describe('with an invalid name containing numbers, whitespace, and special characters', () => {
            it('throws an error for a bad command name', () => {
                expect(() => {
                    let cmd = new GameCommand('    g0!t  ');
                }).to.throw(Error, 'A command name can only take letters');
            });
        });

        describe('with a valid name (only letters)', () => {

            let cmd1,
                cmd2,
                cmd3,
                dummy;

            beforeEach(() => {
                cmd1 = new GameCommand('goit');
                cmd2 = new GameCommand('g');
            });

            it('creates the command object with the correct name', () => {
                expect(cmd1.name).to.equal('goit');
                expect(cmd2.name).to.equal('g');
            });

            it('initializes with no post-exec hooks', () => {
                expect(cmd1.afterEffects).to.deep.equal([]);
                expect(cmd2.afterEffects).to.deep.equal([]);
            });

            it('initializes without a handler, if unprovided', () => {
                expect(cmd1.handler).to.be.undefined;
                expect(cmd2.handler).to.be.undefined;
            });

            it('initializes with a [valid] handler, if provided', () => {
                expect(() => {
                    let x = new GameCommand('honk', goit => {});
                }).to.not.throw(Error);
            });
        });
    });

    describe('when specifying a main handler', () => {

        let goodfcn,
            badfcn,
            cmd;

        beforeEach(() => {
            goodfcn = function(packet) {};
            badfcn = function(i, am, a, goit) {};
            cmd = new GameCommand('test');
        });

        describe('on instantiation', () => {

            describe('that is not a function', () => {
                it('rejects the non-function by throwing an error', () => {
                    expect(() => {
                        let x = new GameCommand('goit', 'I\'m a goit');
                    }).to.throw(Error, 'The command handler must be a function');
                });
            });

            describe('that takes more or less than the standard data packet', () => {
                it('rejects the function by throwing an error', () => {
                    expect(() => {
                        let x = new GameCommand('goit', badfcn);
                    }).to.throw(Error, 'The command handler must take only one argument');
                });
            });

            describe('that is valid', () => {
                it('sets the command\'s handler to the given function', () => {
                    let x = new GameCommand('honk', goodfcn);
                    expect(x.handler).to.equal(goodfcn);
                });
            });
        });

        describe('after instantiation', () => {
            describe('that is not a function', () => {
                it('rejects the non-function by throwing an error', () => {
                    expect(() => {
                        cmd.handler = 'goodfcn';
                    }).to.throw(Error, 'The command handler must be a function');
                });
            });

            describe('that takes more or less than the standard data packet', () => {
                it('rejects the function by throwing an error', () => {
                    expect(() => {
                        cmd.handler = badfcn;
                    }).to.throw(Error, 'The command handler must take only one argument');
                });
            });

            describe('that is valid', () => {
                it('sets the command\'s handler to the given function', () => {
                    cmd.handler = goodfcn;
                    expect(cmd.handler).to.equal(goodfcn);
                });
            });
        });
    });

    describe('when adding a post-execution hook', () => {
        describe('that is not a function', () => {
            it('rejects the non-function by throwing an error', () => {
                expect(() => {
                    let x = new GameCommand('goit');
                    x.addAfterEffect('I\'m a goit');
                }).to.throw(Error, 'A command after effect must be a function');
            });
        });

        describe('that takes more or less than the standard data packet', () => {
            it('rejects the function by throwing an error', () => {
                expect(() => {
                    let x = new GameCommand('goit');
                    x.addAfterEffect(() => {});
                }).to.throw(Error, 'A command after effect can only take one argument');
            });
        });

        describe('that is valid', () => {
            it('adds the callback to the command\'s after-effect list', () => {
                let fcn = packet => {};
                let x = new GameCommand('honk', x => {});
                x.addAfterEffect(fcn);
                expect(x.afterEffects[0]).to.equal(fcn);
                expect(x.afterEffects.length).to.equal(1);
            });
        });
    });

    describe('when executed', () => {

        let cmd;

        describe('and the handler or an after-effect fails', () => {

            let brokencmd,
                brokenfcn;

            beforeEach(() => {
                brokencmd = new GameCommand('plamp', x => {
                    return 'Honk.';
                });

                brokencmd.addAfterEffect(x => {
                    throw new Error('I\'m a stupid goit');
                });
            });

            it('fails with an error', () => {
                expect(() => {
                    brokencmd.execute({});
                }).to.throw(Error);
            });

        });

        describe('with a missing handler', () => {

            beforeEach(() => {
                cmd = new GameCommand('goit');
            });

            it('does not error out', () => {
                expect(() => {
                    cmd.execute();
                }).to.not.throw(Error);
            });

            it('returns an empty response list', () => {
                expect(cmd.execute()).to.deep.equal([]);
            });
        });

        describe('with a valid handler', () => {

            let donecount;

            beforeEach(() => {
                cmd = new GameCommand('goit', packet => {
                    return 'I am the main handler';
                });
            });

            describe('and no post-execution effects', () => {
                it('returns a list containing the original handler\'s response', () => {
                    let responses = cmd.execute({});
                    expect(responses.length).to.equal(1);
                    expect(responses[0]).to.equal('I am the main handler');
                });
            });

            describe('and one post-execution effect', () => {

                let responses;

                beforeEach(() => {
                    cmd.addAfterEffect(p => {
                        return 'I am an after effect';
                    });

                    responses = cmd.execute({});
                });

                it('returns a list containing the handler\'s and the after-effect\'s reponses', () => {
                    expect(responses.length).to.equal(2);
                });

                it('returns the list with the responses in the correct order (handler first, after-effect... after)', () => {
                    expect(responses[0]).to.equal('I am the main handler');
                    expect(responses[1]).to.equal('I am an after effect');
                });

            });

            describe('and more than one post-execution effect', () => {
                let responses;

                beforeEach(() => {
                    cmd.addAfterEffect(p => {
                        return 'I am an after effect';
                    });

                    cmd.addAfterEffect(p => {
                        return 'I am another after effect';
                    });

                    responses = cmd.execute({});
                });

                it('returns a list containing the handler\'s and the after-effects\' reponses', () => {
                    expect(responses.length).to.equal(3);
                });

                it('returns the list with the responses in the correct order (handler first, after-effects.. after)', () => {
                    expect(responses[0]).to.equal('I am the main handler');
                    expect(responses[1]).to.equal('I am an after effect');
                    expect(responses[2]).to.equal('I am another after effect');
                });
            });
        });

    });
});
