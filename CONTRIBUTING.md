If you change anything, make sure it is tested please! Other than that, keep 
your pull requests fairly clean and linted. This project is mostly just for 
fun, and I'd like to keep it that way - fun!