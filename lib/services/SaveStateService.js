'use strict';

const GameDB = require('../services/GameDBService');
const IO = require('../services/IOService');

/*
 * SaveStateService.js
 *
 * Utility for saving the MUD's state to file.
 *
 *
 *
 *
 */

module.exports.attemptSavePlayers = function () {
    
    let playerList = GameDB.Players.all();

    if(playerList.length == 0)
        return;

    let playerList_out = JSON.stringify(playerList);

    return IO.pWrite('Players.mss', playerList_out);
};

module.exports.attemptReadPlayers = function () {

    return IO.pRead('Players.mss')
            .then(function(data) {
                return Promise.resolve(JSON.parse(data));
            })
            .catch(function(err) {
                throw new Error('Players file does not exist');
            });

};

module.exports.attemptSavePlaces = function () {

    let placeList = GameDB.Places.all();

    if(placeList.length == 0)
        return;

    let placeList_out = JSON.stringify(placeList);

    return IO.pWrite('Places.mss', placeList_out);
};

module.exports.attemptReadPlaces = function () {

    return IO.pRead('Places.mss')
            .then(function(data) {
                return Promise.resolve(JSON.parse(data));
            })
            .catch(function(err) {
                throw new Error('Places file does not exist');
            });

};

module.exports.attemptSaveRealms = function () {

    let realmList = GameDB.Realms.all();

    if(realmList.length == 0)
        return;

    let realmList_out = JSON.stringify(realmList);

    return IO.pWrite('Realms.mss', realmList_out);

};

module.exports.attemptReadRealms = function () {

    return IO.pRead('Realms.mss')
            .then(function(data) {
                return Promise.resolve(JSON.parse(data));
            })
            .catch(function(err) {
                throw new Error('Realms file does not exist');
            });
    
};
