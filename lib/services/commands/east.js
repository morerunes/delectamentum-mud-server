'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let east     = new GameCommand('east'),
    east_abb = new GameCommand('e');

function moveeast(packet) {
    let player = GameDB.Players.byId(packet.player_uid);
    if(!player){
        throw new Error('Player ' + packet.player_uid + ' is a myth :(');
    }

    let curPlace = GameDB.Places.byId(player.location);
    let curRealm = GameDB.Realms.byId(curPlace.location.realm);

    if(!curPlace){
        throw new Error('The Place (' + player.location + ') is faked :(');
    }
    if(!curRealm){
        throw new Error('Realm ' + player.location.realm + ' doesn\'t exist');
    }

    let destination = curRealm.at(curPlace.location.X + 1, curPlace.location.Y);

    if(!destination) {
        return 'You walk into a wall';
    } else {
        player.moveTo(destination.uid);
        packet.__playerDelta.location = destination.uid;
        return 'You now find yourself in ' + destination.description;
    }
}

function postMovementTrigger(packet) {

    return 'You hear an unexpected noise after entering.';
}

east.handler = moveeast;
east_abb.handler = moveeast;

east.addAfterEffect = postMovementTrigger;
east_abb.addAfterEffect = postMovementTrigger;

module.exports = [
    east,
    east_abb
];
