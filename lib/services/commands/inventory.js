'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let inventory = new GameCommand('inventory'),
    inv       = new GameCommand('inv'),
    i         = new GameCommand('i');

function showInventory(packet) {
    let player = GameDB.Players.byId(packet.player_uid);

    return JSON.stringify(player.contents);
}

inventory.handler = showInventory;
inv.handler       = showInventory;
i.handler         = showInventory;

module.exports = [
    inventory,
    inv,
    i
];
