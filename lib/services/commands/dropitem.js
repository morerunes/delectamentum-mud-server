'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let drop = new GameCommand('drop'),
    dump = new GameCommand('dump');

function dumpItem(packet) {
    let player = GameDB.Players.byId(packet.player_uid),
        place = GameDB.Places.byId(player.location);

    let contraband = player.take(packet._parameters);

    if(!contraband)
        return 'You gave the place a fart';

    if(!place.accepts(contraband)){
        player.give(contraband);
        return 'Some odd force prevents you from leaving the ' + contraband.name + ' behind';
    }

    place.give(contraband);

    return 'You dropped the ' + contraband.name;
}

function postDrop(packet) {
    return 'Some shit might happen here (post-item drop)';
}

drop.handler = dumpItem;
dump.handler = dumpItem;

drop.addAfterEffect(postDrop);
dump.addAfterEffect(postDrop);

module.exports = [
    drop,
    dump
];
