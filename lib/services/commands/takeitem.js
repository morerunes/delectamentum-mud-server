'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let get = new GameCommand('get'),
    take = new GameCommand('take');

function takeItem(packet) {
    let player = GameDB.Players.byId(packet.player_uid),
        place = GameDB.Places.byId(player.location);

    if(!isNaN(parseFloat(packet._parameters)) && isFinite(packet._parameters)) {

        let amount = parseFloat(packet._parameters);

        if(amount < 0) {
            return 'Nice try, goit. Negative numbers don\'t get you anywhere';
        }

        if(amount % 1 > 0) {
            return 'I don\'t deal in cents, fam';
        }

        if(amount > place.contents['currency_amt']) {

            let amount2 = place.contents['currency_amt'];
            place.contents['currency_amt'] = 0;
            player.contents['currency_amt'] += amount2;

            return 'You took only ' + amount2 + ' gold from the room, because it didn\'t have ' + amount + ' gold in it';

        } else {
            place.contents['currency_amt'] -= amount;
            player.contents['currency_amt'] += amount;

            return 'You snatched ' + amount + ' gold from the room';
        }

    } else {

        let contraband = place.take(packet._parameters);

        if(!contraband)
            return 'You just tried to take a whole load of nothing';

        if(!player.accepts(contraband)){
            place.give(contraband);
            return 'You are unable to take the ' + contraband.name;
        }

        player.give(contraband);

        return 'You take the ' + contraband.name;
    }
}

function postTake(packet) {
    return 'Some shit might happen here (post-item grab)';
}

get.handler = takeItem;
take.handler = takeItem;

get.addAfterEffect(postTake);
take.addAfterEffect(postTake);

module.exports = [
    get,
    take
];
