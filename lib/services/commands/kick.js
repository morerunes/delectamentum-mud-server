'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');
let ActiveUIDs = require('../../services/ActiveSessionCache');

let kick = new GameCommand('kick');

function kickPlayer(packet) {

    let acc = GameDB.Accounts.byId(packet.account_uid);

    if(!acc) {
        throw new Error('Inactive account attempted a player kick');
    }

    if(GameDB.Accounts.byId(packet.account_uid).account_type != 'admin')
        return 'Non-admins cannot kick other players out.';

    let goituid = ActiveUIDs.getStoredUID(packet._parameters);

    if(!goituid)
        return 'Player \"' + packet._parameters + '\" is not in the game';

    let goit = GameDB.Players.byId(goituid);
    GameDB.Players.remove(goituid);
    ActiveUIDs.removeStoredUID(goit.name);

    return 'Player ' + goit.name + ' (' + goituid + ') has been kicked from the game';

}

kick.handler = kickPlayer;

module.exports = [
    kick
];
