'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let ex = new GameCommand('experience'),
    xp  = new GameCommand('xp'),
    exp  = new GameCommand('exp');


function showXP(packet) {
    let player = GameDB.Players.byId(packet.player_uid);

    return 'Your XP: ' + player.spec.xp;
}

exp.handler = showXP;
xp.handler  = showXP;
ex.handler  = showXP;

module.exports = [
    ex,
    xp,
    exp
];
