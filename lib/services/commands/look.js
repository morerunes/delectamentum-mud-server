'use strict';

let _ = require('lodash');
let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let look = new GameCommand('look');

function lookAround(packet) {

    let player = GameDB.Players.byId(packet.player_uid);
    let curLocation = GameDB.Places.byId(player.location);
    let thinglist = '';

    _.forEach(curLocation.contents, (thing) => {
        thinglist += '\n\t' + thing.name;
    });

    return 'The place you find yourself in is ' + curLocation.description +
           '\nAfter a look around, you identify the following things :' + thinglist;
}

look.handler = lookAround;

module.exports = [
    look
];
