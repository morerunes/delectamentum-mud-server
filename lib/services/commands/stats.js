'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let stats = new GameCommand('stats'),
    st    = new GameCommand('st');

function showStats(packet) {

    let player = GameDB.Players.byId(packet.player_uid);

    if(!GameDB.Accounts.byId(packet.account_uid).getPlayer(packet.player_uid)
      || GameDB.Accounts.byId(packet.account_uid).account_type != 'admin')
        return 'I\'m not showing you a damn thing ;)';

    return 'Player: ' + player.name +
              '\n' + player.description +
              '\n\tLevel: ' + player.spec.level +
              '\n\tXP: ' + player.spec.xp;
}

stats.handler = showStats;
st.handler    = showStats;

module.exports = [
    stats,
    st
];
