'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let help = new GameCommand('help'),
    h    = new GameCommand('h');

function helpMessage(packet) {
    return 'I am an unhelpful goit';
}

help.handler = helpMessage;
h.handler = helpMessage;

module.exports = [
    help,
    h
];
