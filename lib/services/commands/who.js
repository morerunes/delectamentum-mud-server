'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');
let _ = require('lodash');

let who = new GameCommand('who');

function showPlayers(packet) {
    let playerListStr = 'Players currently online:\n';

    _.forEach(GameDB.Players.all(), (player) => {
        playerListStr += '\t' + player.name + ', ' + player.description + '\n';
    });

    return playerListStr;
}

who.handler = showPlayers;

module.exports = [
    who
];
