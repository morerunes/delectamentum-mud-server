'use strict';

// **Please do not import the example command template**

module.exports.attack = require('./attack');

module.exports.dropitem = require('./dropitem');
module.exports.takeitem = require('./takeitem');
//module.exports.useitem = require('./useitem');
module.exports.inventory = require('./inventory');
module.exports.look = require('./look');

module.exports.north = require('./north');
module.exports.south = require('./south');
module.exports.east = require('./east');
module.exports.west = require('./west');

module.exports.help = require('./help');
module.exports.quit = require('./quit');
module.exports.kick = require('./kick');
module.exports.stats = require('./stats');
module.exports.xp = require('./xp');
module.exports.who = require('./who');
