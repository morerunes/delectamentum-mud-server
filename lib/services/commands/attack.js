'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');
let log = require('../../services/LogService');
let ActiveUIDs = require('../../services/ActiveSessionCache');

let attack = new GameCommand('attack'),
    a      = new GameCommand('a'),
    whack  = new GameCommand('whack'),
    smack  = new GameCommand('smack');

function attackPlayer(packet) {

    let player = GameDB.Players.byId(packet.player_uid),
        victimuid = ActiveUIDs.getStoredUID(packet._parameters),
        victim,
        dmg;

    if(!victimuid){
        return player.name + ' tried to fight air';
    } else {

        victim = GameDB.Players.byId(victimuid) ? GameDB.Players.byId(victimuid) : GameDB.NPCs.byId(victimuid);
        if(!victim){
            log.error(`Why is ${victimuid} in the cache when that player already died/despawned?`);
            throw new Error(`Already despawned Player/NPC ${victimuid} used to access active entity set`);
        }
    }

    dmg = calcDmg(player.spec, victim.spec);

    victim.spec.hitpoints -= dmg;

    return player.name + ' smacks ' + victim.name + ' in the face';
}

function calcDmg(p_spec, v_spec) {

}

function attackCounter(packet) {

}

attack.handler = attackPlayer;
a.handler = attackPlayer;
whack.handler = attackPlayer;
smack.handler = attackPlayer;

attack.addAfterEffect(attackCounter);
a.addAfterEffect(attackCounter);
whack.addAfterEffect(attackCounter);
smack.addAfterEffect(attackCounter);

module.exports = [
    attack,
    a,
    whack,
    smack
];
