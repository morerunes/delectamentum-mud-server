'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');
let ActiveUIDs = require('../../services/ActiveSessionCache');

let part = new GameCommand('part'),
    quit = new GameCommand('quit'),
    leave = new GameCommand('leave');

function leaveGame(packet) {
    let targetPlayer = GameDB.Players.byId(packet.player_uid);
    if(!targetPlayer) {
        return;
    }

    if(!GameDB.Accounts.byId(packet.account_uid).getPlayer(targetPlayer))
        throw new Error('This is not your character... :\\');

    ActiveUIDs.removeStoredUID(targetPlayer.name);
    GameDB.Players.byId(packet.player_uid).location = '';
    GameDB.Players.remove(packet.player_uid);

    return 'You disappear from the realm';
}

quit.handler  = leaveGame;
part.handler  = leaveGame;
leave.handler = leaveGame;

module.exports = [
    quit,
    part,
    leave
];
