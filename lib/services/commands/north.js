'use strict';

let GameCommand = require('../../services/GameCommand');
let GameDB = require('../../services/GameDBService');

let north     = new GameCommand('north'),
    north_abb = new GameCommand('n');

function moveNorth(packet) {

    let player = GameDB.Players.byId(packet.player_uid);
    if(!player){
        throw new Error('Player ' + packet.player_uid + ' is a myth :(');
    }

    let curPlace = GameDB.Places.byId(player.location);
    let curRealm = GameDB.Realms.byId(curPlace.location.realm);

    if(!curPlace){
        throw new Error('The Place (' + player.location + ') is faked :(');
    }
    if(!curRealm){
        throw new Error('Realm ' + player.location.realm + ' doesn\'t exist');
    }

    let destination = curRealm.at(curPlace.location.X, curPlace.location.Y + 1);

    if(!destination) {
        packet.__playerDelta.location = 'none';
        return 'You walk into a wall';
    } else {
        player.moveTo(destination.uid);
        packet.__playerDelta.location = destination.uid;
        return 'You now find yourself in ' + destination.description;
    }
}

function postMovementTrigger(packet) {

    return 'You hear an unexpected noise after entering.';
}

north.handler = moveNorth;
north_abb.handler = moveNorth;

north.addAfterEffect = postMovementTrigger;
north_abb.addAfterEffect = postMovementTrigger;

module.exports = [
    north,
    north_abb
];
