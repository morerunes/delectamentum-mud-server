'use strict';

/*
 *
 *  Here's a template for writing up a new game command.
 *
 * *****************************************************************************
 * ****For the love of god, don't actually import this command into the game****
 * *****************************************************************************
 */

/* This import is always required,
 * as the GameService\'s command set will be populated
 * by GameCommand objects
 */
let GameCommand = require('../services/GameCommand');

/* Add any other Service or class dependencies here
 *
 * Hint: The Game database service (GameDB) is probably
 *       required for most entity-based interactions
 *
 */
let GameDB = require('../services/GameDBService');

// Initialize the command and any potential abbreviations
let goit = new GameCommand('goit');
let goit_abb = new GameCommand('g');

// Define the handler that will contain the actual game logic
// This should return a string indicating to the player what just happened
let goithandler = function(packet) {
    return 'I\'m a goit!';
};

 // Define any desired post-execution effects
function aftergoit(packet) {
    return 'I\'m still a goit!';
}

// set the command object(s)'s handlers
goit.handler = goithandler;
goit_abb.handler    = goithandler;

// export the command objects
module.exports.commands = [
    goit,
    goit_abb
];
