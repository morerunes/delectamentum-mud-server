'use strict';

const _ = require('lodash');
const SocketIO = require('socket.io');
let io;

const GameDB = require('../services/GameDBService');
const Game = require('../services/GameService');
const log = require('../services/LogService');
const config = require('../services/ConfigService');
const PlayerCreator = require('../services/PlayerCreatorService');
const Account = require('../objects/things/Account');
const Player = require('../objects/things/Player');
const PlayerSpec = require('../objects/things/PlayerSpec');
const Auth = require('../services/AuthService');


// The set of active socket connections
// Includes a quick-ref Player uid to socket id table
let activeConnections = {},
    playerUIDToSocketID = {};


// The set of Socket.io namespaces
let ns = {};

/*
 * Initializes the service by starting up the socket.io 'server'.
 * Give this function the Hapi server listener.
 *
 */
module.exports.start = function(listener) {

    log.debug('Starting up the message service...');
    io = new SocketIO(listener);

    io.on('connect', onConnect);

    ns['all'] = io.sockets;   // also 'io.of('/')'

    // Create all of the non-standard Socket.io namespaces
    let groups = config.option('default-user-groups');
    _.forEach(groups, (group) => {
        log.debug('Adding s.io namespace \'/' + group + '\'');
        ns[group] = io.of('/' + group);
    });
};

module.exports.notifySingleUser = function(packet) {

    let targetConnection = activeConnections[playerUIDToSocketID[packet.player_uid]];
    log.debug('Attempting to respond to ' + JSON.stringify(targetConnection));

    if(targetConnection) {
        ns['all'].connected[targetConnection.socket_id].emit('game:response', packet);
        log.debug('Game response sent to ' + targetConnection.player + ': ' + JSON.stringify(packet));
    }

};

module.exports.notifyGroup = function() {
};

/*
 *  args:
 *      -s_id: a string that refers to the socket id
 *  returns a copy of the socket's MUD connection state
 */
module.exports.inspectConnection = function(s_id) {
    if(activeConnections[s_id]) {
        return JSON.parse(JSON.stringify(activeConnections[s_id]));
    } else {
        return undefined;
    }
};

function onConnect(socket) {

    log.debug('Incoming connection from socket: ' + socket.id);

    activeConnections[socket.id] = {
        socket_id: socket.id,
        account: 'not_logged_in',
        player: 'not_logged_in'
    };

    socket.emit('welcome', 'Hi. Enter \'register\' if this is your first time here, otherwise enter \'login\'.');

    socket.on('create', () => {
        log.debug('(UNIMPLEMENTED) \"Create\" new player instigated by ' + socket.id);

        if(activeConnections[socket.id].account == 'not_logged_in') {
            socket.emit('mud_err', 'Please log in before you do that');
        } else {
            PlayerCreator.startSession(activeConnections[socket.id].account);
            socket.emit('create:player', 'give us the name of your new character');
        }
    });

    socket.on('register', () => {
        log.debug('Event \'register\' from ' + socket.id);
        socket.emit('register:email', 'Please provide a valid email address:');
    });

    socket.on('register:email', (msg) => {

        log.debug('Event \'register:email\' from ' + socket.id);
        let account = GameDB.Accounts.byEmail(msg);

        if(account) {
            socket.emit('register:email:dupe', 'Sorry, that email address is already registered');
        } else {

            account = new Account(msg);
            let accCheck = Auth.validateNewAccount(account);

            if(!accCheck.isValidAccount) {
                log.debug(msg + ' from ' + socket.id + ' is invalid email');
                socket.emit('register:email:invalid', accCheck.err_message);
            } else {
                log.debug(msg + ' from ' + socket.id + ' is valid email');
                GameDB.Accounts.add(account);
                socket.emit('register:email:valid', 'Okay, we\'ll use that then :)');
            }
        }
    });

    socket.on('create:player:name', (msg) => {
        let playerName = msg;

        if(GameDB.Players.search({name:playerName}, true).length) {
            log.debug('Player name already taken');
            socket.emit('create:player:dupe', playerName + ' is already taken');
        } else {
            PlayerCreator.addParameter(activeConnections[socket.id].account, 'name', playerName);
        }
    });

    socket.on('login', () => {

        log.debug('Event \'login\' from ' + socket.id);

        socket.emit('login:email', 'Okay, give me a registered email address');

    });

    socket.on('login:email:attempt', (msg) => {
        log.debug('Event \'login:email:attempt\' from ' + socket.id);

        let acc = GameDB.Accounts.byEmail(msg);

        if(!acc) {
            socket.emit('login:email:fail', 'That email is not registered');
        } else if(_.find(activeConnections, ['account', acc.uid])) {
            socket.emit('login:email:dupe', 'Goit, that email is already logged in.');
        } else {
            activeConnections[socket.id].account = acc.uid;
            socket.emit('login:email:success', 'Okay, now we need a player name');
        }
    });

    socket.on('login:player', (msg) => {
        log.debug('Event \'login:player\' from ' + socket.id);

        let targetPlayerID = GameDB.Accounts.byId(activeConnections[socket.id].account).getPlayer({name: msg});

        if(!targetPlayerID) {
            socket.emit('login:player:fail', 'You don\'t have any players with that name...');
        } else {

            activeConnections[socket.id].player = targetPlayerID;
            Game.attemptAddPlayer({
                account_uid: activeConnections[socket.id].account,
                player_uid: activeConnections[socket.id].player
            });

            playerUIDToSocketID[targetPlayerID] = socket.id;
            socket.emit('login:player:success', 'Okay, you\'re in the game as \"' + msg + '\"');
            socket.emit('client_debug', 'Okay, you\'re in the game as \"' + msg + '\"');
        }

    });

    socket.on('playing', (msg) => {

        if(activeConnections[socket.id].account == 'not_logged_in' ||
            activeConnections[socket.id].player == 'not_logged_in') {
            socket.emit('client_debug', 'nice try, goit');
        } else {

            let packet = {
                command: msg,
                account_uid: activeConnections[socket.id].account,
                player_uid: activeConnections[socket.id].player
            };

            let cmdId = Game.offerTransaction(packet);

            socket.emit('client_debug', cmdId);
        }

    });

    socket.on('playing:quit', () => {
        let target = activeConnections[socket.id];
        target.player = 'not_logged_in';
        delete playerUIDToSocketID[target.player];
    });

    socket.on('disconnect', () => {

        log.debug(socket.id + ' disconnected');

        if(activeConnections[socket.id].player != 'not_logged_in') {

            let kickId = Game.offerTransaction({
                command: 'leave',
                account_uid: activeConnections[socket.id].account,
                player_uid: activeConnections[socket.id].player
            });

            delete playerUIDToSocketID[activeConnections[socket.id].player];
        }

        delete activeConnections[socket.id];

    });

}
