'use strict';

/**
 * GameService.js
 *
 * Keeps track of all of our MUD's maps, players, things, state, etc.
 *
 * To use: const Game = require('../GameService');
 */

const _ = require('lodash');
const Promise = require('bluebird');

const uuid = require('uuid');
const Realms = require('../objects/Realms');
const Places = require('../objects/Places');
const Players = require('../objects/Players');
const Accounts = require('../objects/Accounts');

const log = require('../services/LogService');
const GameDB = require('../services/GameDBService');
const ActiveUIDs = require('../services/ActiveSessionCache');
const MsgSvc = require('../services/MsgService');
const SimpleQueue = require('../services/util/SimpleQueue');

// Amount of time, in milliseconds, the game loop sleeps in between update ticks
const YIELDTIME = 1;

let initCallbacks = [];
let stopCallbacks = [];
let commandBuffer = new SimpleQueue();
let running = false;
let gamepause = false;
let game;

/*
 *  activePlayers is the [literal] set of player UIDs currently roaming around the game
 *  activeNPCs is the [also literal] set of NPC UIDs currently in the game
 *  activeNameTo____UID is for looking up the UID of any of the active players/npcs by name
 */

// The set of commands that the game will use
// { command name : a GameCommand }
let commandSet = {};

module.exports.onlinePlayerCount = function() {
    return ActiveUIDs.onlinePlayerCount();
};

module.exports.onlineNPCCount = function() {
    return ActiveUIDs.onlineNPCCount();
};

module.exports.forceSpawnNPC = function(npc_uid) {
    let npc = GameDB.NPCs.byId(npc_uid);
    if(!npc)
        throw new Error('NPC with uid ' + npc_uid + ' does not exist in DB');
    ActiveUIDs.addPlayer(npc, true);
};

/**
 * reset() will stop the game if it is running, and return the game to its initial state
 */
module.exports.reset = function (a_uid) {
    if(running) this.stop();
    commandBuffer = new SimpleQueue();
    commandSet = {};
    ActiveUIDs.reset();
    running = false;
};

/*
 *  Executes the command given with the invoking player (Assumed to be logged in)
 *  @param packet - Object containing the command string and the invoking player's UID
 *  packet = {
 *      account_uid: String
 *      player_uid: String,
 *      command: String
 *  }
 */
module.exports.offerTransaction = function (packet) {

    if(!running)
        throw new Error('The game is not running...');

    if(!packet.player_uid || !packet.command || !packet.account_uid)
        throw new Error('Packet sent in wrong format');

    if(!ActiveUIDs.isOnline(packet.player_uid))
        throw new Error('Player ' + packet.player_uid + ' is not active');

    log.debug('Game loop received ' + JSON.stringify(packet));

    return createTransaction(packet);

};

/*
 *  Queues up a copy of the supplied info packet
 *  The command in the packet will be processed in the game loop.
 *
 *  returns a unique identifier
 */
function createTransaction(packet) {
    packet._transactionId = uuid();
    packet._playerDelta = {};
    commandBuffer.enqueue(JSON.parse(JSON.stringify(packet)));
    return packet._transactionId;
}

function stopGameByClient(packet) {
    log.debug('Game loop received STOP command');
    running = false;
    return 'Thanks for that, goit';
}

/*
 *  utility function for offerTransaction that parses out a command
 */
function processCommand (packet) {

    //packet.command --> 'attack Goit' -> ['attack', 'Goit'] -> 'Goit'
    let cmdArr = packet.command.split(' '),
        cmd = cmdArr.splice(0,1)[0];
    packet._baseCommand = cmd;
    packet._parameters = (cmdArr.length == 0) ? '' : cmdArr.join(' ');

    let response;

    let command = commandSet[packet._baseCommand];

    if (command) {
        return command.execute(packet);
    } else {
        return 'What the hell is this? :L';
    }
}

function lookAround(packet) {

    let player = GameDB.Players.byId(packet.player_uid);
    let curLocation = GameDB.Places.byId(player.location);
    let thinglist = '';

    _.forEach(curLocation.contents, (thing) => {
        thinglist += '\n\t' + thing.name;
    });

    return 'The place you find yourself in is ' + curLocation.description +
           '\nAfter a look around, you identify the following things :' + thinglist;

}

function showStats(packet) {

    let player = GameDB.Players.byId(packet.player_uid);

    if(!GameDB.Accounts.byId(packet.account_uid).getPlayer(packet.player_uid)
      || GameDB.Accounts.byId(packet.account_uid).account_type != 'admin')
        return 'I\'m not showing you a damn thing ;)';

    return 'Player: ' + player.name +
              '\n' + player.description +
              '\n\tLevel: ' + player.spec.level +
              '\n\tXP: ' + player.spec.xp;
}

/**
 * Adds a function to be executed just before the game is started. Add as many as you want.
 * @param callback - function to be executed before starting the game
 */
module.exports.onInit = function (callback) {
    if (typeof callback === 'function') {
        initCallbacks.push(callback);
    } else {
        throw new Error('Can only add functions as onInit callbacks');
    }
};

/**
 * Same as onInit, but gets called after Game.stop() instead
 * @param callback - function to be executed after game is stopped
 */
module.exports.onStop = function (callback) {
    if (typeof callback === 'function') {
        stopCallbacks.push(callback);
    } else {
        throw new Error('Can only give functions to onStop');
    }
};

/*
 *  adds the player with the associated ID to the list of players subject to getting tortured/killed/whacked/etc.
 *  If successful, the player starts off in the lobby
 *  Fails if the invoking account isn't associated with the chosen player
 *  @param packet - Should be an object containing the invoking Account's UID, and the chosen Player uid
 */
module.exports.attemptAddPlayer = function (packet) {

    let player = GameDB.Players.byId(packet.player_uid);
    if(!player){
        throw new Error('Player ' + packet.player_uid + ' doesn\'t exist');
    }

    if(!GameDB.Accounts.byId(packet.account_uid).getPlayer(player)){
        throw new Error('Player ' + player.name + ' (' + player.uid + ') attempted to join world under the wrong account (' + packet.account_uid + ') :\\');
    }

    if(ActiveUIDs.isOnline(packet.player_uid)){
        throw new Error('You\'re already in the game!');
    }

    player.location = GameDB.Places.Lobby.uid;
    ActiveUIDs.addPlayer(player, false);
};

/**
 * starts the game if it isn't already running, kicks off game loop
 */
module.exports.start = function () {

    if (running) {
        throw new Error('Game is already running!');
    }

    _.forEach(require('../services/commands'), (cmdDef) => {
        _.forEach(cmdDef, (command) => {
            commandSet[command.name] = command;
        });
    });

    log.debug('Loaded commands: ');
    _.forEach(commandSet, (command) => {
        log.debug(`\t${command.name}`);
    });

    init();
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            game = createGameLoop();
            resolve();
        }, 250);

    }).then(() => {
        log.info('game is running!');
    });
};

function init() {
    log.debug('Starting the game loop...');
    _.each(initCallbacks, (callback) => { callback(); });
    initCallbacks = [];
    GameDB.init();
    running = true;
}

function createGameLoop() {
    return new Promise((resolve, reject) => {
        let iter = setInterval(() => {
            let pause = setInterval(() => {
                if(!gamepause) {
                    clearInterval(pause);
                }
            }, 100);

            if(!running) {
                clearInterval(iter);
                resolve();
            }
            procQueuedTransactions();
            tick();
        }, YIELDTIME);
    }).then(function () {
        _.each(stopCallbacks, (callback) => { callback(); });
        stopCallbacks = [];
        log.info('Shutting down now, goodbye!');
    }).catch((e) => {
        running = false;
        log.error('Game stopping due to critical error');
        log.error(e.message);
    });
}

module.exports.viewQueue = function () {
    return JSON.stringify(commandBuffer.qCopy());
};

function procQueuedTransactions() {
    while(commandBuffer.size() > 0) {
        let curCmd = commandBuffer.dequeue();
        log.debug('current command: ' + curCmd.command);
        curCmd.response = processCommand(curCmd);
        log.debug('current response: ' + curCmd.response);
        curCmd._response_type = 'game_response';
        MsgSvc.notifySingleUser(curCmd);
    }
}

module.exports.queuedTransactionCount = function () {

    return commandBuffer.size();

};

function tick() {

}

/**
 * triggers the game loop to stop
 */
module.exports.stop = function (account_uid) {
    running = false;
    log.info('Stopping game...');
};

module.exports.inspectTransaction = function(transactionId) {
    return commandBuffer.inspect(transactionId);
};
