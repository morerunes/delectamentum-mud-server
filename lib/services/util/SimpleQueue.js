'use strict';

function SimpleQueue() {
    let q = [];
    let idToItem = {};
    let inserts = 0;

    this.qCopy = function() {
        return JSON.parse(JSON.stringify(q));
    };

    this.size = function(){
        return q.length - inserts;
    };

    this.empty = function() {
        return q.length == 0;
    };

    this.enqueue = function(n){
        q.push(n);
        idToItem[n.__transactionId] = n;
    };

    this.dequeue = function() {
        if(!q.length) 
            return undefined;

        let item = q[inserts];

        if((++inserts * 2) > q.length) {

            q = q.slice(inserts);
            inserts = 0;
            delete idToItem[item.__transactionId];

        }

        return item;
    };

    this.peek = function() {
        if(q.length > 0)
            return q[inserts];
        else
            return undefined;
    };

    this.inspect = function(transactionId) {
        return idToItem[transactionId];
    };
}

module.exports = SimpleQueue;
