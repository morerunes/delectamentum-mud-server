'use strict';

const Promise = require('bluebird');
const fs = require('fs');

/*
 * IOService.js
 *
 * Contains file IO operations
 *
 */

module.exports.pWrite = function(filename, str) {
    return new Promise(function (resolve, reject) {

        fs.writeFile(filename, str, 'utf-8', function (error) {
            if (error) reject(error.message);
            else resolve(str);
        });

    });
};

module.exports.pRead = function(filename) {
    return new Promise(function (resolve, reject) {

        fs.readFile(filename, 'utf-8', function (error, data) {
            if (error)
                reject(error.message);
            else
                resolve(data);
        });

    });
};
