'use strict';

const Hapi = require('hapi');
const log = require('./LogService');
const server = new Hapi.Server();
const MsgSvc = require('./MsgService');
const Game = require('./GameService');

let routes = [];

server.connection({
    port: 8000
});

/*
 *  onStart: Callback used once the server is running
 */
module.exports.start = function () {
    log.info('Starting MUD Server on port 8000');
    server.start(() => {
        MsgSvc.start(server.listener);
        Game.start();
    });
};

module.exports.addRoute = function(route) {
    server.route(route);
    routes.push(route);
};

module.exports.routes = routes;
