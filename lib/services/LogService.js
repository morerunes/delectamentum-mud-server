'use strict';

/**
 * LogService.js
 *
 * Small wrapper for [log.js](https://www.npmjs.com/package/log), use this instead of console.log in production
 *
 * To use elsewhere: `const log = require('../services/LogService');`
 * Log an error: `log.error('My spoon is too big');`
 * Log a debug message: `log.debug('Doing seemingly harmless operation');`
 * Log some info: `log.info('User _eldritchScholar_ connected to the dungeon');`
 */

const config = require('./ConfigService');
const Log = require('log');
let log = new Log(config.option('logLevel') || 'INFO');

module.exports.info = function (message) {
    log.info(' ' + message);
};

module.exports.debug = function (message) {
    log.debug(message);
};

module.exports.error = function (message) {
    log.error(message);
};

module.exports.setLevel = function (logLevel) {
    log = new Log(logLevel || config.option('logLevel') || 'INFO');
};
