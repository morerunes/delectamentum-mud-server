'use strict';

/**
 * ConfigService.js
 *
 * Keeps all of the configuration options in sync with the entire project. Loads the config.json from the project root
 * to populate default stuff, and then lets you set and retrieve option values at runtime. Stuff like logLevel (mostly
 * for debugging), the name of the MUD, player whitelists/blacklists, banned IPs, etc are all stored here, and are
 * loaded from config.json. Avoid storing Game specific details in here, put those on GameService instead.
 *
 * To use elsewhere:
 * require the config service: `const config = require('../services/ConfigService');`
 * set an option: `config.set('bannedPlayers', [ 'sicilide', 'eldritchScholar' ]);`
 * get an option: `let isAlexBanned = _.includes(config.option('bannedPlayers'), 'eldritchScholar');`
 */

const _ = require('lodash');

const log = require('./LogService');
const options = {};

function init (logLevel, preinit) {
    let config = require('../../config.json');

    _.each(config, (value, key) => {
        options[key] = value;
    });

    if (!preinit) {
        options.logLevel = logLevel || config.logLevel;
        log.setLevel(options.logLevel);
    }
}

init(null, true);

module.exports.init = init;

module.exports.set = function (option, value) {
    options[option] = value;
};

module.exports.option = function (name) {
    return options[name];
};
