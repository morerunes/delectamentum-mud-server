'use strict';

const Player = require('../objects/things/Player');
const PlayerSpec = require('../objects/things/PlayerSpec');
const log = require('../services/LogService');

/*
 * model for an active creator session:
 * {
 *      acc_id: the invoking Account UID
        params: an object containing the options the user has provided
 * }
 */

let activeCreateSessions = {};

function checkAccountSession(account_uid) {

}

function generatePlayerSpec() {

}

function generatePlayer() {

}

module.exports.startSession = function (account_uid) {
    if(!account_uid) {
        return {
            result: 'no account id'
        };
    } else if(activeCreateSessions[account_uid]) {
        return {
            result: 'account already trying to make a player'
        };
    } else {
        activeCreateSessions[account_uid] = {
            acc_id: account_uid,
            player_params: {},
            state: 'unfinished'
        };

        log.debug('creator session started for' + account_uid);
    }
};

module.exports.addParameter = function (account_uid, param, value) {

    if(!account_uid || !param || !value) {
        log.error('I will not do anything with missing data');
    }

    let session = activeCreateSessions[account_uid];

    if(!session) {
        log.error('Tried to add player parameter for an account without an active session');
    } else {
        session.player_params[param] = value;
    }

};

module.exports.makePlayer = function () {

};

module.exports.endSession = function (account_uid) {
    if(!account_uid) {
        return {
            result: 'no account id'
        };
    } else if(activeCreateSessions[account_uid]) {
        delete activeCreateSessions[account_uid];
        return {
            result: 'session ended'
        };
    }
};
