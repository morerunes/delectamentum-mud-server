'use strict';

const Joi = require('joi');
const uuid = require('uuid');

const GameDB = require('../services/GameDBService');
const Account = require('../objects/things/Account');

const accountSchema = Joi.object().keys({
    email: Joi.string().email().required().error(new Error('Invalid Email')),
    account_type: Joi.string(),
    uid: Joi.string().required(),
    player_uids: Joi.object().max(0).error(new Error('Can\'t make new Account with a non-empty player uid list'))
});

/*
 * AuthService.js
 *
 * Handles all operations related to authentication
 *
 */


/*  List of valid session tokens/tickets/macguffins
 *  
 *  Tickets are created as follows:
 *  {
 *      uid:         string,    [the ticket's identifier]
 *      account_uid: string,    [the account that invoked the ticket]
 *      lastUse:     number     [timestamp the ticket was created/last used]
 *  }
 *
 */
let tickets = {};

/*
 *  Auth.validateNewAccount(acc: Account)
 *  
 *  Checks the provided Account object alongside the provided schema 'accountSchema'
 *  for validity.
 * 
 *  Returns an object containing true/false depending on the validation result, 
 *  and a potential error message
 */
module.exports.validateNewAccount = function (acc){

    if(!(acc instanceof Account))
        throw new Error('This isn\'t an Account');

    let valresult = Joi.validate(acc, accountSchema),
        iVA = true,
        err_message = null;
    
    if(valresult.error){
        err_message = valresult.error.message;
        iVA = false;
    }

    return {
        isValidAccount: iVA,
        error_msg: err_message
    };
  
};

/*
 *  Auth.validateLogin(email:string)
 *
 *  Checks the provided credentials alongside their respective Account (provided it exists)
 *
 *  Returns the target Account associated with the credentials,
 *  or throws an error if the login attempt is bad
 *
 */
module.exports.validateLogin = function(email){

    if(!email)
        throw new Error('Email address required for login');

    let target = GameDB.Accounts.byEmail(email);

    if(!target)
        throw new Error('Invalid Login');
    else
        return target;
};

/*
 *  Auth.generateTicket(Account)
 *
 *  Creates an API access ticket for the provided Account
 *  
 *  Returns the UID of the ticket 
 *
 */
module.exports.generateTicket = function(account) {

    if(!account || !(account instanceof Account))
        throw new Error('This isn\'t an Account');

    let target_account = GameDB.Accounts.byId(account.uid);

    if(!target_account)
        throw new Error('Cannot create ticket for an untracked Account');

    let ticket = {
        uid: uuid(),
        account_uid: account.uid,
        lastUse: Date.now()
    };

    tickets[ticket.uid] = ticket;

    return ticket.uid;
};

/*
 *  Auth.retrieveTicket(ticket_uid: string)
 *
 *  Retrieves and returns the ticket with the given UID,
 *  or throws an Error if there's no such ticket
 *
 */

module.exports.retrieveTicket = function (ticket_uid) {

    let ticket = tickets[ticket_uid];

    if(!ticket)
        throw new Error('Ticket with UID ' + ticket_uid + ' does not exist');

    ticket.lastUse = Date.now();

    return ticket;

};
