'use strict';

/*
 * This is a small utility for quick player name to UID references
 * in order to avoid calling GameDB.search()
 */

let GameDB = require('../services/GameDBService');
let Player = require('../objects/things/Player');

/*
 *  activePlayers is the [literal] set of player UIDs currently roaming around the game
 *  activeNPCs is the [also literal] set of NPC UIDs currently in the game
 *  activeNameTo____UID is for looking up the UID of any of the active players/npcs by name
 */
let activePlayers = new Set(),
    activeNPCs = new Set(),
    activeNameToPlayerUID = {},
    activeNameToNPCUID = {},
    activeAccountToPlayerUID = {};

module.exports.onlinePlayerCount = function() {
    return activePlayers.size;
};

module.exports.onlineNPCCount = function() {
    return activeNPCs.size;
};

module.exports.isOnline = function(uid) {
    return activePlayers.has(uid) || activeNPCs.has(uid);
};

module.exports.reset = function() {
    activePlayers = new Set();
    activeNPCs = new Set();
    activeNameToPlayerUID = {};
    activeNameToNPCUID = {};
};

module.exports.removeStoredUID = function (name) {
    if(activeNameToPlayerUID[name]){
        activePlayers.delete(activeNameToPlayerUID[name]);
        delete activeNameToPlayerUID[name];
    } else if(activeNameToNPCUID[name]) {
        activeNPCs.delete(activeNameToNPCUID[name]);
        delete activeNameToNPCUID[name];
    }
};

module.exports.getStoredUID = function(name) {
    if(activeNameToPlayerUID[name]){
        return activeNameToPlayerUID[name];
    } else if(activeNameToNPCUID[name]) {
        return activeNameToNPCUID[name];
    } else {
        return undefined;
    }
};

module.exports.addPlayer = function(player, isNPC) {
    if(isNPC) {
        activeNPCs.add(player.uid);
        activeNameToNPCUID[player.name] = player.uid;
    } else {
        activePlayers.add(player.uid);
        activeNameToPlayerUID[player.name] = player.uid;
    }
};
