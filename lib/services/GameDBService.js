'use strict';

/*
 * GameDBService.js
 *
 * This tracks the state of all active and spawned entities
 *
 */
const _ = require('lodash');
const Promise = require('bluebird');

const Realms = require('../objects/Realms');
const Places = require('../objects/Places');
const Players = require('../objects/Players');
const Accounts = require('../objects/Accounts');
const log = require('../services/LogService');
const SaveState = require('../services/SaveStateService');


/*
 *  Starts the DB off with the Lobby.
 *  Feel free to add any other default rooms/realms here.
 */
module.exports.init = function () {
    this.Realms.Ether.addPlace(this.Places.Lobby, 0, 0);
};

/*
 *  Resets the DB's state to the initial state with only the Ether and Lobby
 */
module.exports.reset = function () {

    this.Places = new Places();
    this.Players = new Players();
    this.Realms = new Realms();
    this.Accounts = new Accounts();
    this.NPCs = new Players();

    this.init();
};

module.exports.movePlace = function(placeId, realmId, x, y) {

    let place = this.Places.byId(placeId);
    let realm = this.Realms.byId(realmId);

    if (!place) {
        throw new Error(placeId + ' does not match any known Places');
    }

    if (placeId === this.Places.Lobby.uid) {
        throw new Error('Cannot move the Lobby');
    }

    if (!realm) {
        throw new Error(realmId + ' does not match any known Cartograph');
    }

    if (realm.at(x, y)) {
        throw new Error('Space [' + x + ', ' + y + '] is already occupied by ' + realm.at(x, y).name + ' in ' + realm.name);
    }

    realm.addPlace(place, x, y);
    place.location = {
        realm: realm.uid,
        X: x,
        Y: y
    };
};

/*
 *  Restores the database from files previously created by the SaveState service.
 *  Any failure while restoring results in the DB being reset to the initial state.
 */
module.exports.restore = function () {

    let iP = SaveState.attemptReadPlayers();

    return iP.then((playerList) => {
        this.Players.restoreFromList(playerList);
        return SaveState.attemptReadPlaces();
    }).then((placeList) => {
        this.Places.restoreFromList(placeList);
        return SaveState.attemptReadRealms();
    }).then((realmList) => {
        this.Realms.restoreFromList(realmList);
    }).catch((e) => {
        this.reset();
        log.error(e.message);
    });

};

module.exports.save = function () {
    let iP = SaveState.attemptSavePlayers();

    return iP.then(() => {
        return SaveState.attemptSavePlaces();
    }).then(() => {
        return SaveState.attemptSaveRealms();
    });
};

module.exports.Places = new Places();
module.exports.Players = new Players();
module.exports.Realms = new Realms();
module.exports.Accounts = new Accounts();
module.exports.NPCs = new Players();
