'use strict';

const log = require('../services/LogService');
const _ = require('lodash');
const Promise = require('bluebird');

class GameCommand {
    constructor (_name, _handler) {
        if(!_name) {
            throw new Error('A command name is required');
        } else if(!(/^[a-zA-Z]+$/.test(_name))) {
            throw new Error('A command name can only take letters');
        } else {
            this.name = _name;
        }

        if(_handler) {
            this.handler = _handler;
        }

        this.afterEffects = [];
    }

    set handler(newhandler) {
        if(typeof newhandler !== 'function') {
            throw new Error('The command handler must be a function');
        } else if (newhandler.length != 1) {
            throw new Error('The command handler must take only one argument');
        } else {
            this._handler = newhandler;
        }
    }

    get handler() {
        return this._handler;
    }

    /*
     * command.execute(packet)
     * Returns a list of responses (strings)
     * generated from the command callbacks
     * ... main handler executes first, then the after effects
     */
    execute(packet) {
        let responses = [];
        let handlers = [];

        if(!this.handler) {
            return [];
        }

        handlers.push(this.handler);

        _.forEach(this.afterEffects, afterEffect => {
            handlers.push(afterEffect);
        });

        try {
            _.forEach(handlers, handler => {
                responses.push(handler(packet));
            });
        } catch (err) {
            log.error(err.message);
            throw err;
        }

        return responses;
    }

    addAfterEffect(ae) {
        if(typeof ae !== 'function') {
            throw new Error('A command after effect must be a function');
        } else if(ae.length != 1) {
            throw new Error('A command after effect can only take one argument');
        } else {
            this.afterEffects.push(ae);
        }
    }
}

module.exports = GameCommand;
