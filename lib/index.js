'use strict';

/* index.js
 *
 * This is the entry point for our MUD server. Here we start up all services, set preferences, build stuff, etc.
 * Try to keep this file small, as it's only real purpose is delegating other stuff.
 */

/*
 Copyright 2016 Aaron Echols and Alex Abbinante

 This file is part of Delectamentum-Mud-Server.

 Delectamentum-Mud-Server is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Delectamentum-Mud-Server is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Delectamentum-Mud-Server.  If not, see <http://www.gnu.org/licenses/>.
 */

const _ = require('lodash');
const ServerService = require('./services/ServerService');
const routes = require('./server/routes/');
const config = require('./services/ConfigService');

config.init();
ServerService.start();
