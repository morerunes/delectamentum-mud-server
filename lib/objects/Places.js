'use strict';

/**
 * Places.js
 *
 * Keeps track of a set of Cartograph objects
 *
 * To use: const Places = require('./Places');
 *          places = new Places();
 * to add a place: places.add();
 *
 * @constructor
 */

const _ = require('lodash');

//const GameDB = require('../services/GameDBService');
const Place = require('./things/Place');
const config = require('../services/ConfigService');

function Places () {
    let places = {};

    this.totalCount = 1;

    this.byName = function (name) {
        return _.filter(places, ['name', name]);
    };

    this.add = function (place) {
        if (!(place instanceof Place)) {
            throw new Error('Cannot add object of type ' + typeof place + ' to GameDB.Places');
        }

        if (!_.isNil(places[place.uid])) {
            throw new Error('Place with uid ' + place.uid + ' already exists');
        }

        places[place.uid] = place;
        this.totalCount++;
    };

    this.byId = function (uid) {
        return places[uid];
    };

    this.remove = function(victim_uid){
        let victim = places[victim_uid];

        if(!victim)
            throw new Error('Place with uid ' + victim_uid + ' never existed');

        if(victim === this.Lobby)
            throw new Error('Cannot remove the lobby');

        delete places[victim_uid];

        this.totalCount--;
    };

    this.restoreFromList = function(list){

        if(list.length == 0)
            throw new Error('Cannot restore Server\'s places from an empty list');

        let newLobby;

        for(let i = 0; i < list.length; i++)
            if(list[i].uid == 'LOBBY')
                newLobby = list[i];

        if(!newLobby)
            throw new Error('Cannot restore Server\'s places without a lobby');        

        places = {};
        this.totalCount = 0;

        this.Lobby = newLobby;

        for(let j = 0; j < list.length; j++)
            this.add(list[j]);

    };

    this.all = function () {
        return _.map(places, (place) => { return place; });
    };

    // build the Lobby
    this.Lobby = new Place('Lobby', 'The Lobby', 1000);
    this.Lobby.uid = config.option('lobby-id');
    places[this.Lobby.uid] = this.Lobby;
}

module.exports = Places;
