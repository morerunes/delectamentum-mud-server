'use strict';

/**
 * Player.js
 *
 * A Thing that is also a Player who can connect to and play the MUD
 *
 * To use elsewhere: const Player = require('../Player');
 * To make a Player: let badgerhat = new Player('badgerhat');
 * To make a Player with a cool description: let silicide = new Player('silicide', 'A cool folk');
 */

const GameDB = require('../../services/GameService');
const Thing = require('./Thing');

class Player extends Thing {

    constructor (name, _description, _spec) {

        let description = _description || 'A Player named ' + name;

        super(name, description, 'Player');

        this.contents['currency_amt'] = 0;

        this.location = '';

        this.spec = _spec;

    }

    moveTo (uid) {
        this.location = uid;
    }

}

module.exports = Player;
