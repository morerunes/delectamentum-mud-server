'use strict';

/*
 *
 * Account.js
 *
 * An entity that is identified by email address. Contains the UIDs of players created under the specific email
 *
 */

const _ = require('lodash');
const uuid = require('uuid');
const log = require('../../../lib/services/LogService');

class Account {

    constructor (_email, _account_type) {

        this.uid = uuid();

        if(!_email)
        {
            log.error('Tried to create account without an email address');
            throw new Error('Cannot create Account without an email address');
        }

        this.email = _email;

        // player_uids[player name] = player.uid
        this.player_uids = {};

        this.account_type = _account_type || 'user';
    }

    addPlayer (player) {
        
        if(!player || !player.uid || player.uid == '')
        {
            log.error('Tried to give an account an empty player UID');
            throw new Error('Cannot give an Account an empty player UID');
        }

        if(this.getPlayer(player)){
            log.error('Tried to give an account a player UID it already has');
            throw new Error('Player UID ' + player.uid + ' already associated with this account');  
        }

        this.player_uids[player.name] = player.uid;

    }

    getPlayer(player) {
        return this.player_uids[player.name];
    }
    
}

module.exports = Account;
