'use strict';

/**
 * Cartograph.js
 *
 * Really a map, but I wanted to distinguish it from the data structure "map". A 2D grid of rooms  and other stuff.
 * This a playable area, like a dungeon or a city. Stores all the places in it by X-Y coords where each slot is a room.
 *
 * To use elsewhere: `const Cartograph = require('../objects/Cartograph');`
 * Make a new map: `let sweetAssDungeon = new Cartograph('name', 'description');`
 * Add a room: `sweetAssDungeon.draw('Empty Room', 'An empty stone room', 4, 8);`
 * What is at 0, 0?: `let maybeARoom = sweetAssDungeon.at(0, 0);`
 */

const log = require('../../services/LogService');
const Thing = require('./Thing');
const Place = require('./Place');

class Cartograph extends Thing {

    constructor (name, description) {
        super(name, description, 'Cartograph');

        this.places = {};
        log.debug('creating new Cartograph ' + name);
    }

    addPlace (place, locX, locY) {
        if (!(place instanceof Place)) {
            throw new Error('Cannot add thing of type ' + typeof place + ' to Cartograph');
        }

        log.debug('Placing ' + place.name + ' at [' + locX + ', ' + locY + ']');

        if (!this.places[locX]) {
            this.places[locX] = {};
        }

        place.location.realm = this.uid;
        place.location.X = locX;
        place.location.Y = locY;
        this.places[locX][locY] = place;
    }
    
    at (locX, locY) {
        if (this.places[locX]) {
            return this.places[locX][locY];
        } else {
            return undefined;
        }
    }

}

module.exports = Cartograph;
