'use strict';

/**
  * PlayerSpec.js
  *
  * Utility class used to define the attributes, skills, and statistics of a particular Player
  *
  */

class PlayerSpec {
  
    constructor () {

        this.attribs = {
            strength: 1,
            agility:  1,
            accuracy: 1,
            intel:    1,
            charisma: 1
        };

        this.hitpoints = 100;

        this.accessories = {};

        this.skills = {};

        this.xp = 0;

        this.level = 1;

        this.inventory = {};
    }

}

module.exports = PlayerSpec;
