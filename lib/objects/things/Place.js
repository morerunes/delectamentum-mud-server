'use strict';

const Thing = require('./Thing');
const config = require('../../services/ConfigService');

/**
 * Place object, essentially a room or town area
 *
 * @param name - name of the room
 * @param description - describe the room
 * @param size - multiplier where 1.0 is living-room sized
 * @constructor
 */
class Place extends Thing {

    constructor(name, description, size) {
        super(name, description, 'Place');

        if (!size) {
            throw new Error('Cannot create a Place with no size');
        }

        this.contents['currency_amt'] = 0;

        this.location = {
            realm: config.option('ether-id'),
            X: 0,
            Y: 0
        };
    }

}

module.exports = Place;
