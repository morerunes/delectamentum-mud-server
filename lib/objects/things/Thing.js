'use strict';

/**
 * Thing.js
 *
 * Most things in the dungeon are a 'Thing'. A Thing has a type, a name, a description, etc. A Thing can also contain
 * other things in some situations (for example, a chest or a book [_"You open the book and find a long lost drawing
 * of a sword"]).
 *
 * To use this elsewhere: `const Thing = require('../objects/Thing');`
 * Make a new thing: `let coolThing = new Thing('name', 'description', 'type');`
 * Turn it into a container: `coolThing.accept('Type of thing it can hold');`
 */
const log = require('../../services/LogService');
const _ = require('lodash');
const uuid = require('uuid');

class Thing {

    constructor (name, description, type) {
        this.name = name;
        this.description = description;
        this.type = type || 'Thing';

        if (!this.name) {
            log.error('Tried to create a Thing with no name');
            throw new Error('Cannot instantiate new Thing(' + (type || 'Thing') + ') with no name');
        }

        this.contents = {};
        this.uid = uuid();
        this.acceptedThingTypes = [];

        log.debug('Created new Thing ' + JSON.stringify(this));
    }

    give (thing) {
        if (!(thing instanceof Thing)) {
            throw new Error('Can only give Things to Things, but tried to give a ' + typeof thing);
        }

        if (!this.accepts(thing)) {
            throw new Error('Cannot add Thing of type ' + thing.type + ' to ' + this.name);
        }

        this.contents[thing.name] = thing;
    }

    take(thing_name) {

        if(!this.contents[thing_name])
            return undefined;

        let contraband = this.contents[thing_name];
        
        delete this.contents[thing_name];
        return contraband;
    }

    accepts (thing) {
        return _.includes(this.acceptedThingTypes, thing.type);
    }

    accept (thing) {
        if (!(thing instanceof Thing)) {
            throw new Error('A Thing can only accept other Things');
        }

        if (!_.includes(this.acceptedThingTypes, thing.type)) {
            log.debug('Adding ' + thing.type + ' to the accepted types of ' + this.name);
            this.acceptedThingTypes.push(thing.type);
        }
    }

    publicView () {
        return {
            name: this.name,
            description: this.description
        };
    }

}

module.exports = Thing;
