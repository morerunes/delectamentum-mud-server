'use strict';

/**
 * Players.js
 *
 * Keeps track of a set of Player objects
 *
 * To use: const Players = require('./Players');
 *          players = new Players();
 * to add a player: players.add();
 *
 * @constructor
 */

const _ = require('lodash');

const Player = require('./things/Player');

function Players () {
    this.totalCount = 0;

    let players = {};

    this.add = function (player) {
        if (!(player instanceof Player)) {
            throw new Error('Cannot add object of type ' + typeof player + ' to GameDB.Players');
        }

        if (players[player.uid]) {
            throw new Error('Player id ' + player.uid + ' already exists.');
        }

        if (_.find(players, ['name', player.name])) {
            throw new Error('Player name ' + player.name + ' is already taken');
        }

        this.totalCount += 1;
        players[player.uid] = player;
    };

    this.all = function () {
        return _.flatMap(players, (player) => { return player; });
    };

    this.byId = function (uid) {
        return players[uid];
    };

    this.remove = function(victim_uid){
        let victim = players[victim_uid];

        if(!victim)
            throw new Error('Player with uid ' + victim_uid + ' never existed');

        delete players[victim_uid];
    };

    this.restoreFromList = function(list){

        if(list.length == 0)
            throw new Error('Cannot restore Server\'s players from an empty list');    

        this.totalCount = 0;
        players = {};

        for(let j = 0; j < list.length; j++)
            this.add(list[j]);

    };

    this.search = function (terms, search) {
        if (_.isEmpty(terms)) {

            throw new Error('Cannot perform search on empty input');

        } else if (_.isEmpty(players)) {

            return [];

        } else {

            let result = _.values(players);
            let compare = (val1, val2) => {
                if (search) {
                    return _.includes(val1.toLowerCase(), val2.toLowerCase());
                } else {
                    return val1 === val2;
                }
            };

            _.each(terms, (value, key) => {
                result = _.filter(result, (player) => {
                    return compare(player[key], value);
                });
            });

            return result;

        }
    };
}

module.exports = Players;
