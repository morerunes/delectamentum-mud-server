'use strict';

const _ = require('lodash');

const Cartograph = require('./things/Cartograph');
const config = require('../services/ConfigService');

function Realms () {
    let realms = {};

    this.totalCount = 0;

    this.add = function (cartograph) {
        if (!(cartograph instanceof Cartograph)) {
            throw new Error('Cannot add thing of type ' + typeof cartograph + ' to Realms');
        }

        if (realms[cartograph.uid] instanceof Cartograph) {
            throw new Error('Realm ' + cartograph.name + ' already exists');
        }

        realms[cartograph.uid] = cartograph;
        this.totalCount++;
    };

    this.byId = function (uuid) {
        return realms[uuid];
    };

    this.byName = function (name) {
        return _.filter(realms, ['name', name]);
    };

    this.remove = function(victim_uid){
        let victim = realms[victim_uid];

        if(!victim)
            throw new Error('Realm with uid ' + victim_uid + ' never existed');

        if(victim === this.Ether)
            throw new Error('Cannot remove the Ether');

        delete realms[victim_uid];

        this.totalCount--;
    };

    this.restoreFromList = function(list){

        let newEther;
        
        if(list.length == 0)
            throw new Error('Cannot restore Server\'s realms from an empty list');

        for(let i = 0; i < list.length; i++)
            if(list[i].uid == 'ETHER')
                newEther = list[i];
        
        if(!newEther)
            throw new Error('Cannot restore Server\'s realms without an Ether');

        realms = {};
        this.totalCount = 0;

        this.Ether = newEther;

        for(let j = 0; j < list.length; j++)
            this.add(list[j]);
    };

    this.all = function () {
        return _.map(realms, (realm) => { return realm; });
    };

    this.Ether = new Cartograph('Ether', 'The Ether');
    this.Ether.uid = config.option('ether-id');
    this.add(this.Ether);
}

module.exports = Realms;
