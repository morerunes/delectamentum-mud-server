'use strict';

/*
 *
 * Accounts.js
 *
 * Keeps track of all Account objects
 *
 */

const _ = require('lodash');

const Account = require('./things/Account');

function Accounts() {
    let accounts = {};

    this.totalCount = 0;

    this.add = function (account) {
        if (!(account instanceof Account)) {
            throw new Error('Cannot add object of type ' + typeof account + ' to Game.Accounts');
        }

        if (!_.isNil(accounts[account.uid])) {
            throw new Error('Account with UID ' + account.uid + ' already exists');
        }

        if (_.find(accounts, ['email', account.email])) {
            throw new Error('Account with email ' + account.email + ' already exists');
        }

        this.totalCount++;
        accounts[account.uid] = account;
    };

    this.byEmail = function (email) {
        return _.find(accounts, ['email', email]);
    };

    this.byId = function (uid) {
        return accounts[uid];
    };

    this.remove = function (victim_uid) {
        let victim = accounts[victim_uid];

        if (!victim)
            throw new Error('Account with uid ' + victim_uid + ' never existed');

        if (victim.account_type != 'user')
            throw new Error('Cannot remove non-standard user accounts');

        delete accounts[victim_uid];

        this.totalCount--;
    };

    this.restoreFromList = function (list) {

        if (list.length == 0)
            throw new Error('Cannot restore Server\'s accounts from an empty list');

        accounts = {};
        this.totalCount = 0;

        for (let j = 0; j < list.length; j++)
            this.add(list[j]);

    };

    this.all = function () {
        return _.map(accounts, (account) => {
            return account;
        });
    };

    this.search = function (terms, search) {
        if (_.isEmpty(terms)) {

            throw new Error('Cannot perform search on empty input');

        } else if (_.isEmpty(accounts)) {

            return [];

        } else {

            let result = _.values(accounts);
            let compare = (val1, val2) => {
                if (search) {
                    return _.includes(val1.toLowerCase(), val2.toLowerCase());
                } else {
                    return val1 === val2;
                }
            };

            _.each(terms, (value, key) => {
                result = _.filter(result, (account) => {
                    return compare(account[key], value);
                });
            });

            return result;

        }
    };
}

module.exports = Accounts;
