'use strict';

const _ = require('lodash');
const Auth = require('../../services/AuthService');
const Account = require('../../objects/things/Account');
const log = require('../../services/LogService');
const Route = require('../Route');

let login = new Route('/login');

login.POST = function (request, reply) {

    log.debug('processing POST at /accounts');

    let message = '';
    let code = 200;
    let type = 'application/json';

    let creds = request.payload;

    if (request.headers['content-type'] !== 'application/json') {

        message = 'Can only accept JSON';
        code = 415;
        type = 'text/plain';

    } else if (_.isEmpty(creds)) {

        message = 'No login credentials provided';
        code = 401;
        type = 'text/plain';

    } else try {

        let target_account = Auth.validateLogin(creds.email);

        let ticket = Auth.generateTicket(target_account);

        let response = {
            account_uid: target_account.uid,
            ticket_uid: ticket
        };

        message = JSON.stringify(response);

    } catch (err) {

        message = err.message;
        code = 401;
        type = 'text/plain';

    }

    reply(message).type(type).code(code);

};
