'use strict';

const _ = require('lodash');
const Auth = require('../../services/AuthService');
const GameDB = require('../../services/GameDBService');
const log = require('../../services/LogService');
const Player = require('../../objects/things/Player');
const PlayerSpec = require('../../objects/things/PlayerSpec');
const Route = require('../Route');

let players = new Route('/players');
let player = new Route('/players/{id}');

players.GET = function (request, reply) {
    log.debug('serving /players via GET');

    let respString;
    let code = 200;
    let type = 'application/json';

    let query = request.query;
    let ticket;
    let missingHeads = false;
    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            missingHeads = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if (GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin') {

            throw new Error('Normal users are not allowed to use this resource');

        } else if (_.isEmpty(query)) {

            let resp = {
                totalPlayers: GameDB.Players.totalCount,
                players: null
            };

            resp.players = GameDB.Players.all();
            respString = JSON.stringify(resp);

        } else {

            let resp = {
                totalPlayers: GameDB.Players.totalCount,
                players: null
            };

            resp.players = GameDB.Players.search(_.omit(query, 'search'), !!query.search);
            respString = JSON.stringify(resp);
        }

    } catch(err) {

        if(!missingHeads && !ticket)
            respString = 'Please log in to use this resource';
        else
            respString = err.message;
        type = 'text/plain';
        code = 401;
    }

    log.debug('serving ' + respString);

    reply(respString).type(type).code(code);
};

players.POST = function (request, reply) {
    log.debug('processing POST at /players');

    let message = '';
    let code = 200;
    let type = 'application/json';

    let result = request.payload;
    let ticket,
        notAdmin = false;

    if(!request.headers['auth-a'] || !request.headers['auth-t']){

        message = 'Authentication headers required';
        code = 401;
        type = 'text/plain';

    } else if (request.headers['content-type'] !== 'application/json') {

        message = 'Can only accept JSON';
        code = 415;
        type = 'text/plain';

    } else if (_.isEmpty(result)) {

        message = 'Cannot create a player from input ' + result;
        code = 415;
        type = 'text/plain';

    } else try {

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin') {
            notAdmin = true;
            throw new Error('Normal users are not allowed to use this resource');
        }

        let player = new Player(result.name, result.description, new PlayerSpec());
        GameDB.Players.add(player);

        message = JSON.stringify(player);

    } catch (err) {

        if(!ticket){
            message = 'Please log in to use this resource';
            code = 401;
        }
        else {
            message = err.message;

            if(notAdmin)
                code = 401;
            else
                code = 400;
        }

        type = 'text/plain';

    }

    reply(message).type(type).code(code);
};

player.GET = function (request, reply) {

    let code = -1;
    let type = '';
    let message = '';
    let ticket, missingHeads;

    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            missingHeads = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin')
            throw new Error('Normal users are not allowed to use this resource');

        let name = decodeURIComponent(request.params.id);
        log.debug('looking for player ' + name);

        let player = GameDB.Players.byId(name);

        if (player) {
            message = JSON.stringify(player);
            type = 'application/json';
            code = 200;
        } else {
            message = 'No Player with uid ' + name;
            type = 'text/plain';
            code = 404;
        }

    } catch(err) {
        if(!ticket && !missingHeads)
            message = 'Please log in to use this resource';
        else
            message = err.message;
        code = 401;
        type = 'text/plain';

    }

    reply(message).type(type).code(code);
};
