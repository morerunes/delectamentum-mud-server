'use strict';

module.exports.accounts = require('./accounts');
module.exports.login = require('./login');
module.exports.places = require('./places');
module.exports.players = require('./players');
module.exports.realms = require('./realms');
module.exports.register = require('./register');
