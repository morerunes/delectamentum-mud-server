'use strict';

const _ = require('lodash');
const Auth = require('../../services/AuthService');
const Account = require('../../objects/things/Account');
const GameDB = require('../../services/GameDBService');
const log = require('../../services/LogService');
const Route = require('../Route');

let register = new Route('/register');

register.POST = function (request, reply) {
    log.debug('Processing registration POST');

    let message = '',
        code = 200,
        type = 'application/json';

    let creds = request.payload;

    if (request.headers['content-type'] !== 'application/json') {

        message = 'Can only accept JSON';
        code = 415;
        type = 'text/plain';

    } else if (_.isEmpty(creds)) {

        message = 'No registration credentials provided';
        code = 401;
        type = 'text/plain';

    } else try {

        let account = new Account(creds.email, creds.account_type);
        let v_result = Auth.validateNewAccount(account);

        if(!(v_result.isValidAccount))
            throw new Error(v_result.error_msg);

        GameDB.Accounts.add(account);

        let response = {
            account_uid: account.uid
        };

        message = JSON.stringify(response);

    } catch(err) {
        message = err.message;
        code = 401;
        type = 'text/plain';
    }

    reply(message).code(code).type(type);
};
