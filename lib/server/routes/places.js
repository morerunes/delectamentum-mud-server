'use strict';

const Auth = require('../../services/AuthService');
const GameDB = require('../../services/GameDBService');
const log = require('../../services/LogService');
const Place = require('../../objects/things/Place');
const Route = require('../Route');

let places = new Route('/places');
let place = new Route('/places/{id}');

places.GET = function (request, reply) {

    log.debug('serving /places via GET');

    let code = 200;
    let type = 'application/json';
    let message = '';

    let ticket,
        query = request.query,
        noHeaders = false,
        places;

    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            noHeaders = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin'){

            throw new Error('Normal users are not allowed to use this resource');

        } else {

            let response = {
                numPlaces: GameDB.Places.totalCount,
                places: null
            };

            response.places = GameDB.Places.all();
            message = JSON.stringify(response);
        } /*else {

         let response = {
         totalPlaces: GameDB.Places.totalCount,
         places: null
         };

         response.places = GameDB.Places.search(_.omit(query, 'search'), !!query.search);
         message = JSON.stringify(response);
         }*/


    } catch(err) {
        if(!ticket && !noHeaders)
            message = 'Please log in to use this resource';
        else
            message = err.message;
        type = 'text/plain';
        code = 401;
    }

    reply(message).type(type).code(code);
};

places.POST = function (request, reply) {

    let code = 200;
    let message = '';
    let type = 'application/json';

    let ticket,
        notAdmin = false;

    if(!request.headers['auth-a'] || !request.headers['auth-t']){

        code = 401;
        message = 'Authentication headers required';
        type = 'text/plain';

    } else if (request.headers['accept'] != 'application/json') {

        code = 406;
        message = 'Server can only generate application/json';
        type = 'text/plain';

    } else if (request.headers['content-type'] != 'application/json') {

        code = 415;
        message = 'Server can only accept application/json';
        type = 'text/plain';

    } else if (!request.payload.name || !request.payload.size) {

        code = 400;
        type = 'text/plain';
        message = 'Cannot create a place from input. Requires a name, description, and size.';

    } else try {

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin'){
            notAdmin = true;
            throw new Error('Normal users are not allowed to use this resource');
        }

        let place = new Place(
            request.payload.name,
            request.payload.description,
            request.payload.size
        );

        message = JSON.stringify(place);
        GameDB.Places.add(place);

    } catch (err) {

        if(!ticket) {
            message = 'Please log in to use this resource';
            code = 401;
        } else {
            message = err.message;

            if(notAdmin)
                code = 401;
            else
                code = 400;
        }

        type = 'text/plain';
    }

    reply(message).code(code).type(type);

};

place.GET = function(request, reply){

    let code = -1;
    let type = '';
    let message = '',
        ticket,
        missingHeads;

    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            missingHeads = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin')
            throw new Error('Normal users are not allowed to use this resource');

        let name = decodeURIComponent(request.params.id);
        log.debug('looking for place ' + name);

        let place = GameDB.Places.byId(name);

        if (place) {
            message = JSON.stringify(place);
            type = 'application/json';
            code = 200;
        } else {
            message = 'No Place with uid ' + name;
            type = 'text/plain';
            code = 404;
        }

    } catch(err) {

        if(!ticket && !missingHeads)
            message = 'Please log in to use this resource';
        else
            message = err.message;

        code = 401;
        type = 'text/plain';

    }

    reply(message).type(type).code(code);
};
