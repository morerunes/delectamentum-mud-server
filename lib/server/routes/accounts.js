'use strict';

const _ = require('lodash');
const Auth = require('../../services/AuthService');
const Account = require('../../objects/things/Account');
const GameDB = require('../../services/GameDBService');
const log = require('../../services/LogService');
const Route = require('../Route');

let accounts = new Route('/accounts');
let account = new Route('/accounts/{id}');

accounts.GET = function(request, reply){

    log.debug('serving /accounts via GET');

    let query = request.query,
        respString,
        code = 200,
        type = 'application/json';

    let ticket,
        missingHeads = false;

    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            missingHeads = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if (GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin') {

            throw new Error('Normal users are not allowed to use this resource');

        } else if (_.isEmpty(query)) {

            let resp = {
                totalAccounts: GameDB.Accounts.totalCount,
                accounts: null
            };

            resp.accounts = GameDB.Accounts.all();
            respString = JSON.stringify(resp);


        } else {

            let resp = {
                totalAccounts: GameDB.Accounts.totalCount,
                accounts: null
            };

            resp.accounts = GameDB.Accounts.search(_.omit(query, 'search'), !!query.search);
            respString = JSON.stringify(resp);

        }

    } catch(err) {

        if(!missingHeads && !ticket)
            respString = 'Please log in to use this resource';
        else
            respString = err.message;
        type = 'text/plain';
        code = 401;
    }


    log.debug('serving ' + respString);

    reply(respString).type(type).code(code);

};

accounts.POST = function (request, reply) {
    log.debug('processing POST at /accounts');

    let message = '';
    let code = 200;
    let type = 'application/json';

    let result = request.payload,
        ticket,
        notAdmin = false;

    if(!request.headers['auth-a'] || !request.headers['auth-t']){

        message = 'Authentication headers required';
        code = 401;
        type = 'text/plain';

    } else if (request.headers['content-type'] !== 'application/json') {

        message = 'Can only accept JSON';
        code = 415;
        type = 'text/plain';

    } else if (_.isEmpty(result)) {

        message = 'Cannot create an account from input ' + result;
        code = 415;
        type = 'text/plain';

    } else try {

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin') {
            notAdmin = true;
            throw new Error('Normal users are not allowed to use this resource');
        }

        let account = new Account(result.email, result.account_type);
        GameDB.Accounts.add(account);

        message = JSON.stringify(account);

    } catch (err) {

        if(!ticket){
            message = 'Please log in to use this resource';
            code = 401;
        }
        else {
            message = err.message;

            if(notAdmin)
                code = 401;
            else
                code = 400;
        }

        type = 'text/plain';

    }

    reply(message).type(type).code(code);
};

account.GET = function (request, reply) {

    let code = -1;
    let type = '';
    let message = '';
    let ticket, missingHeads;

    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            missingHeads = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin')
            throw new Error('Normal users are not allowed to use this resource');

        let name = decodeURIComponent(request.params.id);
        log.debug('looking for account ' + name);

        let acc = GameDB.Accounts.byId(name);

        if (acc) {
            message = JSON.stringify(acc);
            type = 'application/json';
            code = 200;
        } else {
            message = 'No Account with uid ' + name;
            type = 'text/plain';
            code = 404;
        }

    } catch(err) {
        if(!ticket && !missingHeads)
            message = 'Please log in to use this resource';
        else
            message = err.message;
        code = 401;
        type = 'text/plain';

    }

    reply(message).type(type).code(code);
};
