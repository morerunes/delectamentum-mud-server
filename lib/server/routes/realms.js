'use strict';

const Auth = require('../../services/AuthService');
const Cartograph = require('../../objects/things/Cartograph');
const GameDB = require('../../services/GameDBService');
const log = require('../../services/LogService');
const Route = require('../Route');

let realms = new Route('/realms');

realms.GET = function (request, reply) {

    let code = -1;
    let type = '';
    let message = '';

    let ticket,
        query,
        noHeaders = false,
        realms;

    try {

        if(!request.headers['auth-a'] || !request.headers['auth-t']){
            noHeaders = true;
            throw new Error('Authentication headers required');
        }

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin') {

            code = 401;
            type = 'text/plain';
            message = 'Normal users are not allowed to use this resource';

        } else if (request.headers.accept !== 'application/json') {

            code = 406;
            type = 'text/plain';
            message = 'Accept must be application/json';

        } else {

            code = 200;
            type = 'application/json';
            message = JSON.stringify({
                totalRealms: GameDB.Realms.totalCount,
                realms: GameDB.Realms.all()
            });
        }

    } catch (err) {
        if(!ticket && !noHeaders)
            message = 'Please log in to use this resource';
        else
            message = err.message;
        type = 'text/plain';
        code = 401;
    }

    reply(message).type(type).code(code);
};

realms.POST = function (request, reply) {

    let message = 'If this is the response from the server, god help us all';
    let code = 200;
    let type = 'text/plain';
    let ticket,
        notAdmin = false;

    log.debug('Recieved POST at /realms with payload');

    if (!request.headers['auth-t'] || !request.headers['auth-a']){

        message = 'Authentication headers required';
        code = 401;
        type = 'text/plain';

    } else if (request.headers.accept !== 'application/json') {

        message = 'server can only return json';
        code = 406;

    } else if (request.headers['content-type'] !== 'application/json') {

        message = 'server can only accept json';
        code = 415;

    } else if (!request.payload.name) {

        message = 'Cannot create a nameless Realm!';
        code = 400;

    } else try {

        ticket = Auth.retrieveTicket(request.headers['auth-t']);

        if(GameDB.Accounts.byId(request.headers['auth-a']).account_type != 'admin') {
            notAdmin = true;
            throw new Error('Normal users are not allowed to use this resource');
        }

        let realm = new Cartograph(request.payload.name, request.payload.description);
        GameDB.Realms.add(realm);

        code = 200;
        type = 'application/json';
        message = JSON.stringify(realm);

    } catch (err) {

        if(!ticket){
            message = 'Please log in to use this resource';
            code = 401;
        } else {

            message = err.message;

            if(notAdmin)
                code = 401;
            else
                code = 500;
        }

        type = 'text/plain';

    }

    log.debug('responding with ' + JSON.stringify({message, type, code}));

    reply(message).type(type).code(code);
};
