'use strict';

const ServerService = require('../services/ServerService');
const log = require('../services/LogService');

function Route(path) {

    this.path = path;

}

Route.isHTTPVerb = function (val) {
    val = val.toUpperCase();
    return (
        typeof val === 'string' &&
        (
            val === 'GET' ||
            val === 'PUT' ||
            val === 'POST' ||
            val === 'DELETE'
        )
    );
};

function _checkAndAssignHandler(route, handler, method) {

    if (!Route.isHTTPVerb(method)) {
        let message = `${method} is not a valid http verb`;
        log.error(message);
        throw new Error(message);
    }

    if (route[`_${method}`]) {
        let message = 'cannot define a method more than once per route';
        log.error(message);
        throw new Error(message);
    } else if (!handler || typeof handler !== 'function' || handler.length !== 2) {
        let message = `${handler} is not a valid handler!`;
        log.error(message);
        new Error(message);
    } else {
        route[`_${method}`] = handler;

        ServerService.addRoute({
            path: route.path,
            method: method.toUpperCase(),
            handler: route[`_${method}`]
        });

        log.debug(`Route added for ${method} to ${route.path}`);
    }
}

Route.prototype = {

    get path() {
        return this._path;
    },

    set path(path) {
        if (/^\/.*/.test(path)) {
            this._path = path;
        } else {
            throw new Error(`${path} is not a valid path`);
        }
    },

    get GET() {
        return this._get;
    },

    set GET(handler) {
        _checkAndAssignHandler(this, handler, 'get');
    },

    get POST() {
        return this._post;
    },

    set POST(handler) {
        _checkAndAssignHandler(this, handler, 'post');
    }

};

module.exports = Route;
